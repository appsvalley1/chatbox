package com.dating.chatbox;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.dating.chatbox.Adapters.HomeUserListAdapter;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.db.DBUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import it.sephiroth.android.library.tooltip.Tooltip;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class NearbyuserMapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    View view;
    private List<User> movieList = new ArrayList<>();
    GoogleMap googleMap;
    Bitmap returnedBitmap = null;
    private HomeUserListAdapter mAdapter;
    private ProgressBar progressBar;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int REQUEST_CHECK_SETTINGS = 12;
    private FusedLocationProviderClient mFusedLocationClient;
    int strokeColor = 0xff503D87; //Color Code you want
    int shadeColor = 0x448d8d8d; //opaque red fill
    Circle addCircle;
    TextView toolbarTitle;
    Double radius = 5000D;
    private FloatingActionButton fabRadius;
    private LinearLayout lvRadius;
    private Button updateRadius;
    private Marker usermarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearbyuser_maps);

        fabRadius = findViewById(R.id.fabRadius);
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(fabRadius, Tooltip.Gravity.LEFT)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 3000)
                        .activateDelay(800)
                        .showDelay(300)
                        .text("Tap to change the radius..")
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(true).withStyleId(R.style.ToolTipLayoutusersprofileCustomStyle)

                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW)
                        .build()
        ).show();

        fabRadius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lvRadius.setVisibility(View.VISIBLE);
                fabRadius.setVisibility(View.GONE);
            }
        });

        lvRadius = findViewById(R.id.lvRadius);
        updateRadius = findViewById(R.id.updateRadius);
        updateRadius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lvRadius.setVisibility(View.GONE);
                fabRadius.setVisibility(View.VISIBLE);
                loadUsers();
            }
        });
        Toolbar toolbar = findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        toolbarTitle = mCustomView.findViewById(R.id.appname);
        toolbarTitle.setText("Nearby On Maps(Change Location)");
        setSupportActionBar(toolbar);
        mCustomView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(NearbyuserMapsActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        progressBar = findViewById(R.id.progressBar);



        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            getLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SeekBar seekBar = findViewById(R.id.seekbar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                radius = Double.valueOf(i);

                addCircle.setRadius(Double.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    private void getLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // Check Permissions Now
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CHECK_SETTINGS);
            } else {
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    loadCurrentLocation(location);
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            loadUsers();
                                        }
                                    }, 500);
                                } else {
                                    getCurLoc();
                                }

                            }
                        })
                        .addOnFailureListener(this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                getCurLoc();
                            }
                        });
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void getCurLoc() {
        try {
            final LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(this);
            Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
            task.addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(NearbyuserMapsActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                        }
                    }
                }
            });
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult != null && locationResult.getLastLocation() != null) {
                            loadCurrentLocation(locationResult.getLastLocation());


                            mFusedLocationClient.removeLocationUpdates(this);
                        }
                    }
                }, Looper.myLooper());
            }//end of if


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadUsers() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(this);
        }

        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(NearbyuserMapsActivity.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

                progressBar.setVisibility(View.GONE);
            }
        });
        String url = "http://proacdoc.com/Service1.svc/GetAllUsersNearbyLocation?UserId=" + DBUtil.getUSERID() + "&Latitude=" + DBUtil.getLatitude() + "&Longitude=" + DBUtil.getLongitude() + "&Radius=" + new DecimalFormat("#").format(radius/1000);
        Log.e("url", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            JSONArray jsonArray = jsonObject1.optJSONArray("UserList");
                            movieList = new ArrayList<>();
                            if (jsonArray.length() == 0) {
                                Toast.makeText(getApplicationContext(), "No Users in this location",Toast.LENGTH_LONG).show();
                                movieList.clear();
                            } else {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    final User user = new User();
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    user.setId(jsonObject.optInt("Id"));
                                    user.setName(jsonObject.optString("Name"));
                                    user.setCityName(jsonObject.optString("CityName"));
                                    user.setShortBio(jsonObject.optString("ShortBio"));
                                    user.setDistanceKM(jsonObject.optString("DistanceKM"));
                                    user.setBirthday(jsonObject.optString("Birthday"));
                                    user.setLatitude(jsonObject.optString("Latitude"));
                                    user.setLongitude(jsonObject.optString("Longitude"));
                                    if (jsonObject.optString("Gender").equalsIgnoreCase("1")) {
                                        user.setGender("Male");
                                    } else {
                                        user.setGender("Female");
                                    }
                                    user.setViewsCount(jsonObject.optInt("ViewsCount"));
                                    ArrayList<UserImages> userImages = new ArrayList<UserImages>();
                                    JSONArray userImagesarray = jsonObject.optJSONArray("userImages");
                                    for (int j = 0; j < userImagesarray.length(); j++) {
                                        JSONObject userImagejson = userImagesarray.optJSONObject(j);
                                        UserImages userImages1 = new UserImages();
                                        String image1 =  userImagejson.optString("UserImageUrl");
                                        userImages1.setUserImageUrl(image1);
                                        userImages.add(userImages1);
                                    }
                                    user.setUserImages(userImages);
                                    user.setShowOnlineStatus(jsonObject.optInt("ShowOnlineStatus"));
                                    user.setShowLocationStatus(jsonObject.optInt("ShowLocationStatus"));
                                    movieList.add(user);
                                    getMarkerBitmapFromView(user,i==jsonArray.length()-1?true:false);
                                }
                                Toast.makeText(getApplicationContext(), "Move Map to see users",Toast.LENGTH_LONG).show();


                            }
                        } catch (JSONException e) {

                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressBar.setVisibility(View.GONE);

            }
        });
        queue.add(stringRequest);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.style_map));

        this.googleMap = googleMap;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if (!marker.getTag().toString().equalsIgnoreCase("USER")) {
            User.setUserProfile((User) marker.getTag());
            Intent intent = new Intent(NearbyuserMapsActivity.this, UsersProfile.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        }
        return false;

    }

    private void getMarkerBitmapFromView(final User user, final boolean isLast) {
        final View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        final ImageView markerImageView = customMarkerView.findViewById(R.id.profile_image);
        Glide.with(NearbyuserMapsActivity.this)
                .load(user.getUserImages().get(0).getUserImageUrl()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).bitmapTransform(new CropCircleTransformation(NearbyuserMapsActivity.this))
                .into(new GlideDrawableImageViewTarget(markerImageView) {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);

                        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                        customMarkerView.buildDrawingCache();
                        returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(returnedBitmap);
                        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
                        Drawable _drawable = customMarkerView.getBackground();
                        if (_drawable != null)
                            _drawable.draw(canvas);
                        customMarkerView.draw(canvas);
                        LatLng sydney = new LatLng(Double.valueOf(user.getLatitude()), Double.valueOf(user.getLongitude()));
                        Marker marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap))
                                .title(user.getName()));
                        marker.setTag(user);
                        marker.showInfoWindow();
                        if (isLast) {
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));
                        }
                    }
                });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK) {
            getCurLoc();
        }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Location temp = new Location(LocationManager.GPS_PROVIDER);
                temp.setLatitude(place.getLatLng().latitude);
                temp.setLongitude(place.getLatLng().longitude);
                DBUtil.setLatitude(place.getLatLng().latitude + "");
                DBUtil.setLongitude(place.getLatLng().longitude + "");
                loadCurrentLocation(temp);
                toolbarTitle.setText("Nearby From " + place.getName() + " (Change)");
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                toolbarTitle.setText("Unknown Location :(");
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                toolbarTitle.setText("Nearby On Maps");
                // The user canceled the operation.
            }
        }
    }

    private void loadCurrentLocation(final Location location) {

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        getUserMarker(latLng);
        if (addCircle == null) {

            addCircle = googleMap.addCircle(new CircleOptions().center(latLng).radius(radius).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(5));
        } else {
            addCircle.remove();
            addCircle = googleMap.addCircle(new CircleOptions().center(latLng).radius(radius).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(5));
        }


        DBUtil.setLatitude(location.getLatitude() + "");
        DBUtil.setLongitude(location.getLongitude() + "");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadUsers();
            }
        }, 500);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
    }

    private void getUserMarker(LatLng latLng) {
        final View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_user_marker, null);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable _drawable = customMarkerView.getBackground();
        if (_drawable != null)
            _drawable.draw(canvas);
        customMarkerView.draw(canvas);
        if (usermarker != null) {
            usermarker.remove();

        }
        usermarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap))
        );
        usermarker.setTag("USER");
        usermarker.showInfoWindow();


    }


}
