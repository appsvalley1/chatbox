package com.dating.chatbox;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.HomePreferencesUpdate;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.controllers.PreferencesController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.PreferenceResponse;
import com.dating.chatbox.db.DBUtil;
import com.github.guilhe.rangeseekbar.SeekBarRangedView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;


public class MyPrefrences extends AppCompatActivity {
    TextView textviewMin, textviewMax;
    private SeekBarRangedView seekBarRangedView;
    private RadioGroup radioGender, radioStatus;

    private Button buttonAddPreferences;
    private RadioButton radiomale, radioFemale, radioGenderAll;
    private ProgressBar progressBar;
    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_prefrences);

        Toolbar toolbar = findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        TextView appname =  mCustomView.findViewById(R.id.appname);
        appname.setText("My Preferences");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        findviewsById();
        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {

            String Gender;
            String MinAge;
            String MaxAge;


            @Override
            public void onBackground() {
                try {

                    Gender = DBUtil.getGenderPreference();
                    MinAge = DBUtil.getMinAgePreference();
                    MaxAge = DBUtil.getMaxAgePreference();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                super.onUi();
                seekBarRangedView.setSelectedMinValue(Float.valueOf(MinAge));
                seekBarRangedView.setSelectedMaxValue(Float.valueOf(MaxAge));
                switch (Gender) {
                    case "1":
                        radiomale.setChecked(true);
                        break;
                    case "2":
                        radioFemale.setChecked(true);
                        break;
                    case "3":
                        radioGenderAll.setChecked(true);
                        break;
                }

            }
        });


    }

    public void findviewsById() {
        progressBar =  findViewById(R.id.progressBar);
        textviewMin =  findViewById(R.id.textviewMin);
        textviewMax =  findViewById(R.id.textviewMax);
        seekBarRangedView =  findViewById(R.id.seekBarRangedView);
        seekBarRangedView.setOnSeekBarRangedChangeListener(new SeekBarRangedView.OnSeekBarRangedChangeListener() {
            @Override
            public void onChanged(SeekBarRangedView seekBarRangedView, final double v, final double v1) {
                textviewMin.setText(String.valueOf(Math.round(v) + "-"));
                ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                    @Override
                    public void onBackground() {
                        try {
                            DBUtil.setMinAgePreference(String.valueOf(Math.round(v)));
                            DBUtil.setMaxAgePreference(String.valueOf(Math.round(v1)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
                textviewMax.setText(String.valueOf(Math.round(v1)));
            }

            @Override
            public void onChanging(SeekBarRangedView seekBarRangedView, double v, double v1) {
                textviewMin.setText(String.valueOf(Math.round(v) + "-"));
                textviewMax.setText(String.valueOf(Math.round(v1)));

            }
        });
        textviewMin.setText(String.valueOf(seekBarRangedView.getMinValue()) + "-");
        textviewMax.setText(String.valueOf(seekBarRangedView.getMaxValue()));
        radioGender =  findViewById(R.id.radioGender);
        radioGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radiomale) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setGenderPreference("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });


                } else if (checkedId == R.id.radioFemale) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setGenderPreference("2");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });

                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setGenderPreference("3");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });

                }
            }

        });

        buttonAddPreferences = (Button) findViewById(R.id.buttonAddPreferences);
        buttonAddPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    Answers.getInstance().logCustom(new CustomEvent("Preference Set"));
                if (Utils.CheckIf2G()) {
                    Utils.TakingTimeMsg(MyPrefrences.this);
                }
                progressBar.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject();
                String url = Singleton.BaseUrl + "AddUserPreferences?UserId=" + DBUtil.getUSERID() + "&MinAgePreference=" + DBUtil.getMinAgePreference() + "&MaxAgePreference=" + DBUtil.getMaxAgePreference() + "&GenderPreference=" + DBUtil.getGenderPreference();
                PreferencesController controller = new PreferencesController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
                    @Override
                    public void handleResponse(ResponsePayload responseObject) {

                        try {

                            if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof PreferenceResponse) {
                                final PreferenceResponse response = (PreferenceResponse) responseObject.getResponseObject();
                                progressBar.setVisibility(View.GONE);
                                ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                    @Override
                                    public void onBackground() {
                                        try {
                                            DBUtil.setUserPreferences("1");
                                        } catch (Throwable e) {

                                        }
                                        finish();
                                    }

                                    @Override
                                    public void onUi() {
                                        super.onUi();
                                        EventBus.getDefault().post(new HomePreferencesUpdate());
                                        Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                                    }
                                });


                            }

                        } catch (Throwable e) {

                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                }, url);
                controller.setRequestObj(jsonObject);
                controller.sendRequest();






















            }
        });
        radiomale = findViewById(R.id.radiomale);
        radioFemale =  findViewById(R.id.radioFemale);
        radioGenderAll =  findViewById(R.id.radioGenderAll);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
