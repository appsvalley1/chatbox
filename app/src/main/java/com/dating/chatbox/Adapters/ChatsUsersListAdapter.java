package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.ChatScreen;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ChatsModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;

import java.util.List;
import java.util.WeakHashMap;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ChatsUsersListAdapter extends RecyclerView.Adapter<ChatsUsersListAdapter.MyViewHolder> {

    public Activity activity;
    private List<ChatsModel> moviesList;

    public WeakHashMap<Integer, MyViewHolder> getIntegerStringHashMap() {
        return integerStringHashMap;
    }

    public void setIntegerStringHashMap(WeakHashMap<Integer, MyViewHolder> integerStringHashMap) {
        this.integerStringHashMap = integerStringHashMap;
    }

    public WeakHashMap <Integer,MyViewHolder> integerStringHashMap=new WeakHashMap<>(0);
    public ChatsUsersListAdapter(List<ChatsModel> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chatuserslist_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            final ChatsModel activityModel = moviesList.get(position);
            holder.name.setText(activityModel.getChatUser().getName());
            switch (activityModel.getChatIsText()) {
                case 0:
                    holder.chattext.setText(activityModel.getChatText());
                    holder.imageViewPhoto.setVisibility(View.GONE);
                    break;
                case 1:
                    holder.chattext.setText("Photo");
                    holder.imageViewPhoto.setVisibility(View.VISIBLE);
                    break;
            }


            holder.timestamp.setText(Utils.getlongtoago(activityModel.getChatTime()));
           if(activityModel.getChatUser().getUserImages().size()>=1)
           { Glide.with(activity)
                   .load(activityModel.getChatUser().getUserImages().get(0).UserImageUrl).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))
                   .bitmapTransform(new CropCircleTransformation(activity))
                   .into(holder.userImage);}

            holder.lvShowProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User.setUserProfile(activityModel.getChatUser());
                    Intent chatScreen = new Intent(activity, ChatScreen.class);
                    activity.startActivity(chatScreen);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);

                }
            });

            if(integerStringHashMap.get(activityModel.getChatUser().getId())==null)
            {
                integerStringHashMap.put(activityModel.getChatUser().getId(),holder);
            }

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, chattext;
        public ImageView userImage, imageViewPhoto;
        public LinearLayout lvShowProfile;
        public TextView timestamp;

        public MyViewHolder(View view) {
            super(view);
            try {
                name = (TextView) view.findViewById(R.id.name);
                chattext = (TextView) view.findViewById(R.id.chattext);
                userImage = (ImageView) view.findViewById(R.id.userImage);
                lvShowProfile = (LinearLayout) view.findViewById(R.id.lvShowProfile);
                timestamp = (TextView) view.findViewById(R.id.timestamp);
                imageViewPhoto = view.findViewById(R.id.imageViewPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}