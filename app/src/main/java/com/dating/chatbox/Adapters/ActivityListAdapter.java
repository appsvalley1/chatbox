package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Models.ActivityModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;
import com.dating.chatbox.UsersProfile;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ActivityListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Activity activity;
    private List<ActivityModel> moviesList;
    private static final int ITEM_TYPE_Data = 0;
    private static final int ITEM_TYPE_Ad = 1;
    private LayoutInflater inflater;


    public ActivityListAdapter(List<ActivityModel> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == ITEM_TYPE_Data) {
            View dataRow = inflater.inflate(R.layout.activity_row, parent, false);
            return new DataViewHolder(dataRow);
        }
        View adRow = inflater.inflate(R.layout.admob_activity_row, parent, false);
        return new AdViewHolder(adRow);
    }
    @Override
    public int getItemViewType(int position) {
        if (!moviesList.get(position).getActivityIsAd()) {
            return ITEM_TYPE_Data;
        } else {
            return ITEM_TYPE_Ad;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final int itemType = getItemViewType(position);
        if (itemType == ITEM_TYPE_Data) {
            ((DataViewHolder) holder).bindData(moviesList.get(position));
        } else if (itemType == ITEM_TYPE_Ad) {
            ((AdViewHolder) holder).bindAd(moviesList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        public TextView name, dob, textViewDated;
        public ImageView userImage;
        public LinearLayout lvShowProfile;

        public DataViewHolder(View view) {
            super(view);
            try {
                name = (TextView) view.findViewById(R.id.name);
                dob = (TextView) view.findViewById(R.id.dob);
                userImage = (ImageView) view.findViewById(R.id.userImage);
                lvShowProfile = (LinearLayout) view.findViewById(R.id.lvShowProfile);
                textViewDated = (TextView) view.findViewById(R.id.textViewDated);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void bindData(final ActivityModel activityModel) {

            try {

                name.setText(activityModel.getActivityDetail());
                textViewDated.setText(activityModel.getActivitydate());
                if(activityModel.getActivityUser().getUserImages()!=null&&activityModel.getActivityUser().getUserImages().size()!=0)
                {
                    Glide.with(activity)
                            .load(activityModel.getActivityUser().getUserImages().get(0).getUserImageUrl()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))
                            .bitmapTransform(new CropCircleTransformation(activity))
                            .into(userImage);

                }

                lvShowProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User.setUserProfile(activityModel.ActivityUser);
                        Intent intent = new Intent(activity, UsersProfile.class);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                });
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout lvShowProfile;

        public AdViewHolder(View view) {
            super(view);
            try {
                lvShowProfile = view.findViewById(R.id.lvShowProfile);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        private void bindAd(final ActivityModel activityModel) {

            if (activityModel.getActivityIsAd() && activityModel.getNativeAd() != null) {
                lvShowProfile.removeAllViews();
                lvShowProfile.addView(activityModel.getNativeAd());
            }
        }


        // Request an ad
    }



}