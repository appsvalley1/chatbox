package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ChatsModel;
import com.dating.chatbox.R;
import com.dating.chatbox.UsersDetailedImages;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.MyViewHolder> {

    public static final int ITEM_TYPE_Sender = 0;
    public static final int ITEM_TYPE_Reciever = 1;
    public Activity activity;
    private List<ChatsModel> moviesList;

    public ChatsAdapter(List<ChatsModel> moviesList, Activity activity) {
        this.moviesList = moviesList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_Sender) {
            View chatToView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.chatsenderrow, parent, false);
            return new MyViewHolder(chatToView);
        }

        View chatFromView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.chatrecieverrow, parent, false);
        return new MyViewHolder(chatFromView);
    }

    @Override
    public int getItemViewType(int position) {
        if (moviesList.get(position).getisSender()) {
            return ITEM_TYPE_Sender;
        } else {
            return ITEM_TYPE_Reciever;
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            final ChatsModel activityModel = moviesList.get(position);
            holder.lvShowProfile.setVisibility(View.VISIBLE);
            if (activityModel.getChatIsText() == 1) {
                final  String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + activityModel.getChatText() + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                Glide.with(activity)
                        .load(image1).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))
                        .into(holder.image);
                holder.image.setVisibility(View.VISIBLE);
                holder.chattext.setText("");
                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChatApplication.getInstance().isFromProfile=false;
                        Intent usersDetailedImages = new Intent(activity, UsersDetailedImages.class);
                        usersDetailedImages.putExtra("imageURL",image1);
                        activity.startActivity(usersDetailedImages);
                    }
                });
            } else {
                holder.image.setVisibility(View.GONE);
                holder.chattext.setText(activityModel.getChatText());
            }

              holder.textviewAgo.setText(Utils.getlongtoago(activityModel.getChatTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public EmojiconTextView chattext, textviewAgo;
        public LinearLayout lvShowProfile;
        public ImageView image;
        public MyViewHolder(View view) {
            super(view);
            try {
                chattext = view.findViewById(R.id.chattext);
                lvShowProfile = view.findViewById(R.id.lvShowProfile);
                //   textviewAgo = view.findViewById(R.id.textviewAgo);
                image = view.findViewById(R.id.image);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}