package com.dating.chatbox.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.MyImages;
import com.dating.chatbox.R;

import java.util.List;


/**
 * Created by USER on 11/9/2017.
 */

public class MyImagesAdapter extends PagerAdapter {

    private Context mContext;
    private List<UserImages> userImages;

    public MyImagesAdapter(Context context, List<UserImages> _userImages) {
        mContext = context;
        userImages = _userImages;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        try {
            UserImages modelObject = userImages.get(position);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = (ViewGroup) inflater.inflate(R.layout.customusersimageviewpagerrow, container, false);

            ImageView displayImage = (ImageView) view.findViewById(R.id.displayImage);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  Answers.getInstance().logCustom(new CustomEvent("User Image Add Profilr"));
                    Intent myImages = new Intent(mContext, MyImages.class);
                    mContext.  startActivity(myImages);

                }
            });


            Glide.with(mContext) //
                    .load(modelObject.UserImageUrl).dontAnimate() //
                    .diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(displayImage);


            container.addView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        try {
            container.removeView((LinearLayout) view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return userImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}