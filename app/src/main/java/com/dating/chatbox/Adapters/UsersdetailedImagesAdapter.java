package com.dating.chatbox.Adapters;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.R;

import java.util.ArrayList;


/**
 * Created by USER on 11/9/2017.
 */

public class UsersdetailedImagesAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<UserImages> userImages;

    public UsersdetailedImagesAdapter(Context context, ArrayList<UserImages> _userImages) {
        mContext = context;
        userImages = _userImages;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        try {
            UserImages modelObject = userImages.get(position);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = (ViewGroup) inflater.inflate(R.layout.customusersimageviewpagerrow, container, false);

            ImageView displayImage = (ImageView) view.findViewById(R.id.displayImage);


            Glide.with(mContext) //
                    .load(modelObject.getUserImageUrl()).dontAnimate() //
                    .diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(displayImage);


            container.addView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((LinearLayout) view);
    }

    @Override
    public int getCount() {
        return userImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}