package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Custom.OnLoadMoreListener;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;
import com.dating.chatbox.UsersProfile;

import java.util.HashMap;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class HomeUserListAdapter extends RecyclerView.Adapter<HomeUserListAdapter.MyViewHolder> {

    public Activity activity;
    private List<User> moviesList;
    private OnLoadMoreListener onLoadMoreListener;
    private int extraCount;
    private int lastVisibleItem, totalItemCount;
    private int visibleThreshold = 1;
    private boolean loading;
    private HashMap<Integer,String>  integerStringHashMap=new HashMap<>(0);

    public HomeUserListAdapter(List<User> moviesList, Activity activity, RecyclerView recyclerView) {
        this.moviesList = moviesList;

        this.activity = activity;
        try {
            if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final User user = moviesList.get(position);
        holder.name.setText(user.getName());
        holder.dob.setText(user.getBirthday());


        if (user.getShowOnlineStatus() == 0) {
            Glide.with(activity.getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.ImageUserStatus);
        } else {
            if(integerStringHashMap.get(user.getId())==null)
            {
                loadUserStatus(user.getId(), holder.ImageUserStatus);
            }
            else
            {
                String response=integerStringHashMap.get(user.getId());
                if (response.equalsIgnoreCase("null")) {
                    response = "offline";
                }
                if (response.equalsIgnoreCase("Online Now")) {

                    Glide.with(activity.getApplicationContext()).load(R.drawable.greenonline).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.ImageUserStatus);
                } else {
                    Glide.with(activity.getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.ImageUserStatus);
                }
            }

        }


        Glide.with(activity)
                .load(user.getUserImages().get(0).getUserImageUrl()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))
                .bitmapTransform(new CropCircleTransformation(activity))
                .into(holder.userImage);
        holder.lvShowProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.setUserProfile(user);

                Intent intent = new Intent(activity, UsersProfile.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private void loadUserStatus(final int userId, final ImageView _ImageUserStatus) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                //    progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "http://52.41.229.242:9090/plugins/presence/status?jid=" + userId + "@52.41.229.242&type=text";
        url = url.replaceAll(" ", "%20");


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        response = response.trim();
                        if (response.equalsIgnoreCase("null")) {
                            response = "offline";
                        }
                        if (response.equalsIgnoreCase("Online Now")) {

                            Glide.with(activity.getApplicationContext()).load(R.drawable.greenonline).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(_ImageUserStatus);
                        } else {
                            Glide.with(activity.getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(_ImageUserStatus);
                        }
                        integerStringHashMap.put(userId,response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        queue.add(stringRequest);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, dob;
        public ImageView userImage, ImageUserStatus;
        public LinearLayout lvShowProfile;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            dob = (TextView) view.findViewById(R.id.dob);
            userImage = (ImageView) view.findViewById(R.id.userImage);
            lvShowProfile = (LinearLayout) view.findViewById(R.id.lvShowProfile);
            ImageUserStatus = view.findViewById(R.id.ImageUserStatus);
        }
    }
}