package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.res.Resources;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.EventbusLib.DeleteImage;
import com.dating.chatbox.HelperClasses.ImagePicker;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class MyImagesGridAdapter extends RecyclerView.Adapter<MyImagesGridAdapter.MyViewHolder> {

    public Activity activity;
    private List<UserImages> moviesList;

    public MyImagesGridAdapter(List<UserImages> moviesList, Activity activity) {
        this.moviesList = moviesList;

        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.addimagebackground, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            final UserImages user = moviesList.get(position);
            holder.lvImage.setOnClickListener(null);

            if (user.getBlankImage()) {

                holder.lvAddRemoveImage.setImageDrawable(activity.getResources().getDrawable(R.drawable.profile_add_photo));
                holder.lvImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatApplication.getInstance().IsUserImage = false;
                        try {
                            ImagePicker.pickImage(activity);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {


                holder.lvAddRemoveImage.setImageDrawable(activity.getResources().getDrawable(R.drawable.profile_remove_photo));
                Glide.with(activity)
                        .load(user.getUserImageUrl()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))

                        .into(holder.userImage);
                holder.lvImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (user.getFirstImage()) {
                            Toast.makeText(activity.getApplicationContext(), "Picture at Registration Cannot be changed", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                EventBus.getDefault().post(new DeleteImage(user.ImageId));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }




                    }
                });
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView userImage, lvAddRemoveImage;
        public RelativeLayout lvImage;


        public MyViewHolder(View view) {
            super(view);

            try {
                userImage =  view.findViewById(R.id.userImage);
                lvImage =  view.findViewById(R.id.lvImage);
                lvAddRemoveImage =  view.findViewById(R.id.addremoveImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}