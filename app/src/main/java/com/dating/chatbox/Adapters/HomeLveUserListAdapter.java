package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.GoLive.ConstantApp;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;
import com.dating.chatbox.StartBroadcastingActivity;

import java.util.HashMap;
import java.util.List;

import io.agora.rtc.Constants;

public class HomeLveUserListAdapter extends RecyclerView.Adapter<HomeLveUserListAdapter.MyViewHolder> {

    public Activity activity;
    private List<User> moviesList;




    private HashMap<Integer,String>  integerStringHashMap=new HashMap<>(0);

    public HomeLveUserListAdapter(List<User> moviesList, Activity activity) {
        this.moviesList = moviesList;

        this.activity = activity;

    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customrowliveusers, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final User user = moviesList.get(position);
        holder.name.setText(user.getName());
        holder.viewsCount.setText(String.valueOf(user.getViewsCount()));





        Glide.with(activity)
                .load(user.getUserImages().get(0).getUserImageUrl()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))

                .into(holder.userImage);
        holder.lvShowProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(activity,StartBroadcastingActivity.class);
                i.putExtra(ConstantApp.ACTION_KEY_CROLE, Constants.CLIENT_ROLE_AUDIENCE);
                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME,String.valueOf(user.getId()));
                activity. startActivity( i);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, viewsCount;
        public ImageView userImage, ImageUserStatus;
        public RelativeLayout lvShowProfile;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            viewsCount = (TextView) view.findViewById(R.id.viewsCount);
            userImage = (ImageView) view.findViewById(R.id.userImage);
            lvShowProfile =  view.findViewById(R.id.lvShowProfile);
            ImageUserStatus = view.findViewById(R.id.ImageUserStatus);
        }
    }
}