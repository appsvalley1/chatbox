package com.dating.chatbox.Adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.FavouritesModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;
import com.dating.chatbox.UsersProfile;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class FavouriteListAdapter extends RecyclerView.Adapter<FavouriteListAdapter.MyViewHolder> {

    public Activity activity;
    private List<FavouritesModel> moviesList;

    public FavouriteListAdapter(List<FavouritesModel> moviesList, Activity activity) {
        this.moviesList = moviesList;

        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favourite_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final FavouritesModel activityModel = moviesList.get(position);
        holder.name.setText(activityModel.getFavouriteuser().getName());
        holder.textViewDated.setText(Utils.getlongtoago(Long.valueOf(activityModel.getFavouriteDated())));
        Glide.with(activity)
                .load(activityModel.getFavouriteuser().getUserImages().get(0).getUserImageUrl()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(activity.getResources().getDrawable(R.drawable.defalut_profile))
                .bitmapTransform(new CropCircleTransformation(activity))
                .into(holder.userImage);
        holder.lvShowProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.setUserProfile(activityModel.Favouriteuser);

                Intent intent = new Intent(activity, UsersProfile.class);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, dob, textViewDated;
        public ImageView userImage;
        public LinearLayout lvShowProfile;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            dob = (TextView) view.findViewById(R.id.dob);
            userImage = (ImageView) view.findViewById(R.id.userImage);
            lvShowProfile = (LinearLayout) view.findViewById(R.id.lvShowProfile);
            textViewDated = (TextView) view.findViewById(R.id.textViewDated);
        }
    }
}