package com.dating.chatbox.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.R;
import com.dating.chatbox.UsersDetailedImages;

import java.util.ArrayList;


/**
 * Created by USER on 11/9/2017.
 */

public class UsersImagesAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<UserImages> userImages;

    public UsersImagesAdapter(Context context, ArrayList<UserImages> _userImages) {
        mContext = context;
        userImages = _userImages;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        try {
            UserImages modelObject = userImages.get(position);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = (ViewGroup) inflater.inflate(R.layout.customusersimageviewpagerrow, container, false);

            ImageView displayImage = (ImageView) view.findViewById(R.id.displayImage);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatApplication.getInstance().isFromProfile=true;
                    Intent usersDetailedImages = new Intent(mContext, UsersDetailedImages.class);
                    mContext.startActivity(usersDetailedImages);
                }
            });


            Glide.with(mContext) //
                    .load(modelObject.getUserImageUrl()).dontAnimate() //
                    .diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(displayImage);


            container.addView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        try {
            container.removeView((LinearLayout) view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return userImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}