package com.dating.chatbox.Fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Adapters.HomeUserListAdapter;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.OnLoadMoreListener;
import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.HomePreferencesUpdate;
import com.dating.chatbox.GoLive.ConstantApp;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.MyPrefrences;
import com.dating.chatbox.R;
import com.dating.chatbox.StartBroadcastingActivity;
import com.dating.chatbox.controllers.HomeUsersController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.HomeUsersResponse;
import com.dating.chatbox.db.DBUtil;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.skyfishjy.library.RippleBackground;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.agora.rtc.Constants;
import jp.wasabeef.glide.transformations.CropCircleTransformation;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    View view;
    private List<User> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HomeUserListAdapter mAdapter;

    private RippleBackground progressBar;
    private ImageView userImageProgress;
    private LinearLayout small_button;
    private SessionManager sessionManager;
    private LinearLayout lvPreference, lvNoUserFound;
    private int pageCount;
    private boolean loadMoreData = true;
    private boolean isFirstTime = true;
    private LinearLayout lvUnableToLoad;
    private FloatingActionButton fab;


    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.homefragment, container, false);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        lvUnableToLoad = view.findViewById(R.id.lvUnableToLoad);
        lvUnableToLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lvUnableToLoad.setVisibility(View.GONE);
                loadUsers();
            }
        });

        progressBar = view.findViewById(R.id.progressBar);
        userImageProgress = view.findViewById(R.id.userImageProgress);
        recyclerView = view.findViewById(R.id.recycler_view);
        lvNoUserFound = view.findViewById(R.id.lvNoUserFound);
        lvNoUserFound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), MyPrefrences.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        small_button = view.findViewById(R.id.small_button);
        sessionManager = new SessionManager(getActivity());
        lvPreference = view.findViewById(R.id.lvPreference);
        pageCount = 1;
        mAdapter = null;
        isFirstTime = true;
        movieList.clear();


        loadUsers();


        lvPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(), MyPrefrences.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {

            public String Preference;
            public String userImage;

            @Override
            public void onBackground() {
                try {
                    Preference = DBUtil.getUser_Preferences();
                    userImage = DBUtil.getUserImage();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                super.onUi();
                try {
                    Glide.with(getActivity())
                            .load(userImage).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).bitmapTransform(new CropCircleTransformation(getActivity()))
                            .into(userImageProgress);
                    if (Preference.equalsIgnoreCase("")) {
                        lvPreference.setVisibility(View.VISIBLE);
                    } else {
                        lvPreference.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateUsersForLive();
                Intent i = new Intent(getActivity(), StartBroadcastingActivity.class);
                i.putExtra(ConstantApp.ACTION_KEY_CROLE, Constants.CLIENT_ROLE_BROADCASTER);
                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, DBUtil.getUSERID());
                startActivity(i);

            }
        });
        return view;
    }


    private void loadUsers() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(getActivity());
        }
        if (isFirstTime == true) {
            progressBar.startRippleAnimation();
            progressBar.setVisibility(View.VISIBLE);
            isFirstTime = false;
        }
        if (loadMoreData) {
            JSONObject jsonObject = new JSONObject();
            String url = "http://proacdoc.com/Service1.svc/GetAllUserslist?UserId=" + DBUtil.getUSERID() + "&Pageno=" + pageCount;
            HomeUsersController controller = new HomeUsersController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
                @Override
                public void handleResponse(ResponsePayload responseObject) {

                    try {
                        progressBar.stopRippleAnimation();
                        progressBar.setVisibility(View.GONE);
                        if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof HomeUsersResponse) {
                            final HomeUsersResponse response = (HomeUsersResponse) responseObject.getResponseObject();

                            if (response.UserList.size() == 0 && movieList.size() == 0) {
                                lvNoUserFound.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                loadMoreData = false;
                                pageCount = 1;
                            } else {
                                pageCount = pageCount + 1;
                                BindData(response.UserList);
                                if (sessionManager.GetOneTimeTutorial() == false && DBUtil.getUser_Preferences().equalsIgnoreCase("")) {
                                    loadFirstTutorial();
                                }
                            }

                        } else {
                            progressBar.stopRippleAnimation();
                            progressBar.setVisibility(View.GONE);
                            lvUnableToLoad.setVisibility(View.VISIBLE);
                        }
                    } catch (Throwable e) {

                        progressBar.stopRippleAnimation();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }, url);
            controller.setRequestObj(jsonObject);
            controller.sendRequest();
        }


    }

    private void loadFirstTutorial() {
        sessionManager.SetOneTimeTutorial(true);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                TapTargetView.showFor(getActivity(),
                        TapTarget.forView(small_button, "Update Prefrences First", "After that you will be able to search nearby users")

                                .outerCircleColor(R.color.colorPrimaryDark)
                                .outerCircleAlpha(0.99f)
                                .targetCircleColor(R.color.White)
                                .titleTextSize(20)
                                .titleTextColor(R.color.White)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.new_mobile_num_text_color)

                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.Black)
                                .drawShadow(true)
                                .cancelable(true)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(33),
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);
                                if (recyclerView != null && movieList != null) {
                                    View secondView = recyclerView.findViewHolderForAdapterPosition(1).itemView;
                                    loadAppsTutorial(secondView);
                                }

                            }
                        });
            }
        }, 600);
    }

    private void loadAppsTutorial(final View v) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                TapTargetView.showFor(getActivity(),
                        TapTarget.forView(v, "Tap to chat ", "We have shortlisted  a special user for you.\nClick on it and start chatting")

                                .outerCircleColor(R.color.colorPrimaryDark)
                                .outerCircleAlpha(0.99f)
                                .targetCircleColor(R.color.White)
                                .titleTextSize(20)
                                .titleTextColor(R.color.White)
                                .descriptionTextSize(14)
                                .descriptionTextColor(R.color.new_mobile_num_text_color)
                                .textTypeface(Typeface.SANS_SERIF)
                                .dimColor(R.color.Black)
                                .drawShadow(true)
                                .cancelable(true)
                                .tintTarget(true)
                                .transparentTarget(true)
                                .targetRadius(100),
                        new TapTargetView.Listener() {


                            @Override
                            public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                super.onTargetDismissed(view, userInitiated);

                            }

                            @Override
                            public void onTargetCancel(TapTargetView view) {
                                super.onTargetCancel(view);
                                view.dismiss(true);
                            }

                            @Override
                            public void onTargetClick(TapTargetView view) {
                                view.dismiss(true);

                                super.onTargetClick(view);

                            }
                        });
            }
        }, 250);
    }


    private void BindData(List<User> usersList) {

        movieList.addAll(usersList);
        if (movieList != null && movieList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            lvNoUserFound.setVisibility(View.GONE);

            if (mAdapter == null) {

                GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3, GridLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(gridLayoutManager);
                recyclerView.setHasFixedSize(false);
                mAdapter = new HomeUserListAdapter(movieList, getActivity(), recyclerView);
                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        loadUsers();
                    }
                });
                recyclerView.setAdapter(mAdapter);
            } else {
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();

            }
        }
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(HomePreferencesUpdate event) {
        try {
            lvPreference.setVisibility(View.GONE);
            movieList.clear();
            pageCount = 1;
            loadMoreData = true;
            loadUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UpdateUsersForLive() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

            }
        });
        String url = Singleton.BaseUrl + "SendNoificationForLive?UserId=" + DBUtil.getUSERID();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


}
