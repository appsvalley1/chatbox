package com.dating.chatbox.Fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.dating.chatbox.Adapters.ActivityListAdapter;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.ActivityUpdate;
import com.dating.chatbox.InviteAndEarn;
import com.dating.chatbox.Models.ActivityModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;
import com.dating.chatbox.controllers.ActivityController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.ActivityResponse;
import com.dating.chatbox.db.DBUtil;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.NativeAd;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityFragment extends Fragment {


    private List<ActivityModel> listActivityModel = new ArrayList<>();
    private RecyclerView recyclerView;
    private ActivityListAdapter mAdapter;
    private ProgressBar progressBar;
    private User user = new User();
    private LinearLayout lvNoFavourites;
    private LinearLayout lvUnableToLoad;
    private NativeAd nativeAd;

    public ActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activityfragment, container, false);
        recyclerView =view.findViewById(R.id.recycler_view);
        progressBar =  view.findViewById(R.id.progressBar);
        lvNoFavourites =  view.findViewById(R.id.lvNoFavourites);
        lvNoFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), InviteAndEarn.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        lvUnableToLoad = view.findViewById(R.id.lvUnableToLoad);
        lvUnableToLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lvUnableToLoad.setVisibility(View.GONE);
                LoadMyActivities(true);
            }
        });
        LoadMyActivities(true);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        return view;
    }

    private void LoadMyActivities(Boolean showProgress) {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(getActivity());
        }
        if (showProgress) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }

        JSONObject jsonObject = new JSONObject();
        String url = "http://proacdoc.com/Service1.svc/GetAllUserActivitiesLists?UserID=" + DBUtil.getUSERID();
        ActivityController controller = new ActivityController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
            @Override
            public void handleResponse(ResponsePayload responseObject) {

                try {

                    if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof ActivityResponse) {
                        final ActivityResponse response = (ActivityResponse) responseObject.getResponseObject();

                        progressBar.setVisibility(View.INVISIBLE);
                        if (response.ListActivities.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                            lvNoFavourites.setVisibility(View.VISIBLE);

                        }
                        else {
                            recyclerView.setVisibility(View.VISIBLE);
                            lvNoFavourites.setVisibility(View.GONE);
                            mAdapter = new ActivityListAdapter(response.ListActivities, getActivity());
                            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                            recyclerView.setLayoutManager(gridLayoutManager);
                            recyclerView.setHasFixedSize(false);

                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(mAdapter);
                            if (listActivityModel.size() > 1) {
                                loadFBTopAd();
                            }
                        }


                    }

                } catch (Throwable e) {

                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        }, url);
        controller.setRequestObj(jsonObject);
        controller.sendRequest();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ActivityUpdate event) {
        LoadMyActivities(false);
    }

    ;

    private void loadFBTopAd() {
        final AdView adView = new AdView(getActivity(), "473707203053350_483153218775415", AdSize.BANNER_HEIGHT_50);
        adView.setAdListener(new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {
                ActivityModel activityModel = new ActivityModel();
                activityModel.setActivityIsAd(true);
                activityModel.setNativeAd(adView);
                listActivityModel.add(1, activityModel);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
        adView.loadAd();


    }
}
