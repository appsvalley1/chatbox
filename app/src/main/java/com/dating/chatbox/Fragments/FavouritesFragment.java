package com.dating.chatbox.Fragments;


import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Adapters.FavouriteListAdapter;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.InviteAndEarn;
import com.dating.chatbox.Models.FavouritesModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.R;
import com.dating.chatbox.db.DBUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavouritesFragment extends Fragment {

    public IntentFilter intentFilter;
    int FavouriteUserFrom;
    private List<FavouritesModel> favouriteUserList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FavouriteListAdapter mAdapter;
    private ProgressBar progressBar;
    private LinearLayout lvNoFavourites;
    private  LinearLayout lvUnableToLoad;
    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            final int position = viewHolder.getAdapterPosition();
            RemoveFavourite(favouriteUserList.get(position).getFavouriteuser().getId());
            favouriteUserList.remove(position);
            mAdapter.notifyDataSetChanged();
            if (favouriteUserList.size() == 0) {
                recyclerView.setVisibility(View.GONE);
                lvNoFavourites.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                lvNoFavourites.setVisibility(View.GONE);
            }


        }
    };


    public FavouritesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {


            @Override
            public void onBackground() {
                try {
                    FavouriteUserFrom=Integer.parseInt(DBUtil.getUSERID());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }



        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favouritefragment,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        lvNoFavourites = (LinearLayout) view.findViewById(R.id.lvNoFavourites);
        lvNoFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), InviteAndEarn.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        lvUnableToLoad= view.findViewById(R.id.lvUnableToLoad);
        lvUnableToLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lvUnableToLoad.setVisibility(View.GONE);
                LoadfavouriteUsers();
            }
        });
        LoadfavouriteUsers();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {



        super.onViewCreated(view, savedInstanceState);
    }

    private void LoadfavouriteUsers() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(getActivity());
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "http://proacdoc.com/Service1.svc/GetMyFavourites?UserID=" + DBUtil.getUSERID();

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            // Display the first 500 characters of the response string.

                            JSONArray jsonArray = new JSONArray(response);
                            favouriteUserList = new ArrayList<>();

                            if (jsonArray.length() == 0) {

                                recyclerView.setVisibility(View.GONE);
                                lvNoFavourites.setVisibility(View.VISIBLE);

                            } else {
                                recyclerView.setVisibility(View.VISIBLE);
                                lvNoFavourites.setVisibility(View.GONE);
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.optJSONObject(i);

                                    FavouritesModel favouritesModel = new FavouritesModel();
                                    favouritesModel.setFavouriteDated(jsonObject.optString("FavouriteDated"));
                                    favouritesModel.setFavouriteID(jsonObject.optInt("FavouriteID"));

                                    final User user = new User();
                                    JSONObject userJson = jsonObject.optJSONObject("FavouriteUserTO");

                                    user.setId(userJson.optInt("Id"));
                                    user.setName(userJson.optString("Name"));
                                    user.setShortBio(userJson.optString("ShortBio"));
                                    if (userJson.optString("Gender").equalsIgnoreCase("1")) {
                                        user.setGender("Male");
                                    } else {
                                        user.setGender("Female");
                                    }
                                    user.setViewsCount(userJson.optInt("ViewsCount"));

                                    user.setCityName(userJson.optString("CityName"));
                                    user.setShowOnlineStatus(userJson.optInt("ShowOnlineStatus"));
                                    user.setShowLocationStatus(userJson.optInt("ShowLocationStatus"));
                                    ArrayList<UserImages> userImages = new ArrayList<UserImages>();

                                    JSONArray userImagesarray = userJson.optJSONArray("userImages");

                                    for (int j = 0; j < userImagesarray.length(); j++) {


                                        JSONObject userImagejson = userImagesarray.optJSONObject(j);
                                        UserImages userImages1 = new UserImages();
                                        String image1 = userImagejson.optString("UserImageUrl");
                                        userImages1.setUserImageUrl(image1);

                                        //   userImages1.setUserImageUrl("https://24indianews.com/wp-content/uploads/2017/08/Jacqueline-Fernandez.jpg");
                                        userImages.add(userImages1);

                                    }
                                    user.setUserImages(userImages);
                                    user.setDistanceKM(userJson.optString("DistanceKM"));
                                    user.setBirthday(userJson.optString("Birthday") + " Years");
                                    favouritesModel.setFavouriteuser(user);
                                    favouriteUserList.add(favouritesModel);

                                }


                                mAdapter = new FavouriteListAdapter(favouriteUserList, getActivity());
                                LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                                recyclerView.setLayoutManager(gridLayoutManager);
                                recyclerView.setHasFixedSize(false);

                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(mAdapter);
                                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
                                itemTouchHelper.attachToRecyclerView(recyclerView);


                            }


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                lvUnableToLoad.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void RemoveFavourite( int FavouriteUserTO) {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(getActivity());
        }
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

            }
        });
        String url = Singleton.BaseUrl + "RemoveFavourite?FavouriteUserFrom=" + FavouriteUserFrom + "&FavouriteUserTO=" + FavouriteUserTO;

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }




}
