package com.dating.chatbox.Fragments;


import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Adapters.ChatsUsersListAdapter;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ChatsModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.R;
import com.dating.chatbox.db.DBUtil;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatUsersFragment extends Fragment {


    private List<ChatsModel> ChatsModelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ChatsUsersListAdapter mAdapter;
    private ProgressBar progressBar;
    private LinearLayout lvNoChats;
    private LinearLayout lvUnableToLoad;
    ChatManager chatmanager;
    public ChatUsersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.chatusersfragment, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        progressBar = view.findViewById(R.id.progressBar);
        lvNoChats = view.findViewById(R.id.lvNoChats);
        lvUnableToLoad= view.findViewById(R.id.lvUnableToLoad);
        lvUnableToLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lvUnableToLoad.setVisibility(View.GONE);
                loadUsersChats();
            }
        });
        loadUsersChats();
        chatmanager = ChatManager.getInstanceFor(ChatApplication.getInstance().conn2);
        chatmanager.addChatListener(new ChatManagerListener() {

            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {

                chat.addMessageListener(new ChatMessageListener() {

                    @Override
                    public void processMessage(Chat chat, final Message message) {
                        try {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {

                                    ChatsModel chatsModel = new ChatsModel();
                                    chatsModel.setChatText(message.getBody());
                                    chatsModel.setChatIsText(Integer.parseInt(message.getSubject()));
                                    chatsModel.setisSender(false);
                                    chatsModel.setFromServer(false);
                                    chatsModel.setChatUser(User.getUserProfile());
                                    String from[] = message.getFrom().split("@");
                                    if (mAdapter.getIntegerStringHashMap().get(Integer.parseInt(from[0])) != null) {
                                        final TextView updateText = mAdapter.getIntegerStringHashMap().get(Integer.parseInt(from[0])).chattext;
                                        updateText.setText(chatsModel.getChatText());
                                        Integer colorFrom = getResources().getColor(R.color.Black);
                                        Integer colorTo = getResources().getColor(R.color.greencolor);
                                        Integer colorTo1 = getResources().getColor(R.color.Black);
                                        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo, colorTo1);
                                        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                                            @Override
                                            public void onAnimationUpdate(ValueAnimator animator) {
                                                updateText.setTextColor((Integer) animator.getAnimatedValue());
                                            }

                                        });
                                        colorAnimation.start();
                                    }
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

            }
        });

        return view;
    }


    private void loadUsersChats() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(getActivity());
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "http://proacdoc.com/Service1.svc/GetMyChatUsers?ChatSenderUserId=" + DBUtil.getUSERID();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {


                            JSONArray jsonArray = new JSONArray(response);
                            ChatsModelList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                ChatsModel chatsModel = new ChatsModel();

                                JSONObject chat = jsonArray.optJSONObject(i);
                                chatsModel.setisSender(chat.optBoolean("isMainUser"));
                                chatsModel.setChatTime(Long.valueOf(chat.optString("ChatDated")));
                                chatsModel.setChatText(chat.optString("ChatMessageText"));
                                chatsModel.setChatIsText(chat.optInt("ChatIsText"));
                                JSONObject jsonObject = chat.optJSONObject("ChatRecieverUser");

                                final User user = new User();
                                user.setId(jsonObject.optInt("Id"));

                                ArrayList<UserImages> userImages = new ArrayList<UserImages>();

                                JSONArray userImagesarray = jsonObject.optJSONArray("userImages");

                                for (int j = 0; j < userImagesarray.length(); j++) {


                                    JSONObject userImagejson = userImagesarray.optJSONObject(j);
                                    UserImages userImages1 = new UserImages();
                                    String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + userImagejson.optString("UserImageUrl") + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                    userImages1.setUserImageUrl(image1);

                                    //   userImages1.setUserImageUrl("https://24indianews.com/wp-content/uploads/2017/08/Jacqueline-Fernandez.jpg");
                                    userImages.add(userImages1);

                                }
                                user.setUserImages(userImages);


                                user.setName(jsonObject.optString("Name"));
                                chatsModel.setChatUser(user);
                                ChatsModelList.add(chatsModel);


                            }
                            if (ChatsModelList.size() == 0) {
                                lvNoChats.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            } else {
                                lvNoChats.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                            }

                            mAdapter = new ChatsUsersListAdapter(ChatsModelList, getActivity());
                            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                            recyclerView.setLayoutManager(gridLayoutManager);
                            recyclerView.setHasFixedSize(false);

                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(mAdapter);


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                lvUnableToLoad.setVisibility(View.VISIBLE);

            }
        });

        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }




}
