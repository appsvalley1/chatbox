package com.dating.chatbox;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ResponseManager;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.db.DBUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class LoginNow extends AppCompatActivity implements Validator.ValidationListener {

    Validator validator;
    private LinearLayout lvLogin;
    @NotEmpty
    @Email
    private EditText EditTextEmail;
    @Password(min = 6, scheme = Password.Scheme.ANY)
    private EditText EditTextPassword;
    private ProgressBar progressBar;
    private Button startButton;
    private SessionManager sessionManager;
    private TextView textviewForgot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_now);

        sessionManager = new SessionManager(this);
        findViewsbyId();


        lvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(LoginNow.this, RegisterNow.class);
                startActivity(register);
                finish();
            }
        });
        validator = new Validator(this);
        validator.setValidationListener(this);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });


        textviewForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(LoginNow.this, ForgotPassword.class);
                startActivity(register);

            }
        });
    }

    @Override
    public void onValidationSucceeded() {

        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(LoginNow.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(LoginNow.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "AuthenticateUser?EmailAddress=" + EditTextEmail.getText().toString() + "&Password=" + EditTextPassword.getText().toString();
        url = url.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();
                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();
                            if (responseManager.getStatus() == 1) {
                            } else {

                                final JSONObject userJson = jsonObject.optJSONObject("User");
                                DBUtil.setUSERID(userJson.optString("Id"));
                                DBUtil.setInviteURL(userJson.optString("InviteUrl"));
                                DBUtil.setUserCity(userJson.optString("CityName"));
                                DBUtil.setName(userJson.optString("Name"));
                                DBUtil.setShortBio(userJson.optString("ShortBio"));
                                DBUtil.setShowOnlineStatus(userJson.optString("ShowOnlineStatus"));
                                DBUtil.setShowLocationStatus(userJson.optString("ShowLocationStatus"));
                                DBUtil.setShowOnlineStatus(userJson.optString("ShowOnlineStatus"));
                                DBUtil.setShowLocationStatus(userJson.optString("ShowLocationStatus"));
                                ArrayList<UserImages> userImages = new ArrayList<UserImages>();
                                JSONArray userImagesarray = userJson.optJSONArray("userImages");
                                for (int j = 0; j < userImagesarray.length(); j++) {
                                    JSONObject userImagejson = userImagesarray.optJSONObject(j);
                                    UserImages userImages1 = new UserImages();
                                    String imagelist = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + userImagejson.optString("UserImageUrl") + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                    userImages1.setUserImageUrl(imagelist);
                                    userImages.add(userImages1);

                                }
                                DBUtil.setUserImage(userImages.get(0).UserImageUrl);
                                if (userImagesarray.length() > 1) {
                                    DBUtil.setFirstSelfie("1");
                                }
                                sessionManager.SetRegistered(true);
                                sessionManager.SetisProfileInfoAdded(true);
                                if (userJson.optBoolean("HasPreference")) {
                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                        @Override
                                        public void onBackground() {
                                            try {
                                                JSONObject jsonObject1 = userJson.optJSONObject("User_Preferences");
                                                DBUtil.setGenderPreference(jsonObject1.optString("GenderPreference"));
                                                DBUtil.setMaxAgePreference(jsonObject1.optString("MaxAgePreference"));
                                                DBUtil.setMinAgePreference(jsonObject1.optString("MinAgePreference"));
                                                DBUtil.setUserPreferences("1");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }


                                    });
                                } else {
                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                        @Override
                                        public void onBackground() {
                                            try {
                                                DBUtil.setGenderPreference("3");
                                                DBUtil.setMaxAgePreference("60");
                                                DBUtil.setMinAgePreference("18");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }


                                    });
                                }

                                if (userJson.optString("Birthday").equalsIgnoreCase("")) {
                                    sessionManager.SetisProfileInfoAdded(false);
                                    Intent intent = new Intent(LoginNow.this, RegisterTwo.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();

                                } else {
                                    DBUtil.setAge(userJson.optString("Birthday"));
                                    sessionManager.SetisProfileInfoAdded(true);
                                    Intent intent = new Intent(LoginNow.this, MainActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                    finish();
                                }
                            }
                            progressBar.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }

        }

        );

// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void findViewsbyId() {
        lvLogin = (LinearLayout) findViewById(R.id.lvLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        EditTextEmail = (EditText) findViewById(R.id.EditTextEmail);
        EditTextPassword = (EditText) findViewById(R.id.EditTextPassword);
        startButton = (Button) findViewById(R.id.startButton);
        textviewForgot = (TextView) findViewById(R.id.textviewForgot);
    }

}
