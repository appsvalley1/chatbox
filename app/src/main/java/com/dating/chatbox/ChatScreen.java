package com.dating.chatbox;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Adapters.ChatsAdapter;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.UpdateXmppConnection;
import com.dating.chatbox.HelperClasses.ImagePicker;
import com.dating.chatbox.Models.ChatsModel;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.db.DBUtil;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.morsebyte.shailesh.twostagerating.TwoStageRate;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.DefaultExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class ChatScreen extends AppCompatActivity  {
    public TextView textViewLastTime, textViewNoChats;
    ChatManager chatmanager;
    RecyclerView recyclerView;
    ChatsAdapter mAdapter;
    ProgressBar progressBar;
    View rootView;
    EmojIconActions emojIcon;
    ImageView emoji_btn;
    Toolbar toolbar;
    FirebaseStorage storage;
    ProgressDialog pd;
    ProgressDialog pdConnetion;
    private EmojiconEditText EditTextMessage;
    private List<ChatsModel> chatList = new ArrayList<>();
    private TextView textviewStatus;
    private ImageView ImageUserStatus, userImage;
    private String MessageText = "";
    private ImageView sendMessageBtn, imgAttachment;
    private String UserImageUrl = "sassa";
    private MediaPlayer mediaPlayerBackgroundAudio;
    private InterstitialAd interstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_screen);
        Firebase.setAndroidContext(this);
        interstitialAd = new InterstitialAd(this, "473707203053350_479750762448994");
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {

            }

            @Override
            public void onError(Ad ad, AdError adError) {

            }

            @Override
            public void onAdLoaded(Ad ad) {
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });
        interstitialAd.loadAd();
        storage = FirebaseStorage.getInstance();
        findViewById();


        emojIcon = new EmojIconActions(this, rootView, EditTextMessage, emoji_btn);
        emojIcon.ShowEmojIcon();
        mAdapter = new ChatsAdapter(chatList, this);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getApplicationContext());
        gridLayoutManager.setStackFromEnd(true);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(false);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarchat, null);
        mCustomView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ChatScreen.this, UsersProfile.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        textviewStatus = (TextView) mCustomView.findViewById(R.id.textviewStatus);
        ImageUserStatus = (ImageView) mCustomView.findViewById(R.id.ImageUserStatus);
        userImage = (ImageView) mCustomView.findViewById(R.id.userImage);

        Glide.with(this)
                .load(User.getUserProfile().getUserImages().get(0).getUserImageUrl()).dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).bitmapTransform(new CropCircleTransformation(this))
                .into(userImage);
        toolbar.addView(mCustomView);
        TextView username = (TextView) mCustomView.findViewById(R.id.username);
        username.setText(User.getUserProfile().getName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadUsersChats();


        if (User.getUserProfile().getShowOnlineStatus() == 0) {
            Glide.with(getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
            textviewStatus.setText("Offline");
        } else {
            loadUserStatus();
        }

        if (ChatApplication.getInstance().conn2 != null && ChatApplication.getInstance().conn2.isConnected()) {
            loadinitialChatScreen();

        } else {
            pdConnetion = new ProgressDialog(ChatScreen.this);
            pdConnetion.setMessage("Connecting...");
            pdConnetion.setCancelable(false);
            pdConnetion.show();
            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                   @Override
                                                   public void onBackground() {
                                                       Intent startXmppConnectionService = new Intent(getApplicationContext(), StartXmppConnectionService.class);
                                                       startService(startXmppConnectionService);

                                                   }


                                               }

            );

        }

        try {
            if (!EventBus.getDefault().isRegistered(this)) { EventBus.getDefault().register(this); }
        } catch (Throwable e) {

        }


    }

    private void findViewById() {
        EditTextMessage = findViewById(R.id.EditTextMessage);
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        textViewLastTime = findViewById(R.id.textViewLastTime);
        textViewNoChats = findViewById(R.id.textViewNoChats);
        sendMessageBtn = findViewById(R.id.sendMessageBtn);
        rootView = findViewById(R.id.root_view);
        emoji_btn = findViewById(R.id.emoji_btn);
        imgAttachment = findViewById(R.id.imgAttachment);
        imgAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
              /*  PopupMenu popup = new PopupMenu(ChatScreen.this, view);
                popup.setOnMenuItemClickListener(ChatScreen.this);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.chatmediaoptions, popup.getMenu());
                popup.show();*/
            }
        });

    }

    public void send(View v) {
        if (ChatApplication.getInstance().conn2 != null && ChatApplication.getInstance().conn2.isConnected()) {


        } else {
            pdConnetion = new ProgressDialog(ChatScreen.this);
            pdConnetion.setMessage("Connecting...");
            pdConnetion.setCancelable(false);
            pdConnetion.show();
            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                   @Override
                                                   public void onBackground() {
                                                       Intent startXmppConnectionService = new Intent(getApplicationContext(), StartXmppConnectionService.class);
                                                       startService(startXmppConnectionService);
                                                   }


                                               }

            );

        }
        if (EditTextMessage.getText().toString().equalsIgnoreCase("")) {
            return;
        }
        ScaleAnimation scaleSendBtn = new ScaleAnimation(1f, 0.8f, 1f, 0.8f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleSendBtn.setDuration(200);
        scaleSendBtn.setFillAfter(false);
        scaleSendBtn.setRepeatMode(Animation.REVERSE);
        sendMessageBtn.startAnimation(scaleSendBtn);
        sendmessage(EditTextMessage.getText().toString(), String.valueOf(User.getUserProfile().getId()) + "@52.41.229.242", 0);
        MessageText = EditTextMessage.getText().toString();
        SendChatToServer(0);
        EditTextMessage.getText().clear();

        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {

            @Override
            public void onBackground() {
                try {
                    mediaPlayerBackgroundAudio = MediaPlayer.create(getApplicationContext(), R.raw.tap);
                    mediaPlayerBackgroundAudio.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            mediaPlayerBackgroundAudio.start();
                        }
                    });
                } catch (Throwable e) {

                }

            }

        });


    }

    public void listenChat() {

        chatmanager.addChatListener(new ChatManagerListener() {

            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {

                chat.addMessageListener(new ChatMessageListener() {

                    @Override
                    public void processMessage(Chat chat, final Message message) {
                        ChatScreen.this.runOnUiThread(new Runnable() {
                            public void run() {

                                ChatsModel chatsModel = new ChatsModel();
                                chatsModel.setChatText(message.getBody());
                                chatsModel.setChatIsText(Integer.parseInt(message.getSubject()));
                                chatsModel.setisSender(false);
                                chatsModel.setFromServer(false);
                                chatsModel.setChatUser(User.getUserProfile());
                                chatList.add(chatsModel);
                                mAdapter.notifyDataSetChanged();
                                textViewNoChats.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                recyclerView.scrollToPosition(chatList.size() - 1);
                            }
                        });


                    }
                });

            }
        });
    }

    public boolean sendmessage(String body, String destinationuser, int ChatIsText) {

        try {
            String from = DBUtil.getUSERID() + "@52.41.229.242";
            Message message = new Message(destinationuser, Message.Type.chat);
            message.setFrom(from);
            message.setBody(body);
            message.setSubject(String.valueOf(ChatIsText));
            message.addExtension(new DefaultExtensionElement("from", from));
            message.addExtension(new DefaultExtensionElement("to", destinationuser));
            ChatApplication.getInstance().conn2.sendStanza(message);
            ChatsModel chatsModel = new ChatsModel();
            if (ChatIsText == 0) {
                chatsModel.setChatText(EditTextMessage.getText().toString());
            } else {
                chatsModel.setChatText(body);
            }


            long time = System.currentTimeMillis();
            chatsModel.setChatTime(time);
            chatsModel.setisSender(true);
            chatsModel.setChatIsText(ChatIsText);
            chatsModel.setFromServer(false);
            chatList.add(chatsModel);
            mAdapter.notifyDataSetChanged();
            textViewNoChats.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        try {
            Presence localPresence = new Presence(Presence.Type.unavailable);
            localPresence.setStatus("Offline");
            localPresence.setPriority(24);
            ChatApplication.getInstance().conn2.sendStanza(localPresence);
        } catch (Exception localException) {

        }
        super.onBackPressed();
    }

    private void loadUsersChats() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(ChatScreen.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(ChatScreen.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "http://proacdoc.com/Service1.svc/GetMyChats?ChatRecieverUserId=" + User.getUserProfile().getId() + "&ChatSenderUserId=" + DBUtil.getUSERID();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            chatList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                ChatsModel chatsModel = new ChatsModel();

                                JSONObject chat = jsonArray.optJSONObject(i);
                                chatsModel.setisSender(chat.optBoolean("isMainUser"));
                                chatsModel.setChatTime(Long.valueOf(chat.optString("ChatDated")));
                                chatsModel.setChatText(chat.optString("ChatMessageText"));
                                chatsModel.setChatIsText(chat.optInt("ChatIsText"));
                                chatsModel.setFromServer(true);
                                chatList.add(chatsModel);


                            }
                            if (chatList.size() == 0) {
                                textViewNoChats.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.INVISIBLE);
                            }


                            mAdapter = new ChatsAdapter(chatList, ChatScreen.this);
                            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getApplicationContext());
                            gridLayoutManager.setStackFromEnd(true);
                            recyclerView.setLayoutManager(gridLayoutManager);
                            recyclerView.setHasFixedSize(false);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(mAdapter);


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });

        queue.add(stringRequest);
    }

    public void SendChatToServer(int ChatIsText) {
        // progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(ChatScreen.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                //    progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "";
        switch (ChatIsText) {
            case 0:
                url = "http://proacdoc.com/Service1.svc/AddChat?ChatRecieverUserId=" + User.getUserProfile().getId() + "&ChatSenderUserId=" + DBUtil.getUSERID() + "&ChatMessageText=" + MessageText + "&ChatIsText=" + ChatIsText + "&ChatDated=" + System.currentTimeMillis();
                url = url.replaceAll(" ", "%20");
                break;
            case 1:
                url = "http://proacdoc.com/Service1.svc/AddChat?ChatRecieverUserId=" + User.getUserProfile().getId() + "&ChatSenderUserId=" + DBUtil.getUSERID() + "&ChatMessageText=" + UserImageUrl + "&ChatIsText=" + ChatIsText + "&ChatDated=" + System.currentTimeMillis();
                url = url.replaceAll(" ", "%20");
                break;
        }


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        MessageText = "";


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //   progressBar.setVisibility(View.VISIBLE);

            }
        });

        queue.add(stringRequest);
    }

    private void loadUserStatus() {
        RequestQueue queue = Volley.newRequestQueue(ChatScreen.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                //    progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "http://52.41.229.242:9090/plugins/presence/status?jid=" + User.getUserProfile().getId() + "@52.41.229.242&type=text";
        url = url.replaceAll(" ", "%20");


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        response = response.trim();
                        if (response.equalsIgnoreCase("null")) {
                            response = "offline";
                        }
                        textviewStatus.setText(response);
                        if (response.equalsIgnoreCase("Online Now")) {

                            Glide.with(getApplicationContext()).load(R.drawable.greenonline).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                        } else {
                            Glide.with(getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //   progressBar.setVisibility(View.VISIBLE);

            }
        });

        queue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chatscreen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_add:
                AddToFavourites();
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void AddToFavourites() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(ChatScreen.this);
        }
        progressBar.setVisibility(View.VISIBLE);


        RequestQueue queue = Volley.newRequestQueue(this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

            }
        });
        String url = "http://www.proacdoc.com/Service1.svc/AddToFavourite?FavouriteUserFrom=" + DBUtil.getUSERID() + "&FavouriteUserTO=" + User.getUserProfile().getId() + "&FavouriteDated=" + System.currentTimeMillis();

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), User.getUserProfile().getName() + " " + "Added To favourites", Toast.LENGTH_SHORT).show();

                        TwoStageRate.with(ChatScreen.this).showRatePromptDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_PICK) {
            ImagePicker.beginCrop(this, resultCode, data);
        } else if (requestCode == ImagePicker.REQUEST_CROP) {
            Bitmap bitmap = ImagePicker.getImageCropped(this, resultCode, data,
                    ImagePicker.ResizeType.FIXED_SIZE, 500);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] b = baos.toByteArray();

            try {
                pd = new ProgressDialog(ChatScreen.this);
                pd.setMessage("Uploading Image...");
                pd.setCancelable(false);
                pd.show();
                StorageReference storageRef = storage.getReferenceFromUrl("gs://chatbox-8abc4.appspot.com");    //change the url according to your firebase app
                final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                final StorageReference riversRef = storageRef.child("/" + timeStamp + ".jpg");
                riversRef.putBytes(b)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                                //if the upload is successfull
                                //hiding the progress dialog


                                //and displaying a success toast
                                //          isImageUploaded = true;
                                pd.dismiss();

                                riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        UserImageUrl = timeStamp + ".jpg";
                                        ScaleAnimation scaleSendBtn = new ScaleAnimation(1f, 0.8f, 1f, 0.8f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                        scaleSendBtn.setDuration(200);
                                        // animation duration in milliseconds
                                        scaleSendBtn.setFillAfter(false);    // If fillAfter is true, the transformation that this animation performed will persist when it is finished.
                                        scaleSendBtn.setRepeatMode(Animation.REVERSE);
                                        sendMessageBtn.startAnimation(scaleSendBtn);

                                        sendmessage(UserImageUrl, String.valueOf(User.getUserProfile().getId()) + "@52.41.229.242", 1);
                                        MessageText = EditTextMessage.getText().toString();
                                        SendChatToServer(1);
                                        EditTextMessage.getText().clear();
                                        TwoStageRate.with(ChatScreen.this).showRatePromptDialog();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle any errors
                                        pd.dismiss();
                                    }
                                });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                //if the upload is not successfull
                                //hiding the progress dialog
                                pd.dismiss();

                                //and displaying error message
                                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void loadinitialChatScreen() {
        chatmanager = ChatManager.getInstanceFor(ChatApplication.getInstance().conn2);
        Presence localPresence = new Presence(Presence.Type.available);
        localPresence.setStatus("Online Now");
        localPresence.setPriority(24);
        try {
            ChatApplication.getInstance().conn2.sendStanza(localPresence);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        listenChat();


        getRoaster();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();

    }
    public void getRoaster() {
      try {
          Roster roster = ChatApplication.getInstance().roster;
          roster.createEntry(String.valueOf(User.getUserProfile().getId()) + "@52.41.229.242", User.getUserProfile().getName(), null);
          roster.addRosterListener(new RosterListener() {
              // Ignored events public void entriesAdded(Collection<String> addresses) {}
              public void entriesDeleted(Collection<String> addresses) {
              }

              @Override
              public void entriesAdded(Collection<String> addresses) {

              }

              public void entriesUpdated(Collection<String> addresses) {
              }

              public void presenceChanged(Presence presence) {
                  String  from[]=presence.getFrom().split("@");
                  if(from[0].equalsIgnoreCase(String.valueOf(User.getUserProfile().getId()))) {
                      if (presence.getType() == Presence.Type.available) {
                          runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  textviewStatus.setText("Online Now");
                                  Glide.with(getApplicationContext()).load(R.drawable.greenonline).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                              }
                          });

                      } else {
                          runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  textviewStatus.setText("Offline");
                                  Glide.with(getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                              }
                          });
                      }
                  }

                }
          });
      }
      catch (Throwable e)
      {

      }
    }




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UpdateXmppConnection event) {
        if(pdConnetion!=null)
        {  pdConnetion.dismiss();}

        loadinitialChatScreen();
    };



}
