package com.dating.chatbox;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;


import com.dating.chatbox.Custom.SessionManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Splash extends AppCompatActivity {
    ImageView imageView;


    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        printHashKey(getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        sessionManager = new SessionManager(this);
        findViewById();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                RedirectUser();
            }
        }, 500);



    }

    private void findViewById() {
        imageView = (ImageView) findViewById(R.id.imageView);
    }


    private void RedirectUser() {
        if (sessionManager.GetPrivacyClicked() == false) {
            if (Build.VERSION.SDK_INT >= 21) {
                Intent intent = new Intent(Splash.this, PrivacyPolicy.class);
                String transitionName = getString(R.string.imageViewSplash);

                ActivityOptionsCompat options =

                        ActivityOptionsCompat.makeSceneTransitionAnimation(Splash.this,
                                imageView,
                                transitionName
                        );

                ActivityCompat.startActivity(Splash.this, intent, options.toBundle());

            } else {
                Intent intent = new Intent(Splash.this, PrivacyPolicy.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

                finish();
            }
        } else {
            if (sessionManager.GetRegistered() == false) {
                Intent intent = new Intent(Splash.this, RegisterNow.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            } else {

                Intent intent = new Intent(Splash.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();


            }

        }

    }
    public  void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("", "printHashKey()", e);
        }
    }


}
