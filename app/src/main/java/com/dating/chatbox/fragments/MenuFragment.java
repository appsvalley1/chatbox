package com.dating.chatbox.Fragments;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.BuildConfig;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.EventbusLib.UpdateProfileImage;
import com.dating.chatbox.R;
import com.dating.chatbox.db.DBUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


/**
 * Created by USER on 3/13/2018.
 */


public class MenuFragment extends Fragment implements View.OnClickListener {
    private Callbacks callbacks;
    private ImageView userImage;
    private TextView dob, name;
    public MenuFragment() {
    }

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.menufragment, null);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        LinearLayout llHeader = root.findViewById(R.id.llHeader);
        dob = root.findViewById(R.id.dob);
        name = root.findViewById(R.id.name);
        userImage = root.findViewById(R.id.userImage);
        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
            public String ImageUrl, Name, Birthday;
            @Override
            public void onBackground() {
                try {

                    ImageUrl = DBUtil.getUserImage();
                    Name = DBUtil.getName();
                    Birthday = DBUtil.getAge();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                super.onUi();
                try {
                    Glide.with(getActivity())
                            .load(ImageUrl).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).bitmapTransform(new CropCircleTransformation(getActivity()))
                            .into(userImage);
                    name.setText(Name);
                    dob.setText(Birthday + " Years");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });


        LinearLayout lvPhoto = root.findViewById(R.id.lvPhoto);
        LinearLayout lvHome = root.findViewById(R.id.lvHome);
        LinearLayout lvActivity = root.findViewById(R.id.lvActivity);
        LinearLayout lvChats = root.findViewById(R.id.lvChats);
        LinearLayout lvFavourites = root.findViewById(R.id.lvFavourites);
        LinearLayout lvInviteBoost = root.findViewById(R.id.lvInviteBoost);
        LinearLayout lvSettings = root.findViewById(R.id.lvSettings);
        LinearLayout lvChangePassword = root.findViewById(R.id.lvChangePassword);
        LinearLayout lvTerms = root.findViewById(R.id.lvTerms);
        LinearLayout lvFacebook = root.findViewById(R.id.lvFacebook);
        LinearLayout lvTwitter = root.findViewById(R.id.lvTwitter);
        LinearLayout lvInstagram = root.findViewById(R.id.lvInstagram);

        LinearLayout lvMaps = root.findViewById(R.id.lvMaps);
        TextView textVersion = root.findViewById(R.id.textVersion);
        try {
            textVersion.setText(("ChatSe v(" + BuildConfig.VERSION_NAME + ")"));
        } catch (Throwable e) {
            e.printStackTrace();
        }

        try {

            lvPhoto.setOnClickListener(this);
            llHeader.setOnClickListener(this);
            lvHome.setOnClickListener(this);
            lvActivity.setOnClickListener(this);
            lvChats.setOnClickListener(this);
            lvFavourites.setOnClickListener(this);
            lvInviteBoost.setOnClickListener(this);
            lvTwitter.setOnClickListener(this);
            lvFacebook.setOnClickListener(this);
            lvSettings.setOnClickListener(this);
            lvInstagram.setOnClickListener(this);
            lvChangePassword.setOnClickListener(this);
            lvTerms.setOnClickListener(this);
            textVersion.setOnClickListener(this);
            lvMaps.setOnClickListener(this);
        } catch (Throwable e) {

        }

        ImageView ivHome = root.findViewById(R.id.ivHome);
        Glide.with(getActivity())
                .load(R.drawable.homenavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivHome);

        ImageView ivActivity = root.findViewById(R.id.ivActivity);
        Glide.with(getActivity())
                .load(R.drawable.activitynavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivActivity);

        ImageView ivChats = root.findViewById(R.id.ivChats);
        Glide.with(getActivity())
                .load(R.drawable.chatnavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivChats);


        ImageView ivFavourites = root.findViewById(R.id.ivFavourites);
        Glide.with(getActivity())
                .load(R.drawable.favoritenavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivFavourites);

        ImageView ivinvite = root.findViewById(R.id.ivinvite);
        Glide.with(getActivity())
                .load(R.drawable.invitenavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivinvite);


        ImageView ivSettings = root.findViewById(R.id.ivSettings);
        Glide.with(getActivity())
                .load(R.drawable.settingsnavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivSettings);


        ImageView ivChangePassword = root.findViewById(R.id.ivChangePassword);
        Glide.with(getActivity())
                .load(R.drawable.passwordnav).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivChangePassword);


        ImageView ivterms = root.findViewById(R.id.ivterms);
        Glide.with(getActivity())
                .load(R.drawable.privacynavigation).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivterms);


        ImageView ivfacebook = root.findViewById(R.id.ivfacebook);
        Glide.with(getActivity())
                .load(R.drawable.facebooknav).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivfacebook);


        ImageView ivTwitter = root.findViewById(R.id.ivTwitter);


        Glide.with(getActivity())
                .load(R.drawable.twitternav).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivTwitter);


        ImageView ivinsta = root.findViewById(R.id.ivinsta);


        Glide.with(getActivity())
                .load(R.drawable.instagramnav).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivinsta);



        ImageView ivmaps = root.findViewById(R.id.ivmaps);
        Glide.with(getActivity())
                .load(R.drawable.mapsmenu).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivmaps);


        return root;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnItemClickedListener");
        }
    }


    @Override
    public void onClick(View view) {
        try {
            callbacks.onMenuClicked(view.getId());
        } catch (Throwable e) {
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public interface Callbacks {
        void onMenuClicked(int masterItemId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UpdateProfileImage event) {
        try {
            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                public String ImageUrl;

                @Override
                public void onBackground() {
                    try {

                        ImageUrl = DBUtil.getUserImage();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onUi() {
                    super.onUi();
                    try {


                        Glide.with(getActivity())
                                .load(ImageUrl).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).bitmapTransform(new CropCircleTransformation(getActivity()))
                                .into(userImage);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();

        }


    }


}
