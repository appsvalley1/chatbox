package com.dating.chatbox.Fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Adapters.HomeLveUserListAdapter;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.GoLive.ConstantApp;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.R;
import com.dating.chatbox.StartBroadcastingActivity;
import com.dating.chatbox.controllers.HomeLiveUsersController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.HomeLiveUsersResponse;
import com.dating.chatbox.db.DBUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.List;

import io.agora.rtc.Constants;


/**
 * A simple {@link Fragment} subclass.
 */
public class LiveBroadcastersFragment extends Fragment {


    View view;

    private RecyclerView recycler_viewLive;

    private HomeLveUserListAdapter liveUsersAdapter;

    private SwipeRefreshLayout swipeContainer;
private LinearLayout lvNoUserFound;
    private LinearLayout fab;
    public LiveBroadcastersFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmentbroadcastings_users, container, false);
        lvNoUserFound= view.findViewById(R.id.lvNoUserFound);
        fab= view.findViewById(R.id.fab);
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(fab, "alpha",  1f, .3f);
        fadeOut.setDuration(700);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(fab, "alpha", .3f, 1f);
        fadeIn.setDuration(700);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });
        mAnimationSet.start();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateUsersForLive();
                Intent i = new Intent(getActivity(), StartBroadcastingActivity.class);
                i.putExtra(ConstantApp.ACTION_KEY_CROLE, Constants.CLIENT_ROLE_BROADCASTER);
                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, DBUtil.getUSERID());
                startActivity(i);
            }
        });
        recycler_viewLive = view.findViewById(R.id.recycler_viewLive);
        loadLiveUsers();
        swipeContainer = (SwipeRefreshLayout)view. findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadLiveUsers();

            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        return view;
    }


    private void UpdateUsersForLive() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

            }
        });
        String url = Singleton.BaseUrl + "SendNoificationForLive?UserId=" + DBUtil.getUSERID();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
    private void BindLiveUsersData(List<User> usersList) {


        if (usersList != null && usersList.size() > 0) {
            recycler_viewLive.setVisibility(View.VISIBLE);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
            recycler_viewLive.setLayoutManager(gridLayoutManager);
            recycler_viewLive.setHasFixedSize(false);
            liveUsersAdapter = new HomeLveUserListAdapter(usersList, getActivity());
            recycler_viewLive.setAdapter(liveUsersAdapter);
        }
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    private void loadLiveUsers() {
        JSONObject jsonObject = new JSONObject();
        String url = "http://proacdoc.com/Service1.svc/GetAllBroadcastingUser?UserID=" + DBUtil.getUSERID();
        HomeLiveUsersController controller = new HomeLiveUsersController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
            @Override
            public void handleResponse(ResponsePayload responseObject) {

                try {

                    if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof HomeLiveUsersResponse) {
                        final HomeLiveUsersResponse response = (HomeLiveUsersResponse) responseObject.getResponseObject();
                        swipeContainer.setRefreshing(false);

                        if (response.UserList.size() == 0) {
                            lvNoUserFound.setVisibility(View.VISIBLE);

                        } else {
                            lvNoUserFound.setVisibility(View.GONE);
                            BindLiveUsersData(response.UserList);

                        }

                    } else {

                    }
                } catch (Throwable e) {


                }
            }
        }, url);
        controller.setRequestObj(jsonObject);
        controller.sendRequest();
    }


}
