package com.dating.chatbox.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class QurekaProvider extends ContentProvider {

    // The URI Matcher used by this content provider.

    //    Preference
    static final int PREF_GET_VALUE = 100;
    static final int PREF_SET_VALUE = 101;


    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private QurekaDbHelper mOpenHelper;

    static UriMatcher buildUriMatcher() {
        UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        sUriMatcher.addURI(QurekaContract.CONTENT_AUTHORITY,
                QurekaContract.PreferenceTable.TABLE_NAME + "/getPValue", PREF_GET_VALUE);

        sUriMatcher.addURI(QurekaContract.CONTENT_AUTHORITY,
                QurekaContract.PreferenceTable.TABLE_NAME + "/setPValue", PREF_SET_VALUE);

        return sUriMatcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new QurekaDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        Cursor retCursor;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PREF_GET_VALUE: {
                retCursor = mOpenHelper.getReadableDatabase().query(QurekaContract.PreferenceTable.TABLE_NAME, null,
                        selection, selectionArgs, null, null, null);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PREF_GET_VALUE:
                return QurekaContract.PreferenceTable.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PREF_SET_VALUE: {
                db.insertWithOnConflict(QurekaContract.PreferenceTable.TABLE_NAME, null,
                        values, SQLiteDatabase.CONFLICT_REPLACE);
                break;
            }


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}