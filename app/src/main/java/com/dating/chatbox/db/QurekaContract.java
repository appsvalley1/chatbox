package com.dating.chatbox.db;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

class QurekaContract {
    public static final String CONTENT_AUTHORITY = "com.dating.chatbox";

    static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    /****************
     * Preference Table
     *******************/

    static final class PreferenceTable implements BaseColumns {

        static final String TABLE_NAME = "prefTable";
        static final String KEY = "key";
        static final String VALUE = "value";
        static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TABLE_NAME;

        static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        static Uri getPrefValue() {
            return CONTENT_URI.buildUpon().appendPath("getPValue").build();
        }

        static Uri setPrefValue() {
            return CONTENT_URI.buildUpon().appendPath("setPValue").build();
        }
    }
}