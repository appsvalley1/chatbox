package com.dating.chatbox.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class QurekaDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "chatbox.db";
    private static final int DATABASE_VERSION = 1;

    QurekaDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createPrefTable(db);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createPrefTable(SQLiteDatabase db) {
        final String CREATE_PREF_TABLE = "CREATE TABLE "
                + QurekaContract.PreferenceTable.TABLE_NAME +
                "(" +
                QurekaContract.PreferenceTable.KEY + " TEXT UNIQUE," +
                QurekaContract.PreferenceTable.VALUE + " TEXT" +
                ")";
        db.execSQL(CREATE_PREF_TABLE);
    }
}