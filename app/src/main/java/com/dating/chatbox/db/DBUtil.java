package com.dating.chatbox.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.dating.chatbox.ChatApplication;


public class DBUtil {
    private static final String User_PREFERENCES = "User_PREFERENCES";
    private static final String MinAgePreference = "MinAgePreference";
    private static final String MaxAgePreference = "MaxAgePreference";
    private static final String GenderPreference = "GenderPreference";
    private static final String PopularityPreference = "PopularityPreference";
    private static final String USER_CITY = "USER_CITY";
    private static final String USERID = "USERID";
    private static final String Latitude = "Latitude";
    private static final String Longitude = "Longitude";
    private static final String InviteURL = "InviteURL";
    private static final String Name = "Name";
    private static final String Age = "Age";
    private static final String Gender = "Gender";
    private static final String ShortBio = "ShortBio";
    private static final String UserImage = "UserImage";
    private static final String ShowOnlineStatus = "ShowOnlineStatus";
    private static final String ShowLocationStatus = "ShowLocationStatus";
    private static final String ChatSoundStatus = "ChatSoundStatus";
    private static final String AppSound = "AppSound";
    private static final String ChatMessageNotification = "ChatMessageNotification";
    private static final String NotificationSound = "NotificationSound";
    private static final String Vibrations = "Vibrations";
    private static final String MessengingID = "MessengingID";
    private static final String InviteText = "InviteText";
    private static final String InviteTitle = "InviteTitle";
    private static final String ChatCount = "ChatCount";
    private static final String FirstSelfie = "FirstSelfie";
    private static final String ActivityCount = "ActivityCount";

    private static String getDataFromDb(String key) {
        Cursor data = null;
        try {
            Uri uri1 = QurekaContract.PreferenceTable.getPrefValue();
            String selection = QurekaContract.PreferenceTable.KEY + "=?";
            data = ChatApplication.getInstance().getContentResolver().query(uri1, null,
                    selection, new String[]{key}, null);
            if (data != null) {
                while (data.moveToNext()) {
                    if (data.getColumnIndex(QurekaContract.PreferenceTable.VALUE) != -1) {
                        int index = data.getColumnIndexOrThrow(QurekaContract.PreferenceTable.VALUE);
                        return (data.getString(index));
                    }
                }
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            if (data != null) {
                data.close();
            }
        }
    }

    private static boolean getDataFromDbs(String key) {
        Cursor data = null;
        try {
            Uri uri1 = QurekaContract.PreferenceTable.getPrefValue();
            String selection = QurekaContract.PreferenceTable.KEY + "=?";
            data = ChatApplication.getInstance().getContentResolver().query(uri1, null,
                    selection, new String[]{key}, null);
            if (data != null) {
                while (data.moveToNext()) {
                    if (data.getColumnIndex(QurekaContract.PreferenceTable.VALUE) != -1) {
                        int index = data.getColumnIndexOrThrow(QurekaContract.PreferenceTable.VALUE);
                        return data.getInt(index) > 0;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (data != null) {
                data.close();
            }
        }
    }

    private static void setDataForDb(String key, String value) {
        try {
            Uri mInstallUri = QurekaContract.PreferenceTable.setPrefValue();
            ContentValues cv = new ContentValues();
            cv.put(QurekaContract.PreferenceTable.KEY, key);
            cv.put(QurekaContract.PreferenceTable.VALUE, value);
            ChatApplication.getInstance().getContentResolver().insert(
                    mInstallUri, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void setDataForDbs(String key, boolean value) {
        try {
            Uri mInstallUri = QurekaContract.PreferenceTable.setPrefValue();
            ContentValues cv = new ContentValues();
            cv.put(QurekaContract.PreferenceTable.KEY, key);
            cv.put(QurekaContract.PreferenceTable.VALUE, value);
            ChatApplication.getInstance().getContentResolver().insert(
                    mInstallUri, cv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getUser_Preferences() {
        return getDataFromDb(User_PREFERENCES);
    }

    public static void setUserPreferences(String userName) {
        setDataForDb(User_PREFERENCES, userName);
    }


    public static String getMinAgePreference() {
        return getDataFromDb(MinAgePreference);
    }

    public static void setMinAgePreference(String minAgePreference) {
        setDataForDb(MinAgePreference, minAgePreference);
    }


    public static String getMaxAgePreference() {
        return getDataFromDb(MaxAgePreference);
    }

    public static void setMaxAgePreference(String maxAgePreference) {
        setDataForDb(MaxAgePreference, maxAgePreference);
    }


    public static String getGenderPreference() {
        return getDataFromDb(GenderPreference);
    }

    public static void setGenderPreference(String genderPreference) {
        setDataForDb(GenderPreference, genderPreference);
    }


    public static String getPopularityPreference() {
        return getDataFromDb(PopularityPreference);
    }

    public static void setPopularityPreference(String popularityPreference) {
        setDataForDb(PopularityPreference, popularityPreference);
    }

    public static String getUserCity() {
        return getDataFromDb(USER_CITY);
    }

    public static void setUserCity(String userCity) {
        setDataForDb(USER_CITY, userCity);
    }


    public static String getUSERID() {
        return getDataFromDb(USERID);
    }

    public static void setUSERID(String _USERID) {
        setDataForDb(USERID, _USERID);
    }


    public static String getLatitude() {
        return getDataFromDb(Latitude);
    }

    public static void setLatitude(String _Latitude) {
        setDataForDb(Latitude, _Latitude);
    }


    public static String getLongitude() {
        return getDataFromDb(Longitude);
    }

    public static void setLongitude(String _Longitude) {
        setDataForDb(Longitude, _Longitude);
    }


    public static String getInviteURL() {
        return getDataFromDb(InviteURL);
    }

    public static void setInviteURL(String _InviteURL) {
        setDataForDb(InviteURL, _InviteURL);
    }

    public static String getName() {
        return getDataFromDb(Name);
    }

    public static void setName(String _Name) {
        setDataForDb(Name, _Name);
    }


    public static String getAge() {
        return getDataFromDb(Age);
    }

    public static void setAge(String _Age) {
        setDataForDb(Age, _Age);
    }


    public static String getGender() {
        return getDataFromDb(Gender);
    }

    public static void setGender(String _Gender) {
        setDataForDb(Gender, _Gender);
    }


    public static String getUserImage() {
        return getDataFromDb(UserImage);
    }

    public static void setUserImage(String _UserImage) {
        setDataForDb(UserImage, _UserImage);
    }

    public static String getShortBio() {
        return getDataFromDb(ShortBio);
    }

    public static void setShortBio(String _ShortBio) {
        setDataForDb(ShortBio, _ShortBio);
    }


    public static String getShowOnlineStatus() {
        return getDataFromDb(ShowOnlineStatus);
    }

    public static void setShowOnlineStatus(String _ShowOnlineStatus) {
        setDataForDb(ShowOnlineStatus, _ShowOnlineStatus);
    }


    public static String getShowLocationStatus() {
        return getDataFromDb(ShowLocationStatus);
    }

    public static void setShowLocationStatus(String _ShowLocationStatus) {
        setDataForDb(ShowLocationStatus, _ShowLocationStatus);
    }


    public static String getChatSoundStatus() {
        return getDataFromDb(ChatSoundStatus);
    }

    public static void setChatSoundStatus(String _ChatSoundStatus) {
        setDataForDb(ChatSoundStatus, _ChatSoundStatus);
    }


    public static String getAppSound() {
        return getDataFromDb(AppSound);
    }

    public static void setAppSound(String _AppSound) {
        setDataForDb(AppSound, _AppSound);
    }


    public static String getChatMessageNotification() {
        return getDataFromDb(ChatMessageNotification);
    }

    public static void setChatMessageNotification(String _ChatMessageNotification) {
        setDataForDb(ChatMessageNotification, _ChatMessageNotification);
    }


    public static String getNotificationSound() {
        return getDataFromDb(NotificationSound);
    }

    public static void setNotificationSound(String _NotificationSound) {
        setDataForDb(NotificationSound, _NotificationSound);
    }


    public static String getVibrations() {
        return getDataFromDb(Vibrations);
    }

    public static void setVibrations(String _Vibrations) {
        setDataForDb(Vibrations, _Vibrations);
    }


    public static String getMessengingID() {
        return getDataFromDb(MessengingID);
    }

    public static void setMessengingID(String _MessengingID) {
        setDataForDb(MessengingID, _MessengingID);
    }


    public static String getInviteText() {
        return getDataFromDb(InviteText);
    }

    public static void setInviteText(String _InviteText) {
        setDataForDb(InviteText, _InviteText);
    }


    public static String getInviteTitle() {
        return getDataFromDb(InviteTitle);
    }

    public static void setInviteTitle(String _InviteTitle) {
        setDataForDb(InviteTitle, _InviteTitle);
    }


    public static int getChatCount() {
        try {
            return Integer.parseInt(getDataFromDb(ChatCount));
        } catch (Exception e) {
        }
        return 0;
    }

    public static void setChatCount(int chatCount) {
        setDataForDb(ChatCount, "" + chatCount);
    }

    public static String getFirstSelfie() {
        return getDataFromDb(FirstSelfie);
    }

    public static void setFirstSelfie(String firstSelfie) {
        setDataForDb(FirstSelfie, firstSelfie);
    }

    public static int getActivityCount() {
        try {
            return Integer.parseInt(getDataFromDb(ActivityCount));
        } catch (Exception e) {
        }
        return 0;

    }

    public static void setActivityCount(int activityCount) {
        setDataForDb(ActivityCount, "" + activityCount);
    }

}

