package com.dating.chatbox;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.EventbusLib.UpdateProfileImage;
import com.dating.chatbox.EventbusLib.UpdateXmppConnection;
import com.dating.chatbox.db.DBUtil;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.SSLContext;


public class StartXmppConnectionService extends IntentService {
    ChatManager chatmanager;
    SSLContext sc;

    public StartXmppConnectionService() {
        super("StartXmppConnectionService");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {

        try

        {

            sc = SSLContext.getInstance("TLS");

            sc.init(null, null, new SecureRandom());


        } catch (NoSuchAlgorithmException e)

        {

            throw new IllegalStateException(e);

        } catch (KeyManagementException e)

        {

            throw new IllegalStateException(e);

        }
        try {


            SASLAuthentication.unBlacklistSASLMechanism("PLAIN");
            SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
            SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");

            XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                    .setUsernameAndPassword(DBUtil.getUSERID(), DBUtil.getUSERID())
                    .setCustomSSLContext(sc)

                    .setDebuggerEnabled(true)
                    .setPort(5222)
                    .setServiceName("52.41.229.242")
                    .setHost("52.41.229.242").setSendPresence(false)
                    .setDebuggerEnabled(true).setConnectTimeout(100000)

                    .build();
            if (ChatApplication.getInstance().conn2 == null)
                ChatApplication.getInstance().conn2 = new XMPPTCPConnection(config);
            ChatApplication.getInstance().conn2.connect().login();
            ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(ChatApplication.getInstance().conn2);
            reconnectionManager.setEnabledPerDefault(true);
            reconnectionManager.enableAutomaticReconnection();

            chatmanager = ChatManager.getInstanceFor(ChatApplication.getInstance().conn2);
            ChatApplication.getInstance().roster = Roster.getInstanceFor(ChatApplication.getInstance().conn2);
            if (!ChatApplication.getInstance().roster.isLoaded()) try {
                ChatApplication.getInstance().roster.reloadAndWait();
            } catch (SmackException.NotLoggedInException | SmackException.NotConnectedException | InterruptedException e) {
                e.printStackTrace();
            }
            ChatApplication.getInstance().roster.createEntry(DBUtil.getUSERID() + "@52.41.229.242", String.valueOf(DBUtil.getUSERID()), null);
            ChatApplication.getInstance().roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
            Presence localPresence = new Presence(Presence.Type.available);
            localPresence.setStatus("Online Now");
            localPresence.setPriority(24);
            ChatApplication.getInstance().conn2.sendStanza(localPresence);
            try {
                EventBus.getDefault().post(new UpdateXmppConnection());
            } catch (Throwable e) {

            }


        } catch (Exception e) {
            Log.e("Exception", e.getLocalizedMessage());


        }
    }

}