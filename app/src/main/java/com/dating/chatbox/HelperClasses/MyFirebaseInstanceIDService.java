package com.dating.chatbox.HelperClasses;

/**
 * Created by USER on 9/19/2017.
 */

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.db.DBUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {

        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {


            @Override
            public void onBackground() {
                try {

                    DBUtil.setMessengingID(token);
                    if (!DBUtil.getUSERID().equalsIgnoreCase("")) {

                        UpdateDeviceMessengingID(Integer.parseInt(DBUtil.getUSERID()), token);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });
        // TODO: Implement this method to send token to your app server.
    }

    private void UpdateDeviceMessengingID(int UserID, String MessengingID) {
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
            }
        });
        String url = Singleton.BaseUrl + "UpdateDeviceMessengingID?UserID=" + UserID + "&MessengingID=" + MessengingID;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);
    }
}