package com.dating.chatbox.HelperClasses;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.EventbusLib.ActivityUpdate;
import com.dating.chatbox.EventbusLib.UpdateTabBadges;
import com.dating.chatbox.GoLive.ConstantApp;
import com.dating.chatbox.MainActivity;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.R;
import com.dating.chatbox.StartBroadcastingActivity;
import com.dating.chatbox.db.DBUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.agora.rtc.Constants;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("message"));
            if (jsonObject.has("Typeid") && jsonObject.getInt("Typeid") == 6) {
                JoiniveNotification(jsonObject,jsonObject.optString("Title"),jsonObject.optString("Message"));
                return;
            }


            if (jsonObject.has("Typeid") && jsonObject.getInt("Typeid") == 1) {
                ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                    int count;
                    @Override
                    public void onBackground() {
                        try {
                            count=Integer.valueOf(DBUtil.getChatCount())+1;
                            DBUtil.setChatCount(count);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onUi() {
                        super.onUi();
                        try {
                            EventBus.getDefault().post(new UpdateTabBadges());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                if (!ChatApplication.getInstance().isAppForeground)
                chatNotification(jsonObject.optJSONObject("UserJson"), jsonObject.optString("Title"), jsonObject.optString("Message"));
            } else {

                if (jsonObject.has("Typeid") && jsonObject.getInt("Typeid") == 2) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        int count;
                        @Override
                        public void onBackground() {
                            try {
                                count=Integer.valueOf(DBUtil.getActivityCount())+1;
                                DBUtil.setActivityCount(count);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onUi() {
                            super.onUi();
                            try {
                                EventBus.getDefault().post(new UpdateTabBadges());
                                EventBus.getDefault().post(new ActivityUpdate());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }

                postNotification(jsonObject);
            }

        } catch (Exception e) {

        }


    }

    private void postNotification(JSONObject jsonObject) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(jsonObject.optString("Title"))
                .setContentText(jsonObject.optString("Message"))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setContentInfo(jsonObject.optString("Title"))

                .setColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.chatsenoti);
            notificationBuilder.setColor(getResources().getColor(R.color.must_try_bg_color));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(jsonObject.optString("Message")));


        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationBuilder.setLights(Color.YELLOW, 1000, 300);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(
                    "101", "ChatSe", NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("ChatSe");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            //   channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            notificationBuilder.setChannelId("101");
            //   channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notificationBuilder.build());



    }

    private void chatNotification(JSONObject jsonObject, String title, String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Typeid", 1);
        final User user = new User();

        user.setId(jsonObject.optInt("Id"));
        user.setName(jsonObject.optString("Name"));
        user.setShortBio(jsonObject.optString("ShortBio"));
        if (jsonObject.optString("Gender").equalsIgnoreCase("1")) {
            user.setGender("Male");
        } else {
            user.setGender("Female");
        }

        user.setViewsCount(jsonObject.optInt("ViewsCount"));

        user.setBirthday(jsonObject.optString("Birthday") + " Years");


        ArrayList<UserImages> userImages = new ArrayList<UserImages>();

        JSONArray userImagesarray = jsonObject.optJSONArray("userImages");

        for (int j = 0; j < userImagesarray.length(); j++) {


            JSONObject userImagejson = userImagesarray.optJSONObject(j);
            UserImages userImages1 = new UserImages();
            String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + userImagejson.optString("UserImageUrl") + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
            userImages1.setUserImageUrl(image1);
            userImages.add(userImages1);

        }
        user.setUserImages(userImages);

        User.setUserProfile(user);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setContentInfo(title)

                .setColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.chatsenoti);
            notificationBuilder.setColor(getResources().getColor(R.color.must_try_bg_color));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(jsonObject.optString("Message")));


        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationBuilder.setLights(Color.YELLOW, 1000, 300);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(
                    "102", "ChatSe", NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("ChatSe");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            //   channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            notificationBuilder.setChannelId("102");
            //   channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());


    }
    private void JoiniveNotification(JSONObject jsonObject, String title, String message) {

JSONObject users=jsonObject.optJSONObject("UserJson");
        final User user = new User();
        user.setId(users.optInt("Id"));
        Intent intent = new Intent(this, StartBroadcastingActivity.class);
        intent.putExtra(ConstantApp.ACTION_KEY_CROLE, Constants.CLIENT_ROLE_AUDIENCE);
        intent.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, String.valueOf(user.getId()));


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setContentInfo(title)

                .setColor(Color.WHITE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.chatsenoti);
            notificationBuilder.setColor(getResources().getColor(R.color.must_try_bg_color));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(jsonObject.optString("Message")));


        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationBuilder.setLights(Color.YELLOW, 1000, 300);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(
                    "102", "ChatSe", NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("ChatSe");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            //   channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            notificationBuilder.setChannelId("102");
            //   channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());


    }





}