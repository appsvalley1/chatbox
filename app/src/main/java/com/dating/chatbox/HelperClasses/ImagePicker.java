package com.dating.chatbox.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class ImagePicker {

    public static final int REQUEST_PICK = Crop.REQUEST_PICK;
    public static final int REQUEST_CROP = Crop.REQUEST_CROP;
    private static final String TAG = "ImagePicker";
    private static final String TEMP_IMAGE_NAME = "tempImage";
    private static final String CROPPED_IMAGE_NAME = "croppedImage";

    private static boolean isCamera;
    private static Uri selectedImage;

    public enum ResizeType {
        MIN_QUALITY, FIXED_SIZE
    }

    public static void pickImage(Activity activity) {
        Intent intent = getPickImageIntent(activity);
        activity.startActivityForResult(intent, REQUEST_PICK);
    }

    private static Intent getPickImageIntent(Context context) {
        Intent chooserIntent = null;

        List<Intent> intentList = new ArrayList<>();

        try {
            Intent pickIntent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePhotoIntent.putExtra("return-data", true);
          //  takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile();


            Uri apkURI = FileProvider.getUriForFile(
                    context,
                    context.getApplicationContext()
                            .getPackageName() + ".provider", getTempFile(context));

            takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                takePhotoIntent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
                takePhotoIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                takePhotoIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
            } else {
                takePhotoIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            }


            intentList = addIntentsToList(context, intentList, pickIntent);
            intentList = addIntentsToList(context, intentList, takePhotoIntent);

            if (intentList.size() > 0) {
                chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),
                        "Select image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                        intentList.toArray(new Parcelable[intentList.size()]));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return chooserIntent;
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = null;
        try {
            resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo resolveInfo : resInfo) {
                String packageName = resolveInfo.activityInfo.packageName;
                Intent targetedIntent = new Intent(intent);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    targetedIntent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
                    targetedIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                    targetedIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                } else {
                    targetedIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                }

                targetedIntent.setPackage(packageName);
                list.add(targetedIntent);
                Log.d(TAG, "Intent: " + intent.getAction() + " package: " + packageName);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return list;
    }


    public static void beginCrop(Activity activity, int resultCode, Intent imageReturnedIntent) {
        try {
            Uri selectedImage = getImageUri(activity, resultCode, imageReturnedIntent);
            if (selectedImage != null) {
                Uri destination = Uri.fromFile(new File(activity.getExternalCacheDir(), CROPPED_IMAGE_NAME));
                Crop.of(selectedImage, destination).withAspect(600,400).start(activity);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static Uri getImageUri(Activity activity, int resultCode, Intent imageReturnedIntent) {
        try {
            Log.d(TAG, "getImageUri, resultCode: " + resultCode);
            File imageFile = getTempFile(activity);
            if (resultCode == Activity.RESULT_OK) {
                isCamera = (imageReturnedIntent == null || imageReturnedIntent.getData() == null ||
                        imageReturnedIntent.getData().equals(Uri.fromFile(imageFile)));
                if (isCamera) {     /** CAMERA **/
                    selectedImage = Uri.fromFile(imageFile);
                } else {            /** ALBUM **/
                    selectedImage = imageReturnedIntent.getData();
                }
                Log.d(TAG, "selectedImage: " + selectedImage);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return selectedImage;
    }

    public static Bitmap getImageCropped(Activity activity, int resultCode, Intent result,
                                         ResizeType resizeType, int size) {
        Bitmap bm = null;
        try {
            if (resultCode == Activity.RESULT_OK) {
                Uri croppedImageUri = Crop.getOutput(result);
                bm = getImageResized(activity, croppedImageUri, resizeType, size);
                int rotation = getRotation(activity, selectedImage, isCamera);
                bm = rotate(bm, rotation);

            } else if (resultCode == Crop.RESULT_ERROR) {
                Toast.makeText(activity, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return bm;
    }

    public static String getFilePath(Activity activity, Bitmap bm) {
        File imageFile = new File(activity.getExternalCacheDir(), TEMP_IMAGE_NAME);
        try {
            imageFile.getParentFile().mkdirs();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] bitmapdata = bos.toByteArray();


            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(imageFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return imageFile.getAbsolutePath();
    }

    private static File getTempFile(Context context) {
        File imageFile = new File(context.getExternalCacheDir(), TEMP_IMAGE_NAME);
        try {
            imageFile.getParentFile().mkdirs();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return imageFile;
    }

    /**
     * Resize to avoid using too much memory loading big images (e.g.: 2560*1920)
     **/
    private static Bitmap getImageResized(Context context, Uri selectedImage,
                                          ResizeType resizeType, int size) {
        Bitmap bm = null;
        try {
            int[] sampleSizes = new int[]{5, 3, 2, 1};
            int i = 0;
            do {
                bm = decodeBitmap(context, selectedImage, sampleSizes[i]);
                Log.d(TAG, "Resizer: sample " + sampleSizes[i] + ", new bitmap width=" +
                        bm.getWidth() + " height=" + bm.getHeight());
                i++;
            } while (bm.getWidth() < size && i < sampleSizes.length);
            if (resizeType == ResizeType.FIXED_SIZE) {
                bm = Bitmap.createScaledBitmap(bm, size, size, true);
            }
            Log.d(TAG, "Resizer: finalSize, width=" + bm.getWidth() + " height=" + bm.getHeight());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return bm;
    }

    private static Bitmap decodeBitmap(Context context, Uri theUri, int sampleSize) {
        Bitmap actuallyUsableBitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = sampleSize;

            AssetFileDescriptor fileDescriptor = null;

            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(theUri, "r");
            if (fileDescriptor != null) {
                actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(
                        fileDescriptor.getFileDescriptor(), null, options);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return actuallyUsableBitmap;
    }


    private static int getRotation(Context context, Uri imageUri, boolean isCamera) {
        int rotation;
        if (isCamera) {
            rotation = getRotationFromCamera(context, imageUri);
        } else {
            rotation = getRotationFromGallery(context, imageUri);
        }
        Log.d(TAG, "Image rotation: " + rotation);
        return rotation;
    }

    private static int getRotationFromCamera(Context context, Uri imageFile) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageFile, null);
            ExifInterface exif = new ExifInterface(imageFile.getPath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static int getRotationFromGallery(Context context, Uri imageUri) {
        Cursor cursor = null;
        try {
            String[] columns = {MediaStore.Images.Media.ORIENTATION};
            cursor = context.getContentResolver().query(imageUri, columns, null, null, null);
            if (cursor == null) return 0;

            cursor.moveToFirst();

            int orientationColumnIndex = cursor.getColumnIndex(columns[0]);
            int rotation = cursor.getInt(orientationColumnIndex);
            return rotation;
        } finally {
            try {
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable e) {
            }
        }

    }


    private static Bitmap rotate(Bitmap bm, int rotation) {
        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            Bitmap bmOut = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
            return bmOut;
        }
        return bm;
    }
}
