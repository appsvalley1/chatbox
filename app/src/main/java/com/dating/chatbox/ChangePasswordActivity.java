package com.dating.chatbox;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ResponseManager;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.db.DBUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordActivity extends AppCompatActivity implements Validator.ValidationListener {
    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ANY)

    Validator validator;
    User user;
    private LinearLayout lvLogin;
    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ANY)
    private EditText EditTextPassword;
    private ProgressBar progressBar;
    private Button startButton;

    private SessionManager sessionManager;

    public ChangePasswordActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepasswordfragment);
        sessionManager = new SessionManager(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        TextView appname = (TextView) mCustomView.findViewById(R.id.appname);
        appname.setText("Change Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewsbyId();
        validator = new Validator(this);
        validator.setValidationListener(this);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmPasswordChangeDialog();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void findViewsbyId() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        EditTextPassword = (EditText) findViewById(R.id.EditTextPassword);
        startButton = (Button) findViewById(R.id.startButton);
    }

    @Override
    public void onValidationSucceeded() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(ChangePasswordActivity.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(ChangePasswordActivity.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "UpdatePassword?UserID=" + DBUtil.getUSERID() + "&Password=" + EditTextPassword.getText().toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            // Display the first 500 characters of the response string.
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();


                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            if (responseManager.getStatus() == 1) {

                                sessionManager.clearAllData();
                                Intent i = new Intent(ChangePasswordActivity.this, LoginNow.class);
                                startActivity(i);
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                                finish();
                                Toast.makeText(getApplicationContext(), "Password Updated, Please login Again", Toast.LENGTH_SHORT).show();

                            } else {

                                Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();
                            }


                            progressBar.setVisibility(View.GONE);


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void ConfirmPasswordChangeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Update Password");
        builder.setMessage("You Need To Login Again After That.");

        builder.setPositiveButton("Ok, I Understand.", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                validator.validate();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO, Will do Later", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


}
