package com.dating.chatbox;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;

import com.dating.chatbox.Fragments.ActivityFragment;
import com.dating.chatbox.Fragments.ChatUsersFragment;
import com.dating.chatbox.Fragments.FavouritesFragment;
import com.dating.chatbox.Fragments.HomeFragment;
import com.dating.chatbox.Fragments.LiveBroadcastersFragment;
import com.dating.chatbox.Fragments.MenuFragment;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Adapters.ViewPagerAdapter;
import com.dating.chatbox.Custom.CustomNativeAdsManager;
import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.UpdateTabBadges;
import com.dating.chatbox.Models.ResponseManager;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.controllers.DashboardController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.DashboardResponse;
import com.dating.chatbox.db.DBUtil;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.morsebyte.shailesh.twostagerating.TwoStageRate;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements MenuFragment.Callbacks {


    TextView appname;
    User user;
    Dialog ApkUpdate;
    private SessionManager sessionManager;
    private ViewPager viewPager;
    private DrawerLayout drawerLayout;
    private TabLayout tabLayout;
    private String[] pageTitle = {"Home","Live", "Activity", "Chat", "Favourite"};
    private int[] tabImages = {R.drawable.homeselector, R.drawable.activityselector,R.drawable.activityselector, R.drawable.chatselector, R.drawable.favouritesselector};
    private long onBackPressed;
    private InterstitialAd bPInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        drawerLayout = findViewById(R.id.drawerLayout);
        MenuFragment menuFragment = MenuFragment.newInstance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.detail_fragment_container, menuFragment, "Menu Fragment")
                .commit();
        LoadDashboard();
        loadAdmobbackpressAd();
        try {
            String[] adUnitIds = {"ca-app-pub-6689795083534236/5426983121",
                    "ca-app-pub-6689795083534236/1487738116", "ca-app-pub-6689795083534236/942277889"};
            String adUnitId = adUnitIds[new Random().nextInt(adUnitIds.length)];
            bPInterstitialAd = new InterstitialAd(MainActivity.this);
            bPInterstitialAd.setAdUnitId(adUnitId);
            bPInterstitialAd.loadAd(new AdRequest.Builder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        TwoStageRate.with(this).showIfMeetsConditions();

        Toolbar toolbar = findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbar, null);
        toolbar.addView(mCustomView);
        appname = mCustomView.findViewById(R.id.appname);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/proximanova_light.otf");
        appname.setTypeface(tf);
        appname.setGravity(Gravity.LEFT);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_Close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        viewPager = findViewById(R.id.view_pager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);


        setupTabIcons();
        sessionManager = new SessionManager(MainActivity.this);
        if (sessionManager.GetisDeviceMessengingIDAdded() == false) {

            UpdateDeviceMessengingID(Integer.parseInt(DBUtil.getUSERID()), DBUtil.getMessengingID());

        }
        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
            int chatBadgecounter;

            @Override
            public void onBackground() {
                try {
                    chatBadgecounter = DBUtil.getChatCount();
                    Intent startXmppConnectionService = new Intent(getApplicationContext(), StartXmppConnectionService.class);
                    startService(startXmppConnectionService);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                super.onUi();
                TextView tvBadge = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvBadge);

                if (chatBadgecounter == 0) {

                } else {
                    tvBadge.setText("" + chatBadgecounter);
                    tvBadge.setVisibility(View.VISIBLE);
                }

            }
        });

        try {
            handleIntent(getIntent());
            NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nMgr.cancelAll();
        } catch (Throwable e) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void UpdateDeviceMessengingID(int UserID, String MessengingID) {
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
            }
        });
        String url = Singleton.BaseUrl + "UpdateDeviceMessengingID?UserID=" + UserID + "&MessengingID=" + MessengingID;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();
                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            if (responseManager.getStatus() == 1) {
                                sessionManager.SetisDeviceMessengingIDAdded(true);
                            }


                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(stringRequest);
    }


    @Override
    public void onBackPressed() {
        try {
            if (System.currentTimeMillis() - onBackPressed > 2000) {
                onBackPressed = System.currentTimeMillis();
                Toast.makeText(this, "Press twice to exit", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onBackPressed();
        try {
            if (bPInterstitialAd != null && bPInterstitialAd.isLoaded()) {
                bPInterstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert drawerLayout != null;
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        finish();
        try {
            Presence localPresence = new Presence(Presence.Type.unavailable);
            localPresence.setStatus("Offline");
            localPresence.setPriority(24);
            ChatApplication.getInstance().conn2.sendStanza(localPresence);
        } catch (Exception localException) {

        }

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.Preferences) {
         //   Answers.getInstance().logCustom(new CustomEvent("Home Prefrence Clicked"));
            Intent i = new Intent(MainActivity.this, MyPrefrences.class);
            startActivity(i);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }





        return super.onOptionsItemSelected(item);
    }

    private void setupTabIcons() {

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            LinearLayout tab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
            TextView tab_label = tab.findViewById(R.id.nav_label);
            ImageView tab_icon = tab.findViewById(R.id.nav_icon);
            tab_label.setText(pageTitle[i]);
            tab_icon.setImageResource(tabImages[i]);
            tabLayout.getTabAt(i).setCustomView(tab);
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 1:
                        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                            @Override
                            public void onBackground() {
                                try {
                                    DBUtil.setActivityCount(0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onUi() {
                                super.onUi();
                                TextView tvBadge = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvBadge);
                                tvBadge.setVisibility(View.GONE);
                            }
                        });
                        break;
                    case 2:
                        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                            @Override
                            public void onBackground() {
                                try {
                                    DBUtil.setChatCount(0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onUi() {
                                super.onUi();
                                TextView tvBadge = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvBadge);
                                tvBadge.setVisibility(View.GONE);
                            }
                        });
                        break;

                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), "Home");
        adapter.addFrag(new LiveBroadcastersFragment(), "Live");
        adapter.addFrag(new ActivityFragment(), "Activity");
        adapter.addFrag(new ChatUsersFragment(), "Chats");
        adapter.addFrag(new FavouritesFragment(), "Favourites");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                appname.setText(pageTitle[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void loadAdmobbackpressAd() {

        try {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            final com.google.android.gms.ads.AdView mAdView = new AdView(this);
            mAdView.setAdSize(new AdSize(((int) (((float) displayMetrics.widthPixels) / displayMetrics.density)) - 50, 80));
            mAdView.setAdUnitId("ca-app-pub-6689795083534236/6857594267");
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    CustomNativeAdsManager customNativeAdsManager = new CustomNativeAdsManager();
                    customNativeAdsManager.setHasFirstAd(true);
                    customNativeAdsManager.setAdMobBanner(mAdView);
                    customNativeAdsManager.setCustomNativeAdsManager(customNativeAdsManager);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    CustomNativeAdsManager customNativeAdsManager = new CustomNativeAdsManager();
                    customNativeAdsManager.setHasFirstAd(false);
                    customNativeAdsManager.setCustomNativeAdsManager(customNativeAdsManager);
                }

                @Override
                public void onAdOpened() {

                }

                @Override
                public void onAdLeftApplication() {

                }

                @Override
                public void onAdClosed() {

                }
            });

            mAdView.loadAd(adRequest);


        } catch (Exception e) {
            Log.e("error", e.getMessage());

        }
    }


    private void handleIntent(Intent intent) {
        try {
            if (intent != null) {
                Bundle bundle = intent.getExtras();
                int landingtype;
                if (bundle != null) {
                    if (bundle.containsKey("Typeid")) {
                        landingtype = bundle.getInt("Typeid", 0);
                        switch (landingtype) {
                            case 1:
                                Intent chatScreen = new Intent(this, ChatScreen.class);
                                startActivity(chatScreen);
                                overridePendingTransition(R.anim.enter, R.anim.exit);

                                break;

                        }
                    }

                }

            }

        } catch (Throwable e) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();


        try {
            if (ChatApplication.getInstance().conn2 != null && ChatApplication.getInstance().conn2.isConnected()) {
                Presence localPresence = new Presence(Presence.Type.available);
                localPresence.setStatus("Online Now");
                localPresence.setPriority(24);
                ChatApplication.getInstance().conn2.sendStanza(localPresence);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        try {
            if (ApkUpdate != null && ApkUpdate.isShowing()) {
                ApkUpdate.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }

    private void LoadDashboard() {
        Log.e("loading", "sasa");
        JSONObject jsonObject = new JSONObject();
        String url = Singleton.BaseUrl + "GetDashboard?UserID=" + DBUtil.getUSERID() + "&AppVersion=" + Utils.getAppVersionCode();
        DashboardController controller = new DashboardController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
            @Override
            public void handleResponse(ResponsePayload responseObject) {

                try {
                    if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof DashboardResponse) {
                        final DashboardResponse response = (DashboardResponse) responseObject.getResponseObject();

                        if (response.Dashboard.getExist() == 0) {

                            sessionManager.clearAllData();
                            DBUtil.setActivityCount(0);
                            DBUtil.setShortBio("");
                            DBUtil.setUSERID("");
                            DBUtil.setVibrations("");
                            Toast.makeText(getApplicationContext(),"Please register to continue.",Toast.LENGTH_LONG).show();

                            startActivity(new Intent(MainActivity.this,Splash.class));
                            finish();

                        }
                        else
                        {
                            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {


                                @Override
                                public void onBackground() {
                                    try {
                                        DBUtil.setInviteText(response.Dashboard.getInviteText());
                                        DBUtil.setInviteTitle(response.Dashboard.getInviteTitle());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }


                            });

                            if (response.Status != 0) {
                                showApkUpdatePopup(response.Dashboard.getIsRequiredUpdate());
                            }
                        }





                    }
                } catch (Throwable e) {


                }
            }
        }, url);
        controller.setRequestObj(jsonObject);
        controller.sendRequest();


    }

    public void showApkUpdatePopup(int IsRequiredUpdate) {
        try {
            ApkUpdate = new Dialog(MainActivity.this);
            ApkUpdate.requestWindowFeature(Window.FEATURE_NO_TITLE);
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.99);
            ApkUpdate.setContentView(R.layout.customdialogupdateapk);
            ApkUpdate.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            ApkUpdate.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ApkUpdate.setCanceledOnTouchOutside(true);
            Button updatetButton = ApkUpdate.findViewById(R.id.updatetButton);
            updatetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        try {
                            if (ApkUpdate.isShowing())
                                ApkUpdate.dismiss();
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                        Utils.openChatsePlayStore(MainActivity.this);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });


            Button lvNotnow = ApkUpdate.findViewById(R.id.lvNotnow);
            lvNotnow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (ApkUpdate.isShowing())
                            ApkUpdate.dismiss();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });

            if (IsRequiredUpdate == 0) {

                ApkUpdate.setCancelable(true);

            } else {
                lvNotnow.setVisibility(View.GONE);

                ApkUpdate.setCancelable(false);

            }

            ApkUpdate.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            if (!isFinishing() && !ApkUpdate.isShowing())
                ApkUpdate.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMenuClicked(int masterItemId) {
        drawerLayout.closeDrawers();
        switch (masterItemId) {

            case R.id.llHeader:
                Intent intent = new Intent(this, MyProfile.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

                break;

            case R.id.lvHome:
                viewPager.setCurrentItem(0);
                break;
            case R.id.lvMaps:
                Intent i = new Intent(this, NearbyuserMapsActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.lvActivity:
                viewPager.setCurrentItem(1);
                break;

            case R.id.lvChats:
                viewPager.setCurrentItem(2);
                break;

            case R.id.lvFavourites:
                viewPager.setCurrentItem(3);
                break;

            case R.id.lvInviteBoost:
                Intent Invite = new Intent(this, InviteAndEarn.class);
                startActivity(Invite);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.lvTwitter:
                String Twitter1 = "https://www.twitter.com/sechatse/";
                Intent Twitter = new Intent(Intent.ACTION_VIEW);
                Twitter.setData(Uri.parse(Twitter1));
                startActivity(Twitter);
                break;

            case R.id.lvFacebook:
                String Facebookurl = "https://www.facebook.com/sechatse/";
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                i2.setData(Uri.parse(Facebookurl));
                startActivity(i2);
                break;

            case R.id.lvSettings:
                Intent i1 = new Intent(this, SettingsActivity.class);
                startActivity(i1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.lvInstagram:
                String Instagramurl = "https://www.instagram.com/sechatse/";
                Intent ins = new Intent(Intent.ACTION_VIEW);
                ins.setData(Uri.parse(Instagramurl));
                startActivity(ins);
                break;

            case R.id.lvChangePassword:
                Intent ich = new Intent(this, ChangePasswordActivity.class);
                startActivity(ich);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.lvTerms:
                Singleton.BrowserTitle = "Privacy Policy & Terms of Use";
                Singleton.Urls = "http://proacdoc.com/EmailTemplates/privacy.html";
                startActivity(new Intent(MainActivity.this, InAppBrowserActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.textVersion:
                Utils.openChatsePlayStore(getApplicationContext());
                break;
            case R.id.lvPhoto:
                //Answers.getInstance().logCustom(new CustomEvent("User Image Add Menu"));
                Intent myImages = new Intent(this, MyImages.class);
                startActivity(myImages);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UpdateTabBadges event) {
        try {
            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                int chatBadgecounter;
                int activityBadgecounter;

                @Override
                public void onBackground() {
                    try {
                        activityBadgecounter = DBUtil.getActivityCount();
                        chatBadgecounter = DBUtil.getChatCount();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onUi() {
                    super.onUi();
                    TextView tvBadge = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.tvBadge);
                    tvBadge.setVisibility(View.VISIBLE);
                    if (chatBadgecounter == 0) {
                        tvBadge.setVisibility(View.GONE);

                    } else {
                        tvBadge.setText("" + chatBadgecounter);
                    }
                    TextView tvactivity = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.tvBadge);
                    tvactivity.setVisibility(View.VISIBLE);
                    if (activityBadgecounter == 0) {
                        tvactivity.setVisibility(View.GONE);
                    } else {
                        tvactivity.setText("" + activityBadgecounter);
                    }

                }
            });
        } catch (Exception e) {
        }
    }

    ;


}
