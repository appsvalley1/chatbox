package com.dating.chatbox;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Adapters.UsersImagesAdapter;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.ActivityUpdate;
import com.dating.chatbox.EventbusLib.HomePreferencesUpdate;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.controllers.AddToProfileController;
import com.dating.chatbox.controllers.ViewProfileController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.DashboardResponse;
import com.dating.chatbox.controllers.response.RegistrationResponse;
import com.dating.chatbox.db.DBUtil;
import com.eftimoff.viewpagertransformers.DepthPageTransformer;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.morsebyte.shailesh.twostagerating.TwoStageRate;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterListener;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import it.sephiroth.android.library.tooltip.Tooltip;
import me.relex.circleindicator.CircleIndicator;

public class UsersProfile extends AppCompatActivity {
    TextView appname, textViewViewsCount, textViewShortBio, textViewGender, textViewName, textViewAge, textviewStatus, tvLocation, textviewDistance;
    RotateAnimation rotateAnimation;
    private ImageView imageViewFavourite, ImageUserStatus;

    private User user;
    private LinearLayout lvFavourite;
    private InterstitialAd bPInterstitialAd;
    private LottieAnimationView lottieAnim;
    private RelativeLayout rlAnimation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_profile);
        lottieAnim = findViewById(R.id.lottieAnim);
        rlAnimation = findViewById(R.id.rlAnimation);
        try {
            String[] adUnitIds = {"ca-app-pub-6689795083534236/5426983121",
                    "ca-app-pub-6689795083534236/1487738116", "ca-app-pub-6689795083534236/942277889"};
            String adUnitId = adUnitIds[new Random().nextInt(adUnitIds.length)];
            bPInterstitialAd = new InterstitialAd(UsersProfile.this);
            bPInterstitialAd.setAdUnitId(adUnitId);
            bPInterstitialAd.loadAd(new AdRequest.Builder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        appname = (TextView) mCustomView.findViewById(R.id.appname);
        appname.setText(User.getUserProfile().getName() + "' s Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CommitViewProfile();
        ArrayList<UserImages> userImages = new ArrayList<>();
        userImages = User.getUserProfile().getUserImages();
        textViewViewsCount = (TextView) findViewById(R.id.textViewViewsCount);
        textViewViewsCount.setText(String.valueOf(User.getUserProfile().getViewsCount()));
        textViewShortBio = (TextView) findViewById(R.id.textViewShortBio);
        textViewShortBio.setText(String.valueOf(User.getUserProfile().getShortBio()));

        textViewGender = (TextView) findViewById(R.id.textViewGender);
        textViewGender.setText(String.valueOf(User.getUserProfile().getGender()));


        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewName.setText(String.valueOf(User.getUserProfile().getName()));


        textViewAge = (TextView) findViewById(R.id.textViewAge);
        textViewAge.setText(String.valueOf(User.getUserProfile().getBirthday()));
        lvFavourite = (LinearLayout) findViewById(R.id.lvFavourite);


        textviewDistance = (TextView) findViewById(R.id.textviewDistance);
        textviewDistance.setText(User.getUserProfile().getDistanceKM() + " KM");


        tvLocation = (TextView) findViewById(R.id.tvLocation);
        if (User.getUserProfile().getShowLocationStatus() == 0) {
            tvLocation.setText("hidden..");
        } else {
            tvLocation.setText(User.getUserProfile().getCityName());
        }
        imageViewFavourite = (ImageView) findViewById(R.id.imageViewFavourite);
        if (User.getUserProfile().isFavourite()) {
            Glide.with(getApplicationContext()).load(R.drawable.favfilled).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageViewFavourite);

            lvFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RemoveFavourite(User.getUserProfile().getId());
                }
            });

        } else {
            lvFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    lottieAnim.playAnimation();
                    lottieAnim.loop(false);
                    rlAnimation.setVisibility(View.VISIBLE);
                    AddToFavourites();
                }
            });


            Tooltip.make(this,
                    new Tooltip.Builder(101)
                            .anchor(imageViewFavourite, Tooltip.Gravity.BOTTOM)
                            .closePolicy(new Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 3000)
                            .activateDelay(800)
                            .showDelay(300)
                            .text("Mark as favourite and chat later")
                            .maxWidth(500)
                            .withArrow(true)
                            .withOverlay(true).withStyleId(R.style.ToolTipLayoutusersprofileyellowCustomStyle)

                            .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
            ).show();
            Glide.with(getApplicationContext()).load(R.drawable.favunfilled).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageViewFavourite);
        }




        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new UsersImagesAdapter(this, userImages));
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        indicator.setViewPager(viewPager);

        textviewStatus = findViewById(R.id.textviewStatus);
        ImageUserStatus = findViewById(R.id.ImageUserStatus);


        if (User.getUserProfile().getShowOnlineStatus() == 0) {
            Glide.with(getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
            textviewStatus.setText("Offline");
        } else {
            loadUserStatus();
        }
        if (ChatApplication.getInstance().conn2 != null && ChatApplication.getInstance().conn2.isConnected()) {
            getRoaster();

        }
    }


    private void CommitViewProfile() {
        JSONObject jsonObject = new JSONObject();
        String url = "http://www.proacdoc.com/Service1.svc/ViewProfile?ViewFromUserID=" + DBUtil.getUSERID() + "&ViewofUserID=" + User.getUserProfile().getId() + "";
        ViewProfileController controller = new ViewProfileController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
            @Override
            public void handleResponse(ResponsePayload responseObject) {
                try {

                    if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof RegistrationResponse) {
                        final RegistrationResponse response = (RegistrationResponse) responseObject.getResponseObject();
                    }

                } catch (Throwable e) {
                }
            }
        }, url);
        controller.setRequestObj(jsonObject);
        controller.sendRequest();
    }

    private void AddToFavourites() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlAnimation.setVisibility(View.GONE);
            }
        }, 1000);
        //Answers.getInstance().logCustom(new CustomEvent("Add to Favourite Clicked"));
        loadUserStatus();
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(UsersProfile.this);
        }

        JSONObject jsonObject = new JSONObject();
        String url = "http://www.proacdoc.com/Service1.svc/AddToFavourite?FavouriteUserFrom=" + DBUtil.getUSERID() + "&FavouriteUserTO=" + User.getUserProfile().getId() + "&FavouriteDated=" + System.currentTimeMillis();
        AddToProfileController controller = new AddToProfileController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
            @Override
            public void handleResponse(ResponsePayload responseObject) {
                try {

                    if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof DashboardResponse) {
                        final DashboardResponse response = (DashboardResponse) responseObject.getResponseObject();
                        Toast.makeText(getApplicationContext(), User.getUserProfile().getName() + " " + "Added To favourites", Toast.LENGTH_SHORT).show();
                        TwoStageRate.with(UsersProfile.this).showIfMeetsConditions();
                        Glide.with(getApplicationContext()).load(R.drawable.favfilled).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageViewFavourite);
                        EventBus.getDefault().post(new HomePreferencesUpdate());
                        EventBus.getDefault().post(new ActivityUpdate());
                        lvFavourite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                RemoveFavourite(User.getUserProfile().getId());
                            }
                        });


                    }

                } catch (Throwable e) {
                }
            }
        }, url);
        controller.setRequestObj(jsonObject);
        controller.sendRequest();
    }

    public void SendToChat(View v) {
        //Answers.getInstance().logCustom(new CustomEvent("Chat Button Clicked"));
        Intent chatScreen = new Intent(this, ChatScreen.class);
        startActivity(chatScreen);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Preferences:
                //Answers.getInstance().logCustom(new CustomEvent("Users  Profile Prefrence Clicked"));
                Intent i = new Intent(UsersProfile.this, MyPrefrences.class);
                startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void loadUserStatus() {
        RequestQueue queue = Volley.newRequestQueue(UsersProfile.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                //    progressBar.setVisibility(View.INVISIBLE);
            }
        });
        String url = "http://52.41.229.242:9090/plugins/presence/status?jid=" + User.getUserProfile().getId() + "@52.41.229.242&type=text";
        url = url.replaceAll(" ", "%20");


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        response = response.trim();
                        if (response.equalsIgnoreCase("null")) {
                            response = "offline";
                        }

                        textviewStatus.setText(response);
                        if (response.equalsIgnoreCase("Online Now")) {

                            Glide.with(getApplicationContext()).load(R.drawable.greenonline).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                        } else {
                            Glide.with(getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //   progressBar.setVisibility(View.VISIBLE);

            }
        });

        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            if (bPInterstitialAd != null && bPInterstitialAd.isLoaded()) {
                bPInterstitialAd.show();
            } else {
                TwoStageRate.with(UsersProfile.this).showIfMeetsConditions();

            }
        } catch (Throwable e) {

        }
    }

    public void getRoaster() {
        try {
            Roster roster = ChatApplication.getInstance().roster;
            roster.createEntry(String.valueOf(User.getUserProfile().getId()) + "@52.41.229.242", User.getUserProfile().getName(), null);
            roster.addRosterListener(new RosterListener() {
                // Ignored events public void entriesAdded(Collection<String> addresses) {}
                public void entriesDeleted(Collection<String> addresses) {
                }

                @Override
                public void entriesAdded(Collection<String> addresses) {

                }

                public void entriesUpdated(Collection<String> addresses) {
                }

                public void presenceChanged(Presence presence) {

                    String from[] = presence.getFrom().split("@");
                    if (from[0].equalsIgnoreCase(String.valueOf(User.getUserProfile().getId()))) {
                        if (presence.getType() == Presence.Type.available) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    textviewStatus.setText("Online Now");
                                    Glide.with(getApplicationContext()).load(R.drawable.greenonline).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                                }
                            });

                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    textviewStatus.setText("Offline");
                                    Glide.with(getApplicationContext()).load(R.drawable.gray).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageUserStatus);
                                }
                            });
                        }
                    }


                }
            });
        } catch (Exception e) {

        }
    }
    private void RemoveFavourite( int FavouriteUserTO) {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(UsersProfile.this);
        }
        RequestQueue queue = Volley.newRequestQueue(UsersProfile.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

            }
        });
        String url = Singleton.BaseUrl + "RemoveFavourite?FavouriteUserFrom=" + DBUtil.getUSERID() + "&FavouriteUserTO=" + FavouriteUserTO;

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),"Removed from Favourates",Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(new HomePreferencesUpdate());
                        EventBus.getDefault().post(new ActivityUpdate());
                        lvFavourite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                lottieAnim.playAnimation();
                                lottieAnim.loop(false);
                                rlAnimation.setVisibility(View.VISIBLE);
                                AddToFavourites();
                            }
                        });
                        Glide.with(getApplicationContext()).load(R.drawable.favunfilled).diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageViewFavourite);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}
