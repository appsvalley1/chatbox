package com.dating.chatbox;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Adapters.MyImagesAdapter;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.UpdateProfileImage;
import com.dating.chatbox.Models.ResponseManager;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.db.DBUtil;
import com.eftimoff.viewpagertransformers.DepthPageTransformer;
import com.firebase.client.Firebase;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import it.sephiroth.android.library.tooltip.Tooltip;
import me.relex.circleindicator.CircleIndicator;

public class MyProfile extends AppCompatActivity implements Validator.ValidationListener {
    TextView appname,  textViewGender, textViewName, textViewAge, textCity;
    @NotEmpty
    EditText edittextShortBio;
    Validator validator;
    private ImageView imageViewprofile;

    private User user;
    private ProgressBar progressBar;
    private Button startButton;
    private LinearLayout lvChatButton;
    private  List<UserImages> userImagesDefaultAdded = new ArrayList<>(0);
    ViewPager viewPager;
    CircleIndicator indicator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

         viewPager = findViewById(R.id.viewpager);


        if (!EventBus.getDefault().isRegistered(this)) { EventBus.getDefault().register(this); }
        LoadToolbar();
        Firebase.setAndroidContext(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        imageViewprofile = findViewById(R.id.imageViewprofile);
        textViewGender = findViewById(R.id.textViewGender);
        textViewName = findViewById(R.id.textViewName);
        textViewAge = findViewById(R.id.textViewAge);
        textCity = findViewById(R.id.textCity);

        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {

            public String ImageUrl, cityName, Gender, Name, Birthday, ShortBio;

            @Override
            public void onBackground() {
                try {

                    ImageUrl = DBUtil.getUserImage();
                    cityName = DBUtil.getUserCity();
                    Gender = DBUtil.getGender();
                    Name = DBUtil.getName();
                    Birthday = DBUtil.getAge();
                    ShortBio = DBUtil.getShortBio();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                super.onUi();
                try {

                    UserImages userImages=new UserImages();
                    userImages.setUserImageUrl(ImageUrl);
                    userImagesDefaultAdded.add(userImages);
                    viewPager.setAdapter(new MyImagesAdapter(getApplicationContext(), userImagesDefaultAdded));
                    viewPager.setPageTransformer(true, new DepthPageTransformer());
                    indicator =  findViewById(R.id.indicator);
                    indicator.setViewPager(viewPager);
                    if (Gender.equalsIgnoreCase("1")) {
                        textViewGender.setText("Male");
                    } else {
                        textViewGender.setText("Female");
                    }


                    textViewName.setText(Name);

                    textViewAge.setText(Birthday + " Years");

                    textCity.setText(cityName);
                    edittextShortBio.setText(ShortBio);
                    LoadMyImages();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });


        edittextShortBio = findViewById(R.id.edittextShortBio);


        edittextShortBio.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Do whatever you want here
                    startButton.performClick();
                    return true;
                }
                return false;
            }
        });
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        startButton = (Button) findViewById(R.id.startButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        lvChatButton = (LinearLayout) findViewById(R.id.lvChatButton);
        Tooltip.make(this,
                new Tooltip.Builder(101)
                        .anchor(lvChatButton, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 3000)
                        .activateDelay(800)
                        .showDelay(300)
                        .text("Tap To Add More Images")
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(true).withStyleId(R.style.ToolTipLayoutCustomStyle)

                        .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .build()
        ).show();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Preferences:
                //Answers.getInstance().logCustom(new CustomEvent("My Profile  Prefrence Clicked"));
                Intent i = new Intent(MyProfile.this, MyPrefrences.class);
                startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onValidationSucceeded() {
       // Answers.getInstance().logCustom(new CustomEvent("Introduction Updated"));
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(MyProfile.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(MyProfile.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "UpdateShortBio?UserID=" + DBUtil.getUSERID() + "&ShortBio=" + edittextShortBio.getText().toString();
        url = url.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            // Display the first 500 characters of the response string.
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();


                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);

                            imm.hideSoftInputFromWindow(edittextShortBio.getWindowToken(), 0);

                            progressBar.setVisibility(View.GONE);

                            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {

                                public String ImageUrl, cityName, Gender, Name, Birthday, ShortBio;

                                @Override
                                public void onBackground() {
                                    try {

                                        DBUtil.setShortBio(edittextShortBio.getText().toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }


                            });
                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void AddImages(View v)

    {
        //Answers.getInstance().logCustom(new CustomEvent("User Image Add"));
        Intent myImages = new Intent(this, MyImages.class);
        startActivity(myImages);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private void LoadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        appname = mCustomView.findViewById(R.id.appname);
        appname.setText("My Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UpdateProfileImage event) {
        try {
            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                public String ImageUrl;
                @Override
                public void onBackground() {
                    try {
                        ImageUrl = DBUtil.getUserImage();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onUi() {
                    super.onUi();
                    try {
                        Glide.with(MyProfile.this)
                                .load(ImageUrl).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).
                                into(imageViewprofile);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    };

    private void LoadMyImages() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(MyProfile.this);
        }

        RequestQueue queue = Volley.newRequestQueue(MyProfile.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

            }
        });

        String url = Singleton.BaseUrl + "GetUserImagesByUserID?UserID=" + DBUtil.getUSERID();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            userImagesDefaultAdded.clear();
                            JSONArray userImagesarray = new JSONArray(response);
                            for (int j = 0; j < userImagesarray.length(); j++) {
                                JSONObject userImagejson = userImagesarray.optJSONObject(j);
                                UserImages userImages1 = new UserImages();
                                userImages1.setImageId(userImagejson.optInt("ImageId"));
                                userImages1.setFirstImage(userImagejson.optBoolean("IsFirstImage"));
                                String imagelist = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + userImagejson.optString("UserImageUrl") + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                userImages1.setUserImageUrl(imagelist);
                                userImages1.setBlankImage(false);
                                userImagesDefaultAdded.add(userImages1);
                            }
                            viewPager.setAdapter(new MyImagesAdapter(getApplicationContext(), userImagesDefaultAdded));
                            viewPager.setPageTransformer(true, new DepthPageTransformer());
                            indicator.setViewPager(viewPager);

                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


}
