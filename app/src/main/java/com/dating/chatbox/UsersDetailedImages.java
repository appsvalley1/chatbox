package com.dating.chatbox;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.chatbox.Adapters.UsersdetailedImagesAdapter;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.eftimoff.viewpagertransformers.DepthPageTransformer;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class UsersDetailedImages extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_detailed_images);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        TextView appname = (TextView) mCustomView.findViewById(R.id.appname);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (ChatApplication.getInstance().isFromProfile) {
            viewPager.setAdapter(new UsersdetailedImagesAdapter(this, User.getUserProfile().getUserImages()));
            appname.setText(User.getUserProfile().getName() + "' s Profile ");
        } else {
            ArrayList<UserImages> userImages=new ArrayList<>();
            UserImages userImages1=new UserImages();
            userImages1.setUserImageUrl(getIntent().getStringExtra("imageURL"));
            userImages.add(userImages1);
            viewPager.setAdapter(new UsersdetailedImagesAdapter(this, userImages));
            appname.setText("Back");

        }

        viewPager.setPageTransformer(true, new DepthPageTransformer());
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        indicator.setViewPager(viewPager);

        loadAdmobbackpressAd();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadAdmobbackpressAd() {

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.banner_container);
        final com.google.android.gms.ads.AdView mAdView = (com.google.android.gms.ads.AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                // Code to be executed when an ad finishes loading.
                linearLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        mAdView.loadAd(adRequest);


    }
}
