package com.dating.chatbox;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.HelperClasses.ImagePicker;
import com.dating.chatbox.controllers.RegistrationController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.base.ResponsePayload;
import com.dating.chatbox.controllers.response.RegistrationResponse;
import com.dating.chatbox.db.DBUtil;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class RegisterNow extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, Validator.ValidationListener {

    FirebaseStorage storage;
    ProgressDialog pd;
    Validator validator;
    private ImageView imageViewprofile;
    private Button buttonRegister;
    private LinearLayout lvLogin;
    private Boolean isImageUploaded = false;
    private ProgressBar progressBar;
    private String UserImageUrl = "sassa";
    private LottieAnimationView lottieAnim;

    @NotEmpty
    @Email
    private EditText EditTextEmail;
    @Password(min = 6, scheme = Password.Scheme.ANY)
    private EditText EditTextPassword;
    @NotEmpty
    private EditText EditTextName;
    private SessionManager sessionManager;
    private static final String EMAIL = "email";
    private LoginButton loginButton;
    private AccessToken mAccessToken;
    private CallbackManager callbackManager;
    private boolean isFromFacebook = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_now);
        FacebookSdk.sdkInitialize(getApplicationContext());
        isStoragePermissionGranted();

        Firebase.setAndroidContext(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
        sessionManager = new SessionManager(this);

        findViewsbyId();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ScaleAnimation fade_in = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                fade_in.setDuration(500);     // animation duration in milliseconds
                fade_in.setFillAfter(true);    // If fillAfter is true, the transformation that this animation performed will persist when it is finished.
                imageViewprofile.startAnimation(fade_in);
            }
        }, 300);


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromFacebook = true;
                validator.validate();
            }
        });

        lottieAnim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ImagePicker.pickImage(RegisterNow.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        lvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Answers.getInstance().logCustom(new CustomEvent("Login From Reg Clicked"));
                Intent intent = new Intent(RegisterNow.this, LoginNow.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();

            }
        });
        storage = FirebaseStorage.getInstance();

    }

    private void findViewsbyId() {
        imageViewprofile = (ImageView) findViewById(R.id.imageViewprofile);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        lvLogin = (LinearLayout) findViewById(R.id.lvLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        EditTextName = (EditText) findViewById(R.id.EditTextName);

        EditTextEmail = (EditText) findViewById(R.id.EditTextEmail);
        EditTextPassword = (EditText) findViewById(R.id.EditTextPassword);
        lottieAnim = findViewById(R.id.lottieAnim);
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_photos", "user_location", "user_gender", "user_birthday", "user_age_range"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserProfile(loginResult.getAccessToken());


            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


    }


    public void loginfb(View v) {
        loginButton.performClick();
    }

    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            EditTextPassword.setText(object.getString("id"));
                            EditTextName.setText(object.getString("name"));
                            EditTextEmail.setText(object.getString("email"));
                           if(object.has("gender"))
                           {
                               DBUtil.setGender((object.getString("gender").equalsIgnoreCase("Male")) ? "1" : "2");
                           }
                            else
                           {
                               DBUtil.setGender("1");
                           }
                            if(object.has("birthday"))
                            {
                                SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
                                SimpleDateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy" );

                                try {
                                 Date   bdate = originalFormat.parse(object.getString("birthday"));
                                    DBUtil.setAge( targetFormat.format(bdate));
                                } catch (ParseException ex) {
                                    // Handle Exception.
                                }


                            }
                            else
                            {
                                DBUtil.setAge(object.getString("27/11/1986"));
                            }

                            final String image_url = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=normal";
                            Glide.with(getApplicationContext())//
                                    .load(image_url)//
                                    .asBitmap()//

                                    .centerCrop()//
                                    .into(new BitmapImageViewTarget(imageViewprofile) {
                                        @Override
                                        protected void setResource(Bitmap bitmap) {
                                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                            byte[] b = baos.toByteArray();
                                            imageViewprofile.setVisibility(View.VISIBLE);
                                            lottieAnim.setVisibility(View.GONE);
                                            pd = new ProgressDialog(RegisterNow.this);
                                            pd.setMessage("Getting Facebook Details...");
                                            pd.setCancelable(false);
                                            pd.show();
                                            StorageReference storageRef = storage.getReferenceFromUrl("gs://chatbox-8abc4.appspot.com");
                                            final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());


                                            final StorageReference riversRef = storageRef.child("/" + timeStamp + ".jpg");
                                            riversRef.putBytes(b)
                                                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                        @Override
                                                        public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                                                            isImageUploaded = true;
                                                            pd.dismiss();

                                                            riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                @Override
                                                                public void onSuccess(Uri uri) {
                                                                    UserImageUrl = timeStamp + ".jpg";
                                                                    isFromFacebook = true;
                                                                    validator.validate();


                                                                }
                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception exception) {

                                                                    pd.dismiss();
                                                                }
                                                            });
                                                        }
                                                    })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception exception) {

                                                            pd.dismiss();


                                                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                                        }
                                                    });

                                        }
                                    });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email,id,birthday,photos,location,gender,age_range");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onValidationSucceeded() {


        if (isFromFacebook) {

            if (isImageUploaded) {
                if (UserImageUrl.equalsIgnoreCase("sassa")) {
                    Toast.makeText(getApplicationContext(), "Upload Clear Pic again and Continue..", Toast.LENGTH_SHORT).show();
                } else {
                    if (Utils.CheckIf2G()) {
                        Utils.TakingTimeMsg(RegisterNow.this);
                    }
                    if (EditTextName.getText().toString().replaceAll(" ", "").length() <= 3) {
                        Toast.makeText(getApplicationContext(), "Please Enter Valid Name", Toast.LENGTH_SHORT).show();
                    } else progressBar.setVisibility(View.VISIBLE);
                    buttonRegister.setVisibility(View.INVISIBLE);


                    JSONObject jsonObject = new JSONObject();
                    String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                    String url = Singleton.BaseUrl + "RegisterFacebook?Name=" + EditTextName.getText().toString().trim() + "&EmailAddress=" + EditTextEmail.getText().toString().trim() + "&Password=" + EditTextPassword.getText().toString().trim() + "&dated=" + date + "&Gender=" + DBUtil.getGender() + "&Birthday=" + DBUtil.getAge() + "&ImageUrl=" + UserImageUrl + "";
                    url=url.replace(" ","%20");
                    RegistrationController controller = new RegistrationController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
                        @Override
                        public void handleResponse(ResponsePayload responseObject) {

                            try {
                                if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof RegistrationResponse) {
                                    final RegistrationResponse response = (RegistrationResponse) responseObject.getResponseObject();
                                    try {
                                        progressBar.setVisibility(View.GONE);
                                        buttonRegister.setVisibility(View.VISIBLE);
                                        Toast.makeText(getApplicationContext(), response.ResponseMessage, Toast.LENGTH_SHORT).show();
                                        if (response.Status == 1) {
                                            //Answers.getInstance().logCustom(new CustomEvent("New User"));
                                            DBUtil.setUSERID(String.valueOf(response.User.getId()));
                                            RegisterOnChatServer(response.User.getId());
                                            DBUtil.setUserCity(response.User.getCityName());
                                            DBUtil.setName(EditTextName.getText().toString());
                                            DBUtil.setInviteURL(response.User.getInviteUrl());
                                            DBUtil.setShortBio(response.User.getShortBio());
                                            DBUtil.setShowOnlineStatus(String.valueOf(response.User.getShowOnlineStatus()));
                                            DBUtil.setShowLocationStatus(String.valueOf(response.User.getShowLocationStatus()));
                                            String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + UserImageUrl + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                            DBUtil.setUserImage(image1);
                                            sessionManager.SetRegistered(true);
                                            sessionManager.SetisProfileInfoAdded(true);
                                            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                @Override
                                                public void onBackground() {
                                                    try {
                                                        DBUtil.setGenderPreference("3");
                                                        DBUtil.setMaxAgePreference("60");
                                                        DBUtil.setMinAgePreference("18");
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }


                                            });
                                            startService(new Intent(RegisterNow.this, SaveUserDeviceInfo.class));

                                            Intent intent = new Intent(RegisterNow.this, GetLocationActivity.class);
                                            intent.putExtra("isFrom", "Registration");
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.enter, R.anim.exit);
                                            finish();

                                        } else {
                                            if (response.Status == 2) {
                                                //Answers.getInstance().logCustom(new CustomEvent("Old User"));
                                                DBUtil.setUSERID(String.valueOf(response.User.getId()));
                                                RegisterOnChatServer(response.User.getId());
                                                DBUtil.setAge(response.User.getBirthday());
                                                DBUtil.setShowLocationStatus(String.valueOf(response.User.getShowLocationStatus()));
                                                DBUtil.setShowOnlineStatus(String.valueOf(response.User.getShowOnlineStatus()));
                                                DBUtil.setUserCity(response.User.getCityName());
                                                DBUtil.setName(EditTextName.getText().toString());
                                                DBUtil.setInviteURL(response.User.getInviteUrl());
                                                DBUtil.setShortBio(response.User.getShortBio());
                                                String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + response.User.getUserImages().get(0).UserImageUrl + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                                DBUtil.setUserImage(image1);
                                                if (response.User.getUserImages().size() > 1) {
                                                    DBUtil.setFirstSelfie("1");
                                                }
                                                sessionManager.SetRegistered(true);
                                                if (response.User.getHasPreference()) {
                                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                        @Override
                                                        public void onBackground() {
                                                            try {
                                                                DBUtil.setGenderPreference(String.valueOf(response.User.getUserPreferences().getGenderPreference()));
                                                                DBUtil.setMaxAgePreference(String.valueOf(response.User.getUserPreferences().getMaxAgePreference()));
                                                                DBUtil.setMinAgePreference(String.valueOf(response.User.getUserPreferences().getMinAgePreference()));
                                                                DBUtil.setUserPreferences("1");
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }


                                                    });
                                                } else {
                                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                        @Override
                                                        public void onBackground() {
                                                            try {
                                                                DBUtil.setGenderPreference("3");
                                                                DBUtil.setMaxAgePreference("60");
                                                                DBUtil.setMinAgePreference("18");
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }


                                                    });
                                                }

                                                if (response.User.getBirthday().toString().equalsIgnoreCase("")) {

                                                    Intent intent = new Intent(RegisterNow.this, RegisterTwo.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                                    finish();
                                                } else {
                                                    sessionManager.SetisProfileInfoAdded(true);
                                                    Intent intent = new Intent(RegisterNow.this, MainActivity.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                                    finish();
                                                }
                                            }
                                        }
                                        progressBar.setVisibility(View.GONE);
                                    } catch (Throwable e) {
                                        progressBar.setVisibility(View.GONE);
                                        buttonRegister.setVisibility(View.VISIBLE);
                                    }
                                } else {

                                }
                            } catch (Exception e) {
                                progressBar.setVisibility(View.GONE);
                                e.printStackTrace();
                            }
                        }
                    }, url);
                    controller.setRequestObj(jsonObject);
                    controller.sendRequest();
                }


            } else {

                Toast.makeText(getApplicationContext(), "Upload Pic and Continue Registration.", Toast.LENGTH_SHORT).show();


            }

        } else {
            if (isImageUploaded) {
                if (UserImageUrl.equalsIgnoreCase("sassa")) {
                    Toast.makeText(getApplicationContext(), "Upload Clear Pic again and Continue..", Toast.LENGTH_SHORT).show();
                } else {
                    if (Utils.CheckIf2G()) {
                        Utils.TakingTimeMsg(RegisterNow.this);
                    }
                    if (EditTextName.getText().toString().replaceAll(" ", "").length() <= 3) {
                        Toast.makeText(getApplicationContext(), "Please Enter Valid Name", Toast.LENGTH_SHORT).show();
                    } else progressBar.setVisibility(View.VISIBLE);
                    buttonRegister.setVisibility(View.INVISIBLE);


                    JSONObject jsonObject = new JSONObject();
                    String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                    String url = Singleton.BaseUrl + "Register?Name=" + EditTextName.getText().toString().trim() + "&EmailAddress=" + EditTextEmail.getText().toString().trim() + "&Password=" + EditTextPassword.getText().toString().trim() + "&dated=" + date + "&ImageUrl=" + UserImageUrl + "";
                    RegistrationController controller = new RegistrationController(ChatApplication.getInstance().getApplicationContext(), new RequestListener() {
                        @Override
                        public void handleResponse(ResponsePayload responseObject) {

                            try {
                                if (responseObject != null && responseObject.getStatus() == ResponsePayload.ResponseStatus.SUCCESS && responseObject.getResponseObject() instanceof RegistrationResponse) {
                                    final RegistrationResponse response = (RegistrationResponse) responseObject.getResponseObject();
                                    try {
                                        progressBar.setVisibility(View.GONE);
                                        buttonRegister.setVisibility(View.VISIBLE);
                                        Toast.makeText(getApplicationContext(), response.ResponseMessage, Toast.LENGTH_SHORT).show();
                                        if (response.Status == 1) {
                                         //   Answers.getInstance().logCustom(new CustomEvent("New User"));
                                            DBUtil.setUSERID(String.valueOf(response.User.getId()));
                                            RegisterOnChatServer(response.User.getId());
                                            DBUtil.setGender("1");
                                            DBUtil.setUserCity(response.User.getCityName());
                                            DBUtil.setName(EditTextName.getText().toString());
                                            DBUtil.setInviteURL(response.User.getInviteUrl());
                                            DBUtil.setAge(response.User.getBirthday());
                                            DBUtil.setShortBio(response.User.getShortBio());
                                            DBUtil.setShowOnlineStatus(String.valueOf(response.User.getShowOnlineStatus()));
                                            DBUtil.setShowLocationStatus(String.valueOf(response.User.getShowLocationStatus()));
                                            String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + UserImageUrl + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                            DBUtil.setUserImage(image1);
                                            sessionManager.SetRegistered(true);
                                            ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                @Override
                                                public void onBackground() {
                                                    try {
                                                        DBUtil.setGenderPreference("3");
                                                        DBUtil.setMaxAgePreference("60");
                                                        DBUtil.setMinAgePreference("18");
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }


                                            });
                                            startService(new Intent(RegisterNow.this, SaveUserDeviceInfo.class));

                                            Intent intent = new Intent(RegisterNow.this, RegisterTwo.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.enter, R.anim.exit);
                                            finish();

                                        } else {
                                            if (response.Status == 2) {
                                              //  Answers.getInstance().logCustom(new CustomEvent("Old User"));
                                                DBUtil.setUSERID(String.valueOf(response.User.getId()));
                                                RegisterOnChatServer(response.User.getId());
                                                DBUtil.setAge(response.User.getBirthday());
                                                DBUtil.setShowLocationStatus(String.valueOf(response.User.getShowLocationStatus()));
                                                DBUtil.setShowOnlineStatus(String.valueOf(response.User.getShowOnlineStatus()));
                                                DBUtil.setUserCity(response.User.getCityName());
                                                DBUtil.setName(EditTextName.getText().toString());
                                                DBUtil.setInviteURL(response.User.getInviteUrl());
                                                DBUtil.setShortBio(response.User.getShortBio());
                                                String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + response.User.getUserImages().get(0).UserImageUrl + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                                DBUtil.setUserImage(image1);
                                                if (response.User.getUserImages().size() > 1) {
                                                    DBUtil.setFirstSelfie("1");
                                                }
                                                sessionManager.SetRegistered(true);
                                                if (response.User.getHasPreference()) {
                                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                        @Override
                                                        public void onBackground() {
                                                            try {
                                                                DBUtil.setGenderPreference(String.valueOf(response.User.getUserPreferences().getGenderPreference()));
                                                                DBUtil.setMaxAgePreference(String.valueOf(response.User.getUserPreferences().getMaxAgePreference()));
                                                                DBUtil.setMinAgePreference(String.valueOf(response.User.getUserPreferences().getMinAgePreference()));
                                                                DBUtil.setUserPreferences("1");
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }


                                                    });
                                                } else {
                                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                                        @Override
                                                        public void onBackground() {
                                                            try {
                                                                DBUtil.setGenderPreference("3");
                                                                DBUtil.setMaxAgePreference("60");
                                                                DBUtil.setMinAgePreference("18");
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }


                                                    });
                                                }

                                                if (response.User.getBirthday().toString().equalsIgnoreCase("")) {
                                                    sessionManager.SetisProfileInfoAdded(false);
                                                    Intent intent = new Intent(RegisterNow.this, RegisterTwo.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                                    finish();
                                                } else {
                                                    sessionManager.SetisProfileInfoAdded(true);
                                                    Intent intent = new Intent(RegisterNow.this, MainActivity.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                                    finish();
                                                }
                                            }
                                        }
                                        progressBar.setVisibility(View.GONE);
                                    } catch (Throwable e) {
                                        progressBar.setVisibility(View.GONE);
                                        buttonRegister.setVisibility(View.VISIBLE);
                                    }
                                } else {

                                }
                            } catch (Exception e) {
                                progressBar.setVisibility(View.GONE);
                                e.printStackTrace();
                            }
                        }
                    }, url);
                    controller.setRequestObj(jsonObject);
                    controller.sendRequest();
                }


            } else {

                Toast.makeText(getApplicationContext(), "Upload Pic and Continue Registration.", Toast.LENGTH_SHORT).show();


            }
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    public void RegisterOnChatServer(int UserId) {

        String url = "http://52.41.229.242:9090/plugins/userService/userservice?type=add&secret=BjMVaoEu&username=" + UserId + "&password=" + UserId + "&name=" + EditTextName.getText().toString() + "&email=" + EditTextEmail.getText().toString() + "";
        RequestQueue queue = Volley.newRequestQueue(RegisterNow.this);

        url = url.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });

        queue.add(stringRequest);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_PICK) {
                ImagePicker.beginCrop(this, resultCode, data);
            } else if (requestCode == ImagePicker.REQUEST_CROP) {
                Bitmap bitmap = ImagePicker.getImageCropped(this, resultCode, data,
                        ImagePicker.ResizeType.FIXED_SIZE, 500);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                Glide.with(RegisterNow.this).load(b)
                        .placeholder(R.drawable.defalut_profile)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .bitmapTransform(new CropCircleTransformation(RegisterNow.this)).into(imageViewprofile);
                imageViewprofile.setVisibility(View.VISIBLE);
                lottieAnim.setVisibility(View.GONE);
                pd = new ProgressDialog(RegisterNow.this);
                pd.setMessage("Uploading Image...");
                pd.setCancelable(false);
                pd.show();
                StorageReference storageRef = storage.getReferenceFromUrl("gs://chatbox-8abc4.appspot.com");
                final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());


                final StorageReference riversRef = storageRef.child("/" + timeStamp + ".jpg");
                riversRef.putBytes(b)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                                isImageUploaded = true;
                                pd.dismiss();

                                riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        UserImageUrl = timeStamp + ".jpg";


                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {

                                        pd.dismiss();
                                    }
                                });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {

                                pd.dismiss();


                                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });

            } else {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {


                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {

            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


        }
    }


}
