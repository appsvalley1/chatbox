package com.dating.chatbox.GoLive;

import org.json.JSONObject;

/**
 * Created by yt on 2017/12/14/014.
 */

public class MessageBean {
    private String account;
    private JSONObject message;
    private int background;
    private boolean beSelf;




    public MessageBean(String account, JSONObject message, boolean beSelf) {
        this.account = account;
        this.message = message;
        this.beSelf = beSelf;

    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public JSONObject getMessage() {
        return message;
    }

    public void setMessage(JSONObject message) {
        this.message = message;
    }

    public boolean isBeSelf() {
        return beSelf;
    }

    public void setBeSelf(boolean beSelf) {
        this.beSelf = beSelf;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
