package com.dating.chatbox.GoLive;

import android.view.View;

public interface VideoViewEventListener {
    void onItemDoubleClick(View v, Object item);
}
