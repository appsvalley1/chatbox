package com.dating.chatbox.GoLive;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.R;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


/**
 * Created by yt on 2017/12/14/014.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private List<MessageBean> messageBeanList;
    protected final LayoutInflater inflater;

    public MessageAdapter(Context context, List<MessageBean> messageBeanList) {
        inflater = ((Activity) context).getLayoutInflater();
        this.messageBeanList = messageBeanList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.msg_item_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        setupView(holder, position);

    }

    @Override
    public int getItemCount() {

        return messageBeanList.size();
    }


    private void setupView(MyViewHolder holder, int position) {

        MessageBean bean = messageBeanList.get(position);

        if (bean.isBeSelf()) {
            holder.textViewSelfName.setText(bean.getAccount());
            holder.textViewSelfMsg.setText(bean.getMessage().optString("msg"));

        } else {

            Glide.with(ChatApplication.getInstance()).load(bean.getMessage().optString("ImageUrl")) .bitmapTransform(new CropCircleTransformation(ChatApplication.getInstance())).into(holder.textViewOtherName);
            holder.textViewOtherMsg.setText(bean.getMessage().optString("msg"));

        }

        holder.layoutRight.setVisibility(bean.isBeSelf() ? View.VISIBLE : View.GONE);
        holder.layoutLeft.setVisibility(bean.isBeSelf() ? View.GONE : View.VISIBLE);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView textViewOtherName;
        private TextView textViewOtherMsg;
        private TextView textViewSelfName;
        private TextView textViewSelfMsg;
        private RelativeLayout layoutLeft;
        private RelativeLayout layoutRight;

        public MyViewHolder(View itemView) {
            super(itemView);

            textViewOtherName = itemView.findViewById(R.id.item_name_l);
            textViewOtherMsg = (TextView) itemView.findViewById(R.id.item_msg_l);
            textViewSelfName = (TextView) itemView.findViewById(R.id.item_name_r);
            textViewSelfMsg = (TextView) itemView.findViewById(R.id.item_msg_r);
            layoutLeft = (RelativeLayout) itemView.findViewById(R.id.item_layout_l);
            layoutRight = (RelativeLayout) itemView.findViewById(R.id.item_layout_r);
        }
    }
}
