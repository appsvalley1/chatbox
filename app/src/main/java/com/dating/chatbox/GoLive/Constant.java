package com.dating.chatbox.GoLive;

import com.dating.chatbox.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.agora.rtc.RtcEngine;

public class Constant {

    public static final String MEDIA_SDK_VERSION;

    static {
        String sdk = "undefined";
        try {
            sdk = RtcEngine.getSdkVersion();
        } catch (Throwable e) {
        }
        MEDIA_SDK_VERSION = sdk;
    }
    public static int MAX_INPUT_NAME_LENGTH = 128;

    public static Random RANDOM = new Random();

    public static final int[] COLOR_ARRAY = new int[]{R.drawable.shape_circle_black, R.drawable.shape_circle_blue, R.drawable.shape_circle_pink,
            R.drawable.shape_circle_pink_dark, R.drawable.shape_circle_yellow, R.drawable.shape_circle_red};

    private static List<MessageListBean> messageListBeanList = new ArrayList<>();


    public static void addMessageListBeanList(MessageListBean messageListBean) {
        messageListBeanList.add(messageListBean);

    }

    //logout clean list
    public static void cleanMessageListBeanList() {
        messageListBeanList.clear();
    }

    public static MessageListBean getExistMesageListBean(String accountOther) {
        int ret = existMessageListBean(accountOther);
        if (ret > -1) {

            return messageListBeanList.remove(ret);
        }
        return null;
    }

    //return exist list position
    private static int existMessageListBean(String accountOther) {
        int size = messageListBeanList.size();

        for (int i = 0; i < size; i++) {
            if (messageListBeanList.get(i).getAccountOther().equals(accountOther)) {

                return i;
            }
        }
        return -1;
    }


}
