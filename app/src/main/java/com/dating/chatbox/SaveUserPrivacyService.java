package com.dating.chatbox;

import android.app.IntentService;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.db.DBUtil;


/**
 * ShowMania live game show
 */
public class SaveUserPrivacyService extends IntentService {

    public SaveUserPrivacyService() {
        super("SaveUserPrivacyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
                @Override
                public void onRequestFinished(Request<String> request) {

                }
            });

            String url = Singleton.BaseUrl + "UpdateUserPrivacy?UserId=" + DBUtil.getUSERID() + "&ShowOnlineStatus=" + DBUtil.getShowOnlineStatus() + "&ShowLocationStatus=" + DBUtil.getShowLocationStatus();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}