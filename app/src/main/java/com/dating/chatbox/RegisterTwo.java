package com.dating.chatbox;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ResponseManager;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.db.DBUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class RegisterTwo extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, Validator.ValidationListener {
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    int Year, Month, Day;
    Validator validator;
    private LinearLayout lvMale, lvfemale;
    private ImageView imageViewMale, imageViewFemale, imageViewprofile;
    private TextView textViewMale, textViewFemale;
    @NotEmpty
    private EditText editBirthday;
    private TextView textViewName;
    private User user = new User();
    private String Gender = "1";
    private String DOB = "";
    private ProgressBar progressBar;
    private Button ButtonAddPersonalInfo;
    private SessionManager sessionManager;
    SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_two);
        sessionManager = new SessionManager(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        findviewsbyid();
        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
            String ImageUrl,name;
            @Override
            public void onBackground() {
                try {
                    ImageUrl =DBUtil.getUserImage();
                    name=DBUtil.getName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                Glide.with(RegisterTwo.this)
                        .load(ImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).bitmapTransform(new CropCircleTransformation(RegisterTwo.this))
                        .into(imageViewprofile);
                textViewName.setText("Welcome  " + name);
                super.onUi();
            }
        });
        lvMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvMale.setBackground(getResources().getDrawable(R.drawable.customroundedpurplebutton));
                lvfemale.setBackground(getResources().getDrawable(R.drawable.customroundedgrayborderbutton));
                imageViewMale.setImageResource(R.drawable.malewhite);
                imageViewFemale.setImageResource(R.drawable.femaleblack);
                textViewMale.setTextColor(getResources().getColor(R.color.White));
                textViewFemale.setTextColor(getResources().getColor(R.color.Black));
                Gender = "1";

            }
        });
        lvfemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvfemale.setBackground(getResources().getDrawable(R.drawable.customroundedpurplebutton));
                lvMale.setBackground(getResources().getDrawable(R.drawable.customroundedgrayborderbutton));
                imageViewFemale.setImageResource(R.drawable.femalewhite);
                imageViewMale.setImageResource(R.drawable.maleblack);
                textViewMale.setTextColor(getResources().getColor(R.color.Black));
                textViewFemale.setTextColor(getResources().getColor(R.color.White));
                Gender = "2";


            }
        });
        calendar = Calendar.getInstance();
        Year = calendar.get(Calendar.YEAR);
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);
        editBirthday.setCompoundDrawables(getResources().getDrawable(R.drawable.cakeimage), null, null, null);
        editBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDate(Year, Month, Day, R.style.NumberPickerStyle);
            }
        });

        ButtonAddPersonalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

    }

    private void findviewsbyid() {
        lvMale = (LinearLayout) findViewById(R.id.lvMale);
        lvfemale = (LinearLayout) findViewById(R.id.lvfemale);
        imageViewMale = (ImageView) findViewById(R.id.imageViewMale);
        imageViewFemale = (ImageView) findViewById(R.id.imageViewFemale);
        textViewMale = (TextView) findViewById(R.id.textViewMale);
        textViewFemale = (TextView) findViewById(R.id.textViewFemale);
        editBirthday = (EditText) findViewById(R.id.editBirthday);
        textViewName = (TextView) findViewById(R.id.textViewName);
        imageViewprofile = (ImageView) findViewById(R.id.imageViewprofile);
        ButtonAddPersonalInfo = (Button) findViewById(R.id.ButtonAddPersonalInfo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    @Override
    public void onDateSet(DatePicker view, int years, int monthOfYear, int dayOfMonth) {


        String date = +Day + "/" + Month + "/" + years;
        DOB = date;
        int minYear = calendar.get(Calendar.YEAR) - 18;
        if (minYear >= years) {
            editBirthday.setText(date);
        } else {
            editBirthday.getText().clear();
            Toast.makeText(getApplicationContext(), "Your age must be greater then 18", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onValidationSucceeded() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(RegisterTwo.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(RegisterTwo.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "UserUpdateGenderBirthdays?UserID=" + DBUtil.getUSERID() + "&Gender=" + Gender + "&Birthday=" + editBirthday.getText().toString();
        url = url.replaceAll(" ", "%20");
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            // Display the first 500 characters of the response string.
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();
                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            if (responseManager.getStatus() == 1) {
                             //   Answers.getInstance().logCustom(new CustomEvent("Profile Info Added"));
                                DBUtil.setGender(Gender);
                                final JSONObject userJson = jsonObject.optJSONObject("User");
                                DBUtil.setAge(userJson.optString("Birthday"));
                                Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();
                                sessionManager.SetisProfileInfoAdded(true);
                                Intent intent = new Intent(RegisterTwo.this, GetLocationActivity.class);
                                intent.putExtra("isFrom", "Registration");
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();

                            }


                            progressBar.setVisibility(View.GONE);


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @VisibleForTesting
    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        new SpinnerDatePickerDialogBuilder()
                .context(RegisterTwo.this)
                .callback(RegisterTwo.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year - 18, monthOfYear, dayOfMonth)
                .build()
                .show();
    }
}
