package com.dating.chatbox.Custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by USER on 9/19/2017.
 */

public class SessionManager {
    public static final String PREF_NAME = "chatbox";
    public static final String KEY_IsPrivacyClicked = "KEY_IsPrivacyClicked";
    public static final String KEY_IsRegistered = "KEY_IsRegistered";

    public static final String KEY_isDeviceMessengingIDAdded = "KEY_isDeviceMessengingIDAdded";
    public static final String KEY_isProfileInfoAdded = "KEY_isProfileInfoAdded";
    public static final String KEY_OneTimeTutorial = "OneTimeTutorial";

    private final SharedPreferences.Editor editor;
    private final Context _context;
    private final int PRIVATE_MODE = 0;
    private final SharedPreferences pref;
    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void SetPrivacyClicked(Boolean isActive) {
        try {
            editor.putBoolean(KEY_IsPrivacyClicked, isActive);
            editor.commit();
        } catch (Exception ex) {

        }
    }
    public void clearAllData() {
        try {
            editor.clear();
            editor.apply();
        } catch (Exception ex) {

        }
    }

    public Boolean GetPrivacyClicked() {
        return pref.getBoolean(KEY_IsPrivacyClicked, false);
    }
    public void SetRegistered(Boolean isActive) {
        try {
            editor.putBoolean(KEY_IsRegistered, isActive);
            editor.commit();
        } catch (Exception ex) {

        }
    }

    public Boolean GetRegistered() {
        return pref.getBoolean(KEY_IsRegistered, false);
    }


    public void SetisDeviceMessengingIDAdded(Boolean MessengingID) {
        try {
            editor.putBoolean(KEY_isDeviceMessengingIDAdded, MessengingID);
            editor.commit();
        } catch (Exception ex) {

        }
    }

    public Boolean GetisDeviceMessengingIDAdded() {
        return pref.getBoolean(KEY_isDeviceMessengingIDAdded, false);
    }
    public void SetisProfileInfoAdded(Boolean MessengingID) {
        try {
            editor.putBoolean(KEY_isProfileInfoAdded, MessengingID);
            editor.commit();
        } catch (Exception ex) {

        }
    }

    public Boolean GetisProfileInfoAdded() {
        return pref.getBoolean(KEY_isProfileInfoAdded, false);
    }

    public void SetOneTimeTutorial(Boolean OneTimeTutorial) {
        try {
            editor.putBoolean(KEY_OneTimeTutorial, OneTimeTutorial);
            editor.apply();
        } catch (Exception ex) {

        }
    }

    public Boolean GetOneTimeTutorial() {
        return pref.getBoolean(KEY_OneTimeTutorial, false);
    }


}
