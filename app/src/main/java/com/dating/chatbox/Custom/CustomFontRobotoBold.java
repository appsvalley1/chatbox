package com.dating.chatbox.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * 
 * @author TaskBucks.com
 * 
 */
public class CustomFontRobotoBold extends TextView {

	public CustomFontRobotoBold(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public CustomFontRobotoBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomFontRobotoBold(Context context) {
		super(context);
		init();
	}

	private void init() {
		try {
			if (!isInEditMode()) {
				Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/proximanova_bold.otf");
				setTypeface(tf);
			}
		} catch (Exception e) {

		}
	}
}