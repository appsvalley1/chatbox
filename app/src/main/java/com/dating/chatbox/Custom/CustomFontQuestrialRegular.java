package com.dating.chatbox.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class CustomFontQuestrialRegular extends TextView {

	public CustomFontQuestrialRegular(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public CustomFontQuestrialRegular(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomFontQuestrialRegular(Context context) {
		super(context);
		init();
	}

	private void init() {
		try {
			if (!isInEditMode()) {
				Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/proximanova_light.otf");
				setTypeface(tf);
			}
		} catch (Exception e) {

		}
	}
}