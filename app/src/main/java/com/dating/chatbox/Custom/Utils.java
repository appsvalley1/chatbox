package com.dating.chatbox.Custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.dating.chatbox.BuildConfig;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.controllers.base.ResponsePayload;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by USER on 9/19/2017.
 */

public class Utils {

    static long toastTime;
    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + " day ago ";
            } else {
                time = diffDays + " days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + " hr ago";
                } else {
                    time = diffHours + " hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + " min ago";
                    } else {
                        time = diffMinutes + " mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + " secs ago";
                    }
                }

            }

        }
        return time;
    }

    public static int getAppVersionCode() {
        int versionCode = BuildConfig.VERSION_CODE;

        return versionCode;
    }

    public static void openChatsePlayStore(Context context) {
        final String appPackageName = "com.dating.chatbox";
        try {
            try {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }

        } catch (Exception ignored) {
        }
    }

    public static boolean CheckIf2G() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) ChatApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager == null) {
                return false;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            TelephonyManager tm = (TelephonyManager) ChatApplication.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
            if (activeNetworkInfo == null || tm == null) {
                return false;
            }
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE && tm.getNetworkType() == TelephonyManager.NETWORK_TYPE_EDGE) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    public static void TakingTimeMsg(final Activity context) {
        try {
            final Toast toast = Toast.makeText(context, "Taking time, Internet seems Slow...", Toast.LENGTH_SHORT);
            if (context != null && !context.isFinishing())
                toast.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (context != null && !context.isFinishing())
                        toast.show();
                }
            }, 1000);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public static void lateResToast(final Context context) {
        try {
            final Toast toast = Toast.makeText(context,"Internet seems slow", Toast.LENGTH_LONG);
            try {
                if (ChatApplication.getInstance().isAppForeground) {
                    toast.show();
                }
            } catch (Exception e) {
            }
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (ChatApplication.getInstance().isAppForeground)
                            toast.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 1000);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public static boolean isConnected() {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) ChatApplication.getInstance()
                    .getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()
                    && networkInfo.isAvailable()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }
    public static ResponsePayload handleError(ResponsePayload mResponsePayload, VolleyError error, String TAG, boolean showToast) {
        if (error instanceof NetworkError || error instanceof NoConnectionError) {
            mResponsePayload.setStatus(ResponsePayload.ResponseStatus.NO_NETWORK);
            // Utility.showToastWithMsg(mContext.getString(R.string.internet_connection));
            Log.e(TAG, "NetworkError");
        } else if (error instanceof ServerError) {
            mResponsePayload.setStatus(ResponsePayload.ResponseStatus.SERVER_ERROR);
            Log.e(TAG, "ServerError");
            if (showToast) {
                Utils.showToastWithMsg("Error_Sausage");
            }
        } else if (error instanceof ParseError) {
            mResponsePayload.setStatus(ResponsePayload.ResponseStatus.PARSE_ERROR);
            if (showToast) {
                Utils.showToastWithMsg("Error_Pakoda");
            }
            Log.e(TAG, "ParseError");
        } else if (error instanceof TimeoutError) {
            mResponsePayload.setStatus(ResponsePayload.ResponseStatus.TIMEOUT_ERROR);
            if (showToast) {
                Utils.showToastWithMsg("Error_Tequila");
            }
            Log.e(TAG, "TimeoutError");
        } else {
            mResponsePayload.setStatus(ResponsePayload.ResponseStatus.EXCEPTION);
        }
        return mResponsePayload;
    }
    public static void showToastWithMsg(String message) {
        try {
            if (toastTime == 0 || System.currentTimeMillis() > (toastTime + 50)) {
                toastTime = System.currentTimeMillis();
                Toast.makeText(ChatApplication.getInstance(),
                        message, Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        }
    }
    public static String handleResponse(String tag, NetworkResponse response) {
        StringBuilder output = new StringBuilder(); // note: better to use StringBuilder
        try {
            try {
                final InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(response.data));
                final BufferedReader in = new BufferedReader(reader);
                String read;
                while ((read = in.readLine()) != null) {
                    output = output.append(read);
                }
                reader.close();
                in.close();
                String str = output.toString();
                Log.e(tag, str);
                return output.toString();
            } catch (Throwable e) {
            }

        } catch (Throwable e) {
        }
        return "";
    }

}
