package com.dating.chatbox.Custom;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class ChatBubbleManager implements Application.ActivityLifecycleCallbacks {

    private static final long CHECK_DELAY = 500;
    private static ChatBubbleManager instance;
    private boolean foreground = false, paused = true;
    private Handler handler = new Handler();
    private List<Listener> listeners = new CopyOnWriteArrayList<Listener>();
    private Runnable check;

    private static ChatBubbleManager init(Application application) {
        if (instance == null) {
            try {
                instance = new ChatBubbleManager();
                application.registerActivityLifecycleCallbacks(instance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public static ChatBubbleManager get(Application application) {
        if (instance == null) {
            init(application);
        }
        return instance;
    }

    public static ChatBubbleManager get(Context ctx) {
        if (instance == null) {
            try {
                Context appCtx = ctx.getApplicationContext();
                if (appCtx instanceof Application) {
                    init((Application) appCtx);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public static ChatBubbleManager get() {
        return instance;
    }

    public boolean isBackground() {
        return !foreground;
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        paused = false;
        boolean wasBackground = !foreground;
        foreground = true;

        if (check != null)
            handler.removeCallbacksAndMessages(null);

        if (wasBackground) {
            for (Listener l : listeners) {
                try {
                    l.onBecameForeground();
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused = true;

        if (check != null)
            handler.removeCallbacksAndMessages(null);

        handler.postDelayed(check = new Runnable() {
            @Override
            public void run() {
                if (foreground && paused) {
                    foreground = false;
                    for (Listener l : listeners) {
                        try {
                            l.onBecameBackground();
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }
                    }
                }
            }
        }, CHECK_DELAY);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    public interface Listener {
        void onBecameForeground();

        void onBecameBackground();
    }
}