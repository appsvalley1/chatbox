package com.dating.chatbox.Custom;


import com.google.android.gms.ads.AdView;

/**
 * @author TaskBucks.com
 */
public class CustomNativeAdsManager {
    public static CustomNativeAdsManager customNativeAdsManager;
    public AdView adMobBanner;
    private int AdsType = -1;
    //private MvNativeHandler NativeHandle;
    // private Campaign campaign;
    //  private List<Campaign> campaignList;
    private Boolean hasFirstAd = false;

    public static CustomNativeAdsManager getCustomNativeAdsManager() {
        return customNativeAdsManager;
    }

    public void setCustomNativeAdsManager(CustomNativeAdsManager customNativeAdsManager) {
        this.customNativeAdsManager = customNativeAdsManager;
    }
    // public InterstitialAd interstitialAd;


    // public MVRewardVideoHandler MobvistaInterestialAds;

    //  public MVRewardVideoHandler getMobvistaInterestialAds() {
    //   return MobvistaInterestialAds;
    //   }

  /*  public void setMobvistaInterestialAds(MVRewardVideoHandler mobvistaInterestialAds) {
        MobvistaInterestialAds = mobvistaInterestialAds;
    }

    public InterstitialAd getInterstitialAd() {
        return interstitialAd;
    }

    public void setInterstitialAd(InterstitialAd interstitialAd) {
        this.interstitialAd = interstitialAd;
    }*/

    public AdView getAdMobBanner() {
        return adMobBanner;
    }

    public void setAdMobBanner(AdView adMobBanner) {
        this.adMobBanner = adMobBanner;
    }

    public int getAdsType() {
        return AdsType;
    }

    public void setAdsType(int adsType) {
        AdsType = adsType;
    }
/*
    public Item columbiaItemnativeContentAd;

    public Item getColumbiaItemnativeContentAd() {
        return columbiaItemnativeContentAd;
    }

    public void setColumbiaItemnativeContentAd(Item columbiaItemnativeContentAd) {
        this.columbiaItemnativeContentAd = columbiaItemnativeContentAd;
    }*/

    /* public MvNativeHandler getNativeHandle() {
         return NativeHandle;
     }

     public void setNativeHandle(MvNativeHandler nativeHandle) {
         NativeHandle = nativeHandle;
     }

     public Campaign getCampaign() {
         return campaign;
     }

     public void setCampaign(Campaign campaign) {
         this.campaign = campaign;
     }

     public List<Campaign> getCampaignList() {
         return campaignList;
     }

     public void setCampaignList(List<Campaign> campaignList) {
         this.campaignList = campaignList;
     }
 */
    public Boolean getHasFirstAd() {
        return hasFirstAd;
    }

    public void setHasFirstAd(Boolean hasFirstAd) {
        this.hasFirstAd = hasFirstAd;
    }
}
