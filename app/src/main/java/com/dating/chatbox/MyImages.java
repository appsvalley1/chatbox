package com.dating.chatbox;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dating.chatbox.Adapters.MyImagesGridAdapter;
import com.dating.chatbox.Custom.ItemOffsetDecoration;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.EventbusLib.DeleteImage;
import com.dating.chatbox.EventbusLib.UpdateProfileImage;
import com.dating.chatbox.HelperClasses.ImagePicker;
import com.dating.chatbox.Models.ResponseManager;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.Models.UserImages;
import com.dating.chatbox.db.DBUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MyImages extends AppCompatActivity {

    private  ImageView ImageMain;
    private UserImages userImages1 = new UserImages();
    private  User user;
    private  List<UserImages> userImagesDefaultAdded = new ArrayList<>(0);
    private ProgressDialog pd;
    private  FirebaseStorage storage;
    private RecyclerView recycler_view;
    private ProgressBar progressBar;
    private String UserImageUrl;
    private LinearLayout lvUpdateProfile;

    private Dialog firstSelfie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_images);
        lvUpdateProfile =findViewById(R.id.lvUpdateProfile);
        lvUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatApplication.getInstance().IsUserImage = true;
                try {
                    ImagePicker.pickImage(MyImages.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        storage = FirebaseStorage.getInstance();
        Toolbar toolbar =  findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        TextView appname = mCustomView.findViewById(R.id.appname);
        appname.setText("Add Images");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageMain =  findViewById(R.id.ImageMain);
        recycler_view =  findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progressBar);

        LoadMyImages();


        if (!EventBus.getDefault().isRegistered(this)) { EventBus.getDefault().register(this); }


    }

    private void LoadMyImages() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(MyImages.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(MyImages.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "GetUserImagesByUserID?UserID=" + DBUtil.getUSERID();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            userImagesDefaultAdded.clear();
                            JSONArray userImagesarray = new JSONArray(response);
                            for (int j = 0; j < userImagesarray.length(); j++) {
                                JSONObject userImagejson = userImagesarray.optJSONObject(j);
                                UserImages userImages1 = new UserImages();
                                userImages1.setImageId(userImagejson.optInt("ImageId"));
                                userImages1.setFirstImage(userImagejson.optBoolean("IsFirstImage"));
                                String imagelist = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + userImagejson.optString("UserImageUrl") + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                userImages1.setUserImageUrl(imagelist);
                                userImages1.setBlankImage(false);
                                userImagesDefaultAdded.add(userImages1);
                            }
                            if (userImagesarray.length() == 0) {
                                Glide.with(MyImages.this)
                                        .load(user.getImageUrl()).dontAnimate()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL).into(ImageMain);
                            } else {
                                if(!isFinishing())
                                {
                                    Glide.with(MyImages.this)
                                            .load(userImagesDefaultAdded.get(0).UserImageUrl).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).diskCacheStrategy(DiskCacheStrategy.ALL)

                                            .into(ImageMain);
                                    lvUpdateProfile.setVisibility(View.VISIBLE);
                                }

                            }
                            switch (userImagesarray.length()) {
                                case 0:
                                    userImages1 = new UserImages();
                                    userImages1.setBlankImage(true);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);

                                    break;
                                case 1:
                                    userImages1 = new UserImages();
                                    userImages1.setBlankImage(true);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                                        String isSeen;
                                        @Override
                                        public void onBackground() {
                                            try {
                                                isSeen = DBUtil.getFirstSelfie();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onUi() {
                                            super.onUi();
                                            if (isSeen.equalsIgnoreCase("")) {
                                                showFirstSelfiePopup();
                                            }
                                        }
                                    });

                                    break;
                                case 2:
                                    userImages1 = new UserImages();
                                    userImages1.setBlankImage(true);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    break;
                                case 3:
                                    userImages1 = new UserImages();
                                    userImages1.setBlankImage(true);
                                    userImagesDefaultAdded.add(userImages1);
                                    userImagesDefaultAdded.add(userImages1);
                                    break;
                                case 4:
                                    userImages1 = new UserImages();
                                    userImages1.setBlankImage(true);
                                    userImagesDefaultAdded.add(userImages1);
                                    break;
                            }

                            MyImagesGridAdapter mAdapter = new MyImagesGridAdapter(userImagesDefaultAdded, MyImages.this);
                            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3, GridLayoutManager.VERTICAL, false);
                            recycler_view.setLayoutManager(gridLayoutManager);
                            recycler_view.setHasFixedSize(false);
                            recycler_view.setItemAnimator(new DefaultItemAnimator());
                            recycler_view.setAdapter(mAdapter);
                            ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(MyImages.this, R.dimen._2sdp);
                            recycler_view.addItemDecoration(itemDecoration);
                            mAdapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                            try {
                                EventBus.getDefault().post(new UpdateProfileImage());
                            } catch (Throwable e) {

                            }


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_PICK) {
            ImagePicker.beginCrop(this, resultCode, data);
        } else if (requestCode == ImagePicker.REQUEST_CROP) {
            Bitmap bitmap = ImagePicker.getImageCropped(this, resultCode, data,
                    ImagePicker.ResizeType.FIXED_SIZE, 500);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] b = baos.toByteArray();
            if (ChatApplication.getInstance().IsUserImage) {

                try {

                    pd = new ProgressDialog(MyImages.this);
                    pd.setMessage("Uploading Image...");
                    pd.setCancelable(false);
                    pd.show();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://chatbox-8abc4.appspot.com");    //change the url according to your firebase app
                    final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());


                    final StorageReference riversRef = storageRef.child("/" + timeStamp + ".jpg");
                    riversRef.putBytes(b)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                                    pd.dismiss();

                                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            UserImageUrl = timeStamp + ".jpg";

                                            progressBar.setVisibility(View.VISIBLE);
                                            RequestQueue queue = Volley.newRequestQueue(MyImages.this);
                                            queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
                                                @Override
                                                public void onRequestFinished(Request<String> request) {
                                                    progressBar.setVisibility(View.INVISIBLE);
                                                }
                                            });

                                            String url = Singleton.BaseUrl + "UpdateProfileImage?ImageId=" + userImagesDefaultAdded.get(0).getImageId() + "&UserImageUrl=" + UserImageUrl;
                                            if (Utils.CheckIf2G()) {
                                                Utils.TakingTimeMsg(MyImages.this);
                                            }
// Request a string response from the provided URL.
                                            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            try {


                                                                progressBar.setVisibility(View.GONE);
                                                                LoadMyImages();
                                                                String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + UserImageUrl + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
                                                                DBUtil.setUserImage(image1);


                                                            } catch (Exception e) {
                                                                progressBar.setVisibility(View.GONE);
                                                            }
                                                        }
                                                    }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressBar.setVisibility(View.VISIBLE);

                                                }
                                            });
// Add the request to the RequestQueue.
                                            queue.add(stringRequest);


                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            // Handle any errors
                                            pd.dismiss();
                                        }
                                    });
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    //if the upload is not successfull
                                    //hiding the progress dialog
                                    pd.dismiss();

                                    //and displaying error message
                                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });

                } catch (Exception e) {

                }


            } else {
                try {

                    pd = new ProgressDialog(MyImages.this);
                    pd.setMessage("Uploading Image...");
                    pd.setCancelable(false);
                    pd.show();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://chatbox-8abc4.appspot.com");    //change the url according to your firebase app
                    final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());


                    final StorageReference riversRef = storageRef.child("/" + timeStamp + ".jpg");
                    riversRef.putBytes(b)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {

                                    pd.dismiss();

                                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            UserImageUrl = timeStamp + ".jpg";

                                            progressBar.setVisibility(View.VISIBLE);
                                            RequestQueue queue = Volley.newRequestQueue(MyImages.this);
                                            queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
                                                @Override
                                                public void onRequestFinished(Request<String> request) {
                                                    progressBar.setVisibility(View.INVISIBLE);
                                                }
                                            });

                                            String url = Singleton.BaseUrl + "AddImagesToProfile?UserID=" + DBUtil.getUSERID() + "&UserImageUrl=" + UserImageUrl;

// Request a string response from the provided URL.
                                            if (Utils.CheckIf2G()) {
                                                Utils.TakingTimeMsg(MyImages.this);
                                            }
                                            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            try {


                                                                progressBar.setVisibility(View.GONE);
                                                                LoadMyImages();


                                                            } catch (Exception e) {
                                                                progressBar.setVisibility(View.GONE);
                                                            }
                                                        }
                                                    }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressBar.setVisibility(View.VISIBLE);

                                                }
                                            });
// Add the request to the RequestQueue.
                                            queue.add(stringRequest);
                                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            // Handle any errors
                                            pd.dismiss();
                                        }
                                    });
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    //if the upload is not successfull
                                    //hiding the progress dialog
                                    pd.dismiss();

                                    //and displaying error message
                                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });

                } catch (Exception e) {

                }


            }

        }
    }

    public void DeleteMyImages(int imageID) {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(MyImages.this);
        }
        RequestQueue queue = Volley.newRequestQueue(MyImages.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "DeleteImages?ImageId=" + imageID;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();


                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();
                            if (responseManager.getStatus() == 1) {
                                userImagesDefaultAdded.clear();
                                LoadMyImages();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();

    }
    public void showFirstSelfiePopup() {
        try {
            firstSelfie = new Dialog(MyImages.this);
            firstSelfie.requestWindowFeature(Window.FEATURE_NO_TITLE);
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.99);
            firstSelfie.setContentView(R.layout.custom_dialog_upload_selfile);
            firstSelfie.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);

            firstSelfie.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            firstSelfie.setCanceledOnTouchOutside(true);
            Button btnTakeSelfie = firstSelfie.findViewById(R.id.btnTakeSelfie);
            btnTakeSelfie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        try {
                            if (firstSelfie.isShowing())
                                firstSelfie.dismiss();
                            ChatApplication.getInstance().IsUserImage = false;
                            try {
                                ImagePicker.pickImage(MyImages.this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }

                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
            firstSelfie.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setFirstSelfie("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    });
                }
            });

            firstSelfie.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            if (!isFinishing() && !firstSelfie.isShowing())
                firstSelfie.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DeleteImage intent) {
        try {
            DeleteMyImages(intent.getImageID());
        } catch (Exception e) {
            e.printStackTrace();
        }
    };


}
