package com.dating.chatbox;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.chatbox.db.DBUtil;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.Locale;


public class GetLocationActivity extends AppCompatActivity {
    private static final int REQUEST_CHECK_SETTINGS = 12;
    private FusedLocationProviderClient mFusedLocationClient;
    private TextView tvCity;
    private LinearLayout llSelectLocation,  llProgress, root;
    private ImageView ivLocationbg;
    private String isFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            viewBinds();
            Intent intent = getIntent();
            if (intent != null && !TextUtils.isEmpty(intent.getStringExtra("isFrom"))) {
                isFrom = intent.getStringExtra("isFrom");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void viewBinds() {
        try {
            llSelectLocation = findViewById(R.id.llSelectLocation);

            tvCity = findViewById(R.id.tvCity);
            llProgress = findViewById(R.id.llProgress);

            root = findViewById(R.id.root);
            ivLocationbg = findViewById(R.id.ivLocationbg);
            llSelectLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    llProgress.setVisibility(View.VISIBLE);
                    getLocation();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // Check Permissions Now
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CHECK_SETTINGS);
            } else {
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    getAddress(location);
                                } else {
                                    getCurLoc();
                                }

                            }
                        })
                        .addOnFailureListener(this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                getCurLoc();
                            }
                        });
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void getCurLoc() {
        try {
            final LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(this);
            Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
            task.addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(GetLocationActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                        }
                    }
                }
            });
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult != null && locationResult.getLastLocation() != null) {
                            getAddress(locationResult.getLastLocation());
                            mFusedLocationClient.removeLocationUpdates(this);
                        }
                    }
                }, Looper.myLooper());
            }//end of if


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddress(final Location Userlocation) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                List<Address> addresses = null;
                try {
                    Geocoder geocoder = new Geocoder(GetLocationActivity.this, Locale.getDefault());

                    addresses = geocoder.getFromLocation(Userlocation.getLatitude(),
                            Userlocation.getLongitude(), 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (addresses != null && addresses.size() != 0) {
                        Address address = addresses.get(0);
                        String subAdminArea = address.getSubAdminArea();
                        if (TextUtils.isEmpty(subAdminArea)) {
                            subAdminArea = address.getLocality();
                        }
                        final String location = subAdminArea;
                        if (!TextUtils.isEmpty(location)) {

                            DBUtil.setUserCity(location);
                            DBUtil.setLatitude(Userlocation.getLatitude() + "");
                            DBUtil.setLongitude(Userlocation.getLongitude() + "");


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        llProgress.setVisibility(View.INVISIBLE);
                                        tvCity.setText(location);
                                        //  DBUtil.setUserCity(location);

                                        Intent i = new Intent(GetLocationActivity.this, SaveUserCityNameService.class);

                                        startService(i);

                                        if (!TextUtils.isEmpty(isFrom.trim()) && isFrom.equalsIgnoreCase("Registration")) {
                                            startActivity(new Intent(GetLocationActivity.this, MainActivity.class));
                                            overridePendingTransition(R.anim.enter, R.anim.exit);

                                        } else {
                                            Intent intent = new Intent();
                                            intent.putExtra("CITY", location);
                                            setResult(2, intent);
                                        }
                                        finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    llProgress.setVisibility(View.INVISIBLE);
                                    Toast.makeText(GetLocationActivity.this, "Unable to fetch location, please try again", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK) {
            getCurLoc();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("CITY", "");
        setResult(2, intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            //check if all permissions are granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation();

            }


        }
    }
}
