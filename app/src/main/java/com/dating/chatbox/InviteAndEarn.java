package com.dating.chatbox;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ShareCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.Models.User;
import com.dating.chatbox.db.DBUtil;
import com.morsebyte.shailesh.twostagerating.TwoStageRate;


public class InviteAndEarn extends AppCompatActivity {

    public TextView textViewShortURL;
    public String InviteText, InviteTitle;
    String longURL;
    String shareBody;
    private User user;
    private LinearLayout lvTap;
    private Button btnShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_and_earn);
        textViewShortURL = (TextView) findViewById(R.id.textViewShortURL);
        textViewShortURL.setText(DBUtil.getInviteURL());
        lvTap = (LinearLayout) findViewById(R.id.lvTap);
        lvTap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setClipboard(getApplicationContext(), DBUtil.getInviteURL());
                Toast.makeText(getApplicationContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        TextView appname = (TextView) mCustomView.findViewById(R.id.appname);
        appname.setText("Invite & Boost Your Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnShare = findViewById(R.id.btnShare);
        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {


            @Override
            public void onBackground() {
                try {

                    InviteText = DBUtil.getInviteText();
                    shareBody = DBUtil.getInviteURL();
                    InviteTitle = DBUtil.getInviteTitle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Install this awesome chat app");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Install and Boost"));*/
                Uri myUri = Uri.parse(shareBody);
                ShareCompat.IntentBuilder
                        // getActivity() or activity field if within Fragment
                        .from(InviteAndEarn.this)
                        // The text that will be shared
                        .setText(InviteText + " " + myUri)
                        // most general text sharing MIME type
                        .setType("text/plain")






        /*
         * [OPTIONAL] Designate a URI to share. Your type that
         * is set above will have to match the type of data
         * that your designating with this URI. Not sure
         * exactly what happens if you don't do that, but
         * let's not find out.
         *
         * For example, to share an image, you'd do the following:
         *     File imageFile = ...;
         *     Uri uriToImage = ...; // Convert the File to URI
         *     Intent shareImage = ShareCompat.IntentBuilder.from(activity)
         *       .setType("image/png")
         *       .setStream(uriToImage)
         *       .getIntent();
         */

        /*
         * [OPTIONAL] Designate the email recipients as an array
         * of Strings or a single String
         */

        /*
         * [OPTIONAL] Designate the email addresses that will be
         * BCC'd on an email as an array of Strings or a single String
         */
        /*
         * The title of the chooser that the system will show
         * to allow the user to select an app
         */
                        .setChooserTitle(InviteTitle)
                        .startChooser();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Preferences:
                Intent i = new Intent(InviteAndEarn.this, MyPrefrences.class);
                startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void setClipboard(Context context, String text) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }

    @Override
    public void onBackPressed() {
        TwoStageRate.with(InviteAndEarn.this).showIfMeetsConditions();
        super.onBackPressed();
    }
}
