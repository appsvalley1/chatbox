package com.dating.chatbox.controllers.base;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.dating.chatbox.Custom.Utils;
import com.google.gson.Gson;

import org.json.JSONObject;


/**
 * Created by sys3002 on 17/3/18.
 */

public class GsonRequest<T> extends Request<T> {
    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private JSONObject mRequestJson;
    private final Response.Listener<T> listener;
    private String TAG;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url   URL of the request to make
     * @param clazz Relevant class object, for Gson's reflection
     */
    public GsonRequest(String tag, String url, JSONObject mRequestJson, Class<T> clazz,
                       Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.TAG = tag;
        this.clazz = clazz;
        this.mRequestJson = mRequestJson;
        this.listener = listener;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {


        try {

            byte[] requestData = mRequestJson.toString().getBytes("utf-8");
            return requestData;
        } catch (Throwable e) {
            return null;
        }

    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = Utils.handleResponse(TAG, response);

            return Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }
}
