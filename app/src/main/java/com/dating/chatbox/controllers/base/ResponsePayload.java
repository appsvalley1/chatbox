package com.dating.chatbox.controllers.base;

/**
 * Created by ajay on 8/29/2015.
 */
public final class ResponsePayload {

    public interface ResponseStatus {
        int FAILED = 0;
        int SUCCESS = 1;
        int NO_NETWORK = 2;
        int EXCEPTION = 3;
        int SERVER_ERROR = 4;
        int PARSE_ERROR = 5;
        int TIMEOUT_ERROR = 6;
    }

    int mStatus;
    String mMessage;
    Exception mException;
    Object mResponseObject;

    public ResponsePayload() {
        mStatus = ResponseStatus.FAILED;
        mMessage = "";
        mException = null;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int _status) {
        this.mStatus = _status;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String _message) {
        this.mMessage = _message;
    }

    public Exception getException() {
        return mException;
    }

    public void setException(Exception _exception) {
        this.mException = _exception;
    }

    public Object getResponseObject() {
        return mResponseObject;
    }

    public void setResponseObject(Object responseObject) {
        this.mResponseObject = responseObject;
    }
}