package com.dating.chatbox.controllers;

import android.content.Context;

import com.dating.chatbox.controllers.base.BaseController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.response.RegistrationResponse;

import org.json.JSONObject;

/**
 * Created by Prateek on 19-07-2018.
 */



public class RegistrationController extends BaseController {
    public static final String TAG = "DashBoardController";

    public RegistrationController(Context context, RequestListener listener,String url) {
        super(context, listener, RegistrationResponse.class, TAG);
        mUrl = url;
    }

    public void setRequestObj(JSONObject jsonObject) {
        super.mRequestObjectPayload = jsonObject;
    }

}
