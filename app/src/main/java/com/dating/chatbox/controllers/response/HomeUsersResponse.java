package com.dating.chatbox.controllers.response;

import com.dating.chatbox.Models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Prateek on 19-07-2018.
 */
public class HomeUsersResponse {

    @SerializedName("UserList")
    @Expose
    public ArrayList<User> UserList = null;

}
