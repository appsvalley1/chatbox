package com.dating.chatbox.controllers;

import android.content.Context;

import com.dating.chatbox.controllers.base.BaseController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.response.HomeLiveUsersResponse;
import com.dating.chatbox.controllers.response.HomeUsersResponse;

import org.json.JSONObject;


public class HomeLiveUsersController extends BaseController {
    public static final String TAG = "HomeLiveUsersController";

    public HomeLiveUsersController(Context context, RequestListener listener, String url) {
        super(context, listener, HomeLiveUsersResponse.class, TAG);
        mUrl = url;
    }

    public void setRequestObj(JSONObject jsonObject) {
        super.mRequestObjectPayload = jsonObject;
    }

}
