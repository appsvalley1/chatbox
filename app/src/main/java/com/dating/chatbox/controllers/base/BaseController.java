package com.dating.chatbox.controllers.base;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dating.chatbox.ChatApplication;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.Utils;

import org.json.JSONObject;



public abstract class BaseController<T> {
    protected Context mContext;
    protected ResponsePayload mResponsePayload;
    protected RequestListener mRequestListener;
    protected String mUrl;
    protected boolean isNetworkAvailable;
    protected JSONObject mRequestObjectPayload;
    private int method = Request.Method.POST;
    private int code;
    public String TAG;
    private boolean showToast = true;
    private Class<T> clazz;
    private Handler handler;

    private Runnable checkInternetRunnable = new Runnable() {
        @Override
        public void run() {
            Utils.lateResToast(mContext);
        }
    };


    public BaseController(Context context, RequestListener listener, Class<T> clazz, String TAG) {
        mContext = context;
        mResponsePayload = new ResponsePayload();
        this.mRequestListener = listener;
        this.clazz = clazz;
        this.TAG = TAG;
        handler = new Handler(Looper.getMainLooper());
        constructResponsePayloadIfNecessary();
        initializedHttpClient();

        if (showToast)
            checkinternet();

    }

    public BaseController(Context context, RequestListener listener, Class<T> clazz, String TAG, boolean showToast) {
        mContext = context;
        mResponsePayload = new ResponsePayload();
        this.mRequestListener = listener;
        this.clazz = clazz;
        this.TAG = TAG;
        this.showToast = showToast;
        handler = new Handler(Looper.getMainLooper());
        constructResponsePayloadIfNecessary();
        initializedHttpClient();

        if (showToast)
            checkinternet();

    }


    private void checkinternet() {
        handler.postDelayed(checkInternetRunnable, 4000);
    }

    private void initializedHttpClient() {
        mUrl = Singleton.BaseUrl;
    }

    public ResponsePayload getResponsePayload() {
        return mResponsePayload;
    }

    public void setResponsePayload(ResponsePayload _responsePayload) {
        this.mResponsePayload = _responsePayload;
    }

    public void setRequestPayload(JSONObject object) {
        mRequestObjectPayload = object;
    }

    public void setRequestMethod(int method) {
        this.method = method;
    }

    public void setRequestCode(int code) {
        this.code = code;
    }

    public void sendRequest() {


        GsonRequest customRequest = new GsonRequest<T>(TAG, mUrl, mRequestObjectPayload, clazz, new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                handler.removeCallbacks(checkInternetRunnable);
                mResponsePayload.setStatus(ResponsePayload.ResponseStatus.SUCCESS);
                mResponsePayload.setResponseObject(response);
                mRequestListener.handleResponse(mResponsePayload);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handler.removeCallbacks(checkInternetRunnable);
                Utils.handleError(mResponsePayload, error, TAG, showToast);
                mRequestListener.handleResponse(mResponsePayload);
            }
        });
        ChatApplication.getInstance().addToRequestQueue(customRequest);

    }

    protected void constructResponsePayloadIfNecessary() {
        if (!Utils.isConnected()) {
            isNetworkAvailable = false;
            mResponsePayload.setStatus(ResponsePayload.ResponseStatus.NO_NETWORK);
            mResponsePayload.setMessage("Unable to connect to internet");
        } else {
            isNetworkAvailable = true;
        }
    }

}
