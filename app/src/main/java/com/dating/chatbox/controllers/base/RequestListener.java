package com.dating.chatbox.controllers.base;

/**
 * Created by ajay on 8/29/2015.
 */
public interface RequestListener {

    void handleResponse(ResponsePayload responseObject);
}
