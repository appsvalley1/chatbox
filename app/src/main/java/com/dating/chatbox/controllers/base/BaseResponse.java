package com.dating.chatbox.controllers.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by W2O on 21-09-2015.
 */
public class BaseResponse {

    @SerializedName("STATUS_MESSAGE")
    @Expose
    public String statusMessage;
    @SerializedName("STATUS_CODE")
    @Expose
    public Integer statusCode;

}
