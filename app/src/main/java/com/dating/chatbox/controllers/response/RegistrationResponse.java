package com.dating.chatbox.controllers.response;

import com.dating.chatbox.Models.User;
import com.dating.chatbox.controllers.base.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Prateek on 19-07-2018.
 */
public class RegistrationResponse extends BaseResponse {




    @SerializedName("ResponseMessage")
    @Expose
    public String ResponseMessage;

    @SerializedName("Status")
    @Expose
    public int Status;

    @SerializedName("User")
    @Expose
    public User User;

}
