package com.dating.chatbox.controllers.response;

import com.dating.chatbox.Models.ActivityModel;
import com.dating.chatbox.controllers.base.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Prateek on 19-07-2018.
 */
public class ActivityResponse extends BaseResponse {

    @SerializedName("ListActivities")
    @Expose
    public ArrayList<ActivityModel> ListActivities = null;

}
