package com.dating.chatbox.controllers;

import android.content.Context;

import com.dating.chatbox.controllers.base.BaseController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.response.DashboardResponse;
import com.dating.chatbox.controllers.response.RegistrationResponse;

import org.json.JSONObject;

/**
 * Created by Prateek on 19-07-2018.
 */



public class DashboardController extends BaseController {
    public static final String TAG = "DashBoardController";

    public DashboardController(Context context, RequestListener listener, String url) {
        super(context, listener, DashboardResponse.class, TAG);
        mUrl = url;
    }

    public void setRequestObj(JSONObject jsonObject) {
        super.mRequestObjectPayload = jsonObject;
    }

}
