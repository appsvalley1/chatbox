package com.dating.chatbox.controllers;

import android.content.Context;

import com.dating.chatbox.controllers.base.BaseController;
import com.dating.chatbox.controllers.base.RequestListener;
import com.dating.chatbox.controllers.response.PreferenceResponse;
import com.dating.chatbox.controllers.response.RegistrationResponse;

import org.json.JSONObject;



public class PreferencesController extends BaseController {
    public static final String TAG = "PreferencesController";

    public PreferencesController(Context context, RequestListener listener, String url) {
        super(context, listener, PreferenceResponse.class, TAG);
        mUrl = url;
    }

    public void setRequestObj(JSONObject jsonObject) {
        super.mRequestObjectPayload = jsonObject;
    }

}