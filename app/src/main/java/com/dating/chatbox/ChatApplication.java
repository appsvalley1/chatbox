package com.dating.chatbox;

import android.content.Context;
import android.os.Handler;


import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.dating.chatbox.Custom.ChatBubbleManager;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.GoLive.WorkerThread;
import com.dating.chatbox.db.DBUtil;
import com.google.android.gms.ads.MobileAds;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.roster.Roster;


import io.agora.AgoraAPIOnlySignal;



public class ChatApplication extends MultiDexApplication {

    public   boolean isAppForeground;
    public static ChatApplication mInstance;
    public static AbstractXMPPConnection conn2;
    public Roster roster;
    public boolean IsUserImage = false;
    private Handler mHandler;
    private static final int HTTP_TIMEOUT = 30 * 1000;
    public boolean isFromProfile=false;
    private RequestQueue mRequestQueue;
    public static final String TAG = "Chatse";
    private WorkerThread mWorkerThread;
    private AgoraAPIOnlySignal m_agoraAPI;
    private ChatBubbleManager.Listener bmListener = new ChatBubbleManager.Listener() {
        @Override
        public void onBecameForeground() {

            isAppForeground = true;
        }

        @Override
        public void onBecameBackground() {
            RemoveBroadcastingUser();
            isAppForeground = false;
        }
    };

    public static ChatApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        ChatBubbleManager.get(this).addListener(bmListener);
        initWorkerThread();
        mInstance = this;

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6689795083534236~1389645167");
        setupAgoraEngine();
    }
    public AgoraAPIOnlySignal getmAgoraAPI() {
        return m_agoraAPI;
    }
    private void setupAgoraEngine() {
        String appID = getString(R.string.private_app_id);

        try {
            m_agoraAPI = AgoraAPIOnlySignal.getInstance(this, appID);


        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));

            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public RequestQueue getRequestQueuewithoutSSL() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(HTTP_TIMEOUT, 0, 0));
        getRequestQueuewithoutSSL().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req, int timeout) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(timeout, 0, 0));
        getRequestQueuewithoutSSL().add(req);
    }
    public synchronized void initWorkerThread() {
        if (mWorkerThread == null) {
            mWorkerThread = new WorkerThread(getApplicationContext());
            mWorkerThread.start();

            mWorkerThread.waitForReady();
        }
    }

    public synchronized WorkerThread getWorkerThread() {
        return mWorkerThread;
    }

    public synchronized void deInitWorkerThread() {
        mWorkerThread.exit();
        try {
            mWorkerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mWorkerThread = null;
    }
    private void RemoveBroadcastingUser()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Singleton.BaseUrl + "RemoveBroadcastingUser?UserID=" + DBUtil.getUSERID();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


}
