package com.dating.chatbox;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.Custom.Utils;
import com.dating.chatbox.Models.ResponseManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ForgotPassword extends AppCompatActivity implements Validator.ValidationListener {

    Validator validator;
    private LinearLayout lvLogin;
    @NotEmpty
    @Email
    private EditText EditTextEmail;
    private ProgressBar progressBar;
    private Button startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);

        findViewsbyId();


        lvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        validator = new Validator(this);
        validator.setValidationListener(this);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        if (Utils.CheckIf2G()) {
            Utils.TakingTimeMsg(ForgotPassword.this);
        }
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(ForgotPassword.this);
        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        String url = Singleton.BaseUrl + "ForgotPassword?EmailAddress=" + EditTextEmail.getText().toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            // Display the first 500 characters of the response string.
                            JSONObject jsonObject = new JSONObject(response);
                            ResponseManager responseManager = new ResponseManager();


                            responseManager.setResponseMessage(jsonObject.optString("ResponseMessage"));
                            responseManager.setStatus(jsonObject.optInt("Status"));
                            if (responseManager.getStatus() == 1) {


                                Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getApplicationContext(), responseManager.getResponseMessage(), Toast.LENGTH_SHORT).show();

                                finish();


                            }


                            progressBar.setVisibility(View.GONE);


                        } catch (JSONException e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.VISIBLE);

            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void findViewsbyId() {
        lvLogin = (LinearLayout) findViewById(R.id.lvLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        EditTextEmail = (EditText) findViewById(R.id.EditTextEmail);
        startButton = (Button) findViewById(R.id.startButton);
    }

}
