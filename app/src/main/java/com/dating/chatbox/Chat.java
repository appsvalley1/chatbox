package com.dating.chatbox;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;


public class Chat extends AppCompatActivity {
  /*  LinearLayout layout;
    ImageView sendMessageBtn, ImageUserStatus, userImage;
    EditText EditTextMessage;
    ScrollView scrollView;
    Firebase reference1, reference2;
    RecyclerView recyclerView;
    ChatsAdapter mAdapter;
    private List<ChatsModel> chatList = new ArrayList<>();
    private TextView textviewStatus, textViewLastTime, textViewNoChats;
    private ImageView ImageEmogiOpen;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_chat);
        /*textViewNoChats = findViewById(R.id.textViewNoChats);
        textViewLastTime = findViewById(R.id.textViewLastTime);
        ImageEmogiOpen = findViewById(R.id.ImageEmogiOpen);
        ImageEmogiOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarchat, null);
        textviewStatus = (TextView) mCustomView.findViewById(R.id.textviewStatus);
        ImageUserStatus = (ImageView) mCustomView.findViewById(R.id.ImageUserStatus);
        userImage = (ImageView) mCustomView.findViewById(R.id.userImage);
        Glide.with(this)
                .load(User.getUserProfile().getImageUrl()).dontAnimate()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(userImage);
        toolbar.addView(mCustomView);
        TextView username = (TextView) mCustomView.findViewById(R.id.username);
        username.setText(User.getUserProfile().getName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.recyclerView);

        sendMessageBtn = findViewById(R.id.sendMessageBtn);
        EditTextMessage = findViewById(R.id.EditTextMessage);
        scrollView = findViewById(R.id.scrollView);
        Firebase.setAndroidContext(this);
        reference1 = new Firebase("https://chatbox-8abc4.firebaseio.com/messages/" + DBUtil.getUSERID());
        reference2 = new Firebase("https://chatbox-8abc4.firebaseio.com//messages/" + +User.getUserProfile().getId());
        mAdapter = new ChatsAdapter(chatList, this);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getApplicationContext());
        gridLayoutManager.setStackFromEnd(true);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(false);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        gridLayoutManager.setStackFromEnd(true);
        recyclerView.setAdapter(mAdapter);

        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = EditTextMessage.getText().toString();

                if (!messageText.equals("")) {

                    textViewNoChats.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("message", messageText);
                    map.put("user", DBUtil.getUSERID());
                    long time = System.currentTimeMillis();
                    map.put("TimeStamp", String.valueOf(time));
                    reference1.push().setValue(map);
                    reference2.push().setValue(map);
                    EditTextMessage.setText("");
                }
            }
        });

        reference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String userid = map.get("user").toString();
                Long TimeStamp = Long.parseLong(map.get("TimeStamp").toString());

                if (userid.equalsIgnoreCase(DBUtil.getUSERID())) {
                    ChatsModel chatsModel = new ChatsModel();
                    chatsModel.setChatText(message);
                    chatsModel.setisSender(true);
                    chatsModel.setChatTime(TimeStamp);


                    chatsModel.setChatUser(User.getUserProfile());
                    chatList.add(chatsModel);
                    mAdapter.notifyDataSetChanged();

                } else {
                    ChatsModel chatsModel = new ChatsModel();
                    chatsModel.setChatText(message);
                    chatsModel.setisSender(false);

                    chatsModel.setChatTime(TimeStamp);
                    chatsModel.setChatUser(User.getUserProfile());
                    chatList.add(chatsModel);
                    mAdapter.notifyDataSetChanged();
                }
              *//*  textViewLastTime.setText(Utils.getDateCurrentTimeZone(TimeStamp));*//*

                recyclerView.scrollToPosition(chatList.size() - 1);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}