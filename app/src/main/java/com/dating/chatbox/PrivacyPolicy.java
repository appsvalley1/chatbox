package com.dating.chatbox;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dating.chatbox.Custom.CustomFontQuestrialRegular;
import com.dating.chatbox.Custom.SessionManager;
import com.dating.chatbox.Custom.Singleton;
import com.google.android.gms.ads.AdRequest;


public class PrivacyPolicy extends AppCompatActivity {
    private CustomFontQuestrialRegular p_t_desc_text;
    private Button startButton;
    private SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        loadAdmobbackpressAd();

        sessionManager = new SessionManager(this);

        findViewById();
        try {
            String myString = "Please review & accept our Privacy Policy & Terms & Conditions to continue";
            int i1 = myString.indexOf("Pri");
            int i2 = myString.indexOf("icy");
            int i3 = myString.indexOf("Ter");
            int i4 = myString.indexOf("ons");
            p_t_desc_text.setMovementMethod(LinkMovementMethod.getInstance());
            p_t_desc_text.setText(myString, TextView.BufferType.SPANNABLE);

            Spannable mySpannable = (Spannable) p_t_desc_text.getText();
            ClickableSpan myClickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    Singleton.BrowserTitle = "Privacy Policy";
                    Singleton.Urls = "http://proacdoc.com/EmailTemplates/privacy.html";
                    startActivity(new Intent(widget.getContext(), InAppBrowserActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorPrimary));
                }
            };
            mySpannable.setSpan(myClickableSpan, i1, i2 + 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            Spannable spannable = (Spannable) p_t_desc_text.getText();
            ClickableSpan ClickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View widget) {

                    Singleton.BrowserTitle = "Terms & Conditions";
                    Singleton.Urls = "http://proacdoc.com/EmailTemplates/privacy.html";
                    startActivity(new Intent(widget.getContext(), InAppBrowserActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorPrimary));
                }
            };
            spannable.setSpan(ClickableSpan, i3, i4 + 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {

        }

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Answers.getInstance().logCustom(new CustomEvent("Privacy Clicked"));
                sessionManager.SetPrivacyClicked(true);
                Intent intent = new Intent(PrivacyPolicy.this, RegisterNow.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
    }


    private void findViewById() {
        startButton = (Button) findViewById(R.id.startButton);

        p_t_desc_text = (CustomFontQuestrialRegular) findViewById(R.id.p_t_desc_text);
    }
    private void loadAdmobbackpressAd() {
        final com.google.android.gms.ads.AdView mAdView = (com.google.android.gms.ads.AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

}
