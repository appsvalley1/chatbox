package com.dating.chatbox;

import android.app.IntentService;
import android.content.Intent;
import android.provider.Settings;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dating.chatbox.Custom.Singleton;
import com.dating.chatbox.db.DBUtil;


/**
 * ShowMania live game show
 */
public class SaveUserDeviceInfo extends IntentService {

    public SaveUserDeviceInfo() {
        super("SaveUserDeviceInfo");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
                @Override
                public void onRequestFinished(Request<String> request) {

                }
            });


            String Version = android.os.Build.VERSION.RELEASE;
            String Brand = android.os.Build.BRAND;
            String Manufacturer = android.os.Build.MANUFACTURER;
            String Model = android.os.Build.MODEL;
            String DeviceID = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            String url = Singleton.BaseUrl + "AddUserDeviceInformation?UserID=" + DBUtil.getUSERID() + "&OSVersion=" + Version + "&Brand=" + Brand + "&Manufacturer=" + Manufacturer + "&Model=" + Model + "&DeviceID=" + DeviceID;
            url = url.replaceAll(" ", "%20");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}