package com.dating.chatbox.Models;

import com.dating.chatbox.controllers.base.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class User extends BaseResponse{

    public static User userProfile;

    public String getInviteUrl() {
        return InviteUrl;
    }

    public void setInviteUrl(String inviteUrl) {
        InviteUrl = inviteUrl;
    }

    private String Birthday;
    private String CityName;
    private String Dated;
    private String EmailAddress;
    private String Gender;
    private String InviteUrl;


    private ArrayList<UserImages> userImages = new ArrayList<>();
    private int Id;

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    private String ImageUrl;

    public boolean isFavourite() {
        return IsFavourite;
    }

    public void setFavourite(boolean favourite) {
        IsFavourite = favourite;
    }

    @SerializedName("Name")
    @Expose

    private String Name;

    private String Password;
    private Boolean IschatUser;
    @SerializedName("ViewsCount")
    @Expose
    private int ViewsCount;
    private String ShortBio;
    private int ShowLocationStatus;
    private String Latitude;
    private String Longitude;
    public Boolean HasPreference;

    public Boolean getHasPreference() {
        return HasPreference;
    }

    public void setHasPreference(Boolean hasPreference) {
        HasPreference = hasPreference;
    }

    private int ShowOnlineStatus;
    private String DistanceKM;



    @SerializedName("IsFavourite")
    @Expose
    private boolean IsFavourite;



    public User(String _name, String _Birthday, String _ImageUrl) {
        this.Name = _name;
        this.Birthday = _Birthday;
        this.ImageUrl = _ImageUrl;


    }
    public User() {

    }

    public static User getUserProfile() {
        return userProfile;
    }

    public static void setUserProfile(User userProfile) {
        User.userProfile = userProfile;
    }

    public String getDistanceKM() {
        return DistanceKM;
    }

    public void setDistanceKM(String distanceKM) {
        DistanceKM = distanceKM;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public int getShowLocationStatus() {
        return ShowLocationStatus;
    }

    public void setShowLocationStatus(int showLocationStatus) {
        ShowLocationStatus = showLocationStatus;
    }

    public int getShowOnlineStatus() {
        return ShowOnlineStatus;
    }

    public void setShowOnlineStatus(int showOnlineStatus) {
        ShowOnlineStatus = showOnlineStatus;
    }

    public ArrayList<UserImages> getUserImages() {
        return userImages;
    }

    public void setUserImages(ArrayList<UserImages> userImages) {
        this.userImages = userImages;
    }

    public String getBirthday() {
        return Birthday+ " Years";
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }



    public String getDated() {
        return Dated;
    }

    public void setDated(String dated) {
        Dated = dated;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getGender() {


        if (Gender.equalsIgnoreCase("1")) {
            return "Male";
        }
        return "Female";

    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    public int getViewsCount() {
        return ViewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.ViewsCount = viewsCount;
    }

    public String getShortBio() {
        return ShortBio;
    }

    public void setShortBio(String shortBio) {
        ShortBio = shortBio;
    }

    public Boolean getIschatUser() {
        return IschatUser;
    }

    public void setIschatUser(Boolean ischatUser) {
        IschatUser = ischatUser;
    }



    @SerializedName("User_Preferences")
    @Expose
    public UserPreferences userPreferences;
}
