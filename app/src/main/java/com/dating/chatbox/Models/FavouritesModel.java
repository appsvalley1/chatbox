package com.dating.chatbox.Models;

/**
 * Created by USER on 10/16/2017.
 */

public class FavouritesModel {

    public static FavouritesModel favouritesModel;
    public String FavouriteDated;
    public int FavouriteID;


    public User Favouriteuser;

    public static FavouritesModel getFavouritesModel() {
        return favouritesModel;
    }

    public static void setFavouritesModel(FavouritesModel favouritesModel) {
        FavouritesModel.favouritesModel = favouritesModel;
    }

    public User getFavouriteuser() {
        return Favouriteuser;
    }

    public void setFavouriteuser(User favouriteuser) {
        Favouriteuser = favouriteuser;
    }

    public int getFavouriteID() {
        return FavouriteID;
    }

    public void setFavouriteID(int favouriteID) {
        FavouriteID = favouriteID;
    }

    public String getFavouriteDated() {
        return FavouriteDated;
    }

    public void setFavouriteDated(String favouriteDated) {
        FavouriteDated = favouriteDated;
    }
}
