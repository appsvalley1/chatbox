package com.dating.chatbox.Models;

/**
 * Created by USER on 9/18/2017.
 */

public class ResponseManager {
    public String ResponseMessage;
    public int Status;

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
