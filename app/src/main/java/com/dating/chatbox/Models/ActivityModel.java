package com.dating.chatbox.Models;

import com.facebook.ads.AdView;
import com.facebook.ads.NativeAd;
import com.google.firebase.database.Exclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 9/28/2017.
 */

public class ActivityModel {

    public static ActivityModel activityModel;
    public String UsersActivitiesText;
    public String UsersActivitiesDated;
    public int ActivityType;

    public boolean getActivityIsAd() {
        return ActivityIsAd;
    }

    public void setActivityIsAd(boolean activityIsAd) {
        ActivityIsAd = activityIsAd;
    }
    @Exclude
    public transient boolean ActivityIsAd;


    @SerializedName("ActivityDoneByUser")
    @Expose
    public User ActivityUser;


    public AdView getNativeAd() {
        return nativeAd;
    }

    public void setNativeAd(AdView nativeAd) {
        this.nativeAd = nativeAd;
    }
    @Exclude
    private transient AdView nativeAd;

    public static ActivityModel getActivityModel() {
        return activityModel;
    }

    public static void setActivityModel(ActivityModel activityModel) {
        ActivityModel.activityModel = activityModel;
    }

    public int getActivityType() {
        return ActivityType;
    }

    public void setActivityType(int activityType) {
        ActivityType = activityType;
    }

    public User getActivityUser() {
        return ActivityUser;
    }

    public void setActivityUser(User activityUser) {
        ActivityUser = activityUser;
    }

    public String getActivitydate() {
        return UsersActivitiesDated;
    }

    public void setActivitydate(String activitydate) {
        UsersActivitiesDated = activitydate;
    }

    public String getActivityDetail() {
        return UsersActivitiesText;
    }

    public void setActivityDetail(String activityDetail) {
        UsersActivitiesText = activityDetail;
    }
}
