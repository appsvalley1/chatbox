package com.dating.chatbox.Models;

/**
 * Created by USER on 9/29/2017.
 */

public class ChatsModel {

    private int ChatsID;
    private String ChatText;
    private long ChatTime;
    private User ChatUser;
    private int ChatIsText;
    private Boolean isSender;
    private Boolean isFromServer;

    public int getChatIsText() {
        return ChatIsText;
    }

    public void setChatIsText(int chatIsText) {
        ChatIsText = chatIsText;
    }

    public int getChatsID() {
        return ChatsID;
    }

    public void setChatsID(int chatsID) {
        ChatsID = chatsID;
    }

    public String getChatText() {
        return ChatText;
    }

    public void setChatText(String chatText) {
        ChatText = chatText;
    }

    public long getChatTime() {
        return ChatTime;
    }

    public void setChatTime(long chatTime) {
        ChatTime = chatTime;
    }

    public User getChatUser() {
        return ChatUser;
    }

    public void setChatUser(User chatUser) {
        ChatUser = chatUser;
    }


    public Boolean getFromServer() {
        return isFromServer;
    }

    public void setFromServer(Boolean fromServer) {
        isFromServer = fromServer;
    }

    public Boolean getisSender() {
        return isSender;
    }

    public void setisSender(Boolean _isSender) {
        isSender = _isSender;
    }
}
