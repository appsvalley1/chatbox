package com.dating.chatbox.Models;



/**
 * Created by USER on 12/2/2017.
 */

public class PreferenceModel {

    public long PreferenceAgeMin;
    public long PreferenceAgeMax;
    //Gender 1 Male 2 femaile, 3 all
    public int PreferenceGender;
    //Popularity 1 Low 2 Mideum, 3 all
    public int PreferencePopularity;
    //Status 1 online 2 offline, 3 all
    public int PreferenceStatus;

    public long getPreferenceAgeMin() {
        return PreferenceAgeMin;
    }

    public void setPreferenceAgeMin(long preferenceAgeMin) {
        PreferenceAgeMin = preferenceAgeMin;
    }

    public long getPreferenceAgeMax() {
        return PreferenceAgeMax;
    }

    public void setPreferenceAgeMax(long preferenceAgeMax) {
        PreferenceAgeMax = preferenceAgeMax;
    }

    public int getPreferenceGender() {
        return PreferenceGender;
    }

    public void setPreferenceGender(int preferenceGender) {
        PreferenceGender = preferenceGender;
    }

    public int getPreferencePopularity() {
        return PreferencePopularity;
    }

    public void setPreferencePopularity(int preferencePopularity) {
        PreferencePopularity = preferencePopularity;
    }

    public int getPreferenceStatus() {
        return PreferenceStatus;
    }

    public void setPreferenceStatus(int preferenceStatus) {
        PreferenceStatus = preferenceStatus;
    }
}
