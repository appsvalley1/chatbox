package com.dating.chatbox.Models;

/**
 * Created by Prateek on 19-07-2018.
 */
public class UserPreferences {


   private int GenderPreference;

    private int  MaxAgePreference;

    private int MinAgePreference;

    public int getGenderPreference() {
        return GenderPreference;
    }

    public void setGenderPreference(int genderPreference) {
        GenderPreference = genderPreference;
    }

    public int getMaxAgePreference() {
        return MaxAgePreference;
    }

    public void setMaxAgePreference(int maxAgePreference) {
        MaxAgePreference = maxAgePreference;
    }

    public int getMinAgePreference() {
        return MinAgePreference;
    }

    public void setMinAgePreference(int minAgePreference) {
        MinAgePreference = minAgePreference;
    }
}
