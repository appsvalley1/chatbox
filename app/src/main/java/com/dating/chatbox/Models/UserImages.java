package com.dating.chatbox.Models;



/**
 * Created by USER on 11/9/2017.
 */

public class UserImages {

    public int ImageId;
    public String UserImageUrl;
    public Boolean IsBlankImage;
    public Boolean IsFirstImage;

    public Boolean getFirstImage() {
        return IsFirstImage;
    }

    public void setFirstImage(Boolean firstImage) {
        IsFirstImage = firstImage;
    }

    public String getUserImageUrl() {
        String image1 = "https://firebasestorage.googleapis.com/v0/b/chatbox-8abc4.appspot.com/o/" + UserImageUrl + "?alt=media&token=b8675037-24fb-483c-a759-79a17bdac69c";
        return  image1;
    }

    public void setUserImageUrl(String userImageUrl) {
        UserImageUrl = userImageUrl;
    }

    public Boolean getBlankImage() {
        return IsBlankImage;
    }

    public void setBlankImage(Boolean blankImage) {
        IsBlankImage = blankImage;
    }

    public int getImageId() {
        return ImageId;
    }

    public void setImageId(int imageId) {
        ImageId = imageId;
    }


}
