package com.dating.chatbox.Models;

/**
 * Created by Prateek on 19-07-2018.
 */
public class DashboardModel {

    private int AppVersion;
    private String InviteText;

    public int getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(int appVersion) {
        AppVersion = appVersion;
    }

    public String getInviteText() {
        return InviteText;
    }

    public void setInviteText(String inviteText) {
        InviteText = inviteText;
    }

    public String getInviteTitle() {
        return InviteTitle;
    }

    public void setInviteTitle(String inviteTitle) {
        InviteTitle = inviteTitle;
    }

    public int getIsRequiredUpdate() {
        return IsRequiredUpdate;
    }

    public int getExist() {
        return Exists;
    }

    public void setExist(int exist) {
        Exists = exist;
    }

    public void setIsRequiredUpdate(int isRequiredUpdate) {
        IsRequiredUpdate = isRequiredUpdate;
    }

    private String    InviteTitle;
    private int IsRequiredUpdate;

    private int Exists;
}
