package com.dating.chatbox;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dating.chatbox.Custom.ThreadManager;
import com.dating.chatbox.db.DBUtil;

public class SettingsActivity extends AppCompatActivity {

    private SwitchCompat switchshowonline, switchshowlocation, switchsoundchats, switchsappsounds, switchshowmessagenotification, switchnotificationsounds, switchnotificationvibration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        LayoutInflater mInflater = LayoutInflater.from(getApplicationContext());
        View mCustomView = mInflater.inflate(R.layout.customtoolbarother, null);
        toolbar.addView(mCustomView);
        TextView appname = (TextView) mCustomView.findViewById(R.id.appname);
        appname.setText("Settings");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewsById();


        ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {

            public String ShowOnlineStatus, ShowLocationStatus, ChatSoundStatus, AppSound, ChatMessageNotification, NotificationSound, Vibrations;

            @Override
            public void onBackground() {
                try {

                    ShowOnlineStatus = DBUtil.getShowOnlineStatus();
                    ShowLocationStatus = DBUtil.getShowLocationStatus();
                    ChatSoundStatus = DBUtil.getChatSoundStatus();
                    AppSound = DBUtil.getAppSound();
                    ChatMessageNotification = DBUtil.getChatMessageNotification();
                    NotificationSound = DBUtil.getNotificationSound();
                    Vibrations = DBUtil.getVibrations();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUi() {
                super.onUi();
                try {

                    if (ShowOnlineStatus.equalsIgnoreCase("") || ShowOnlineStatus.equalsIgnoreCase("0")) {

                        switchshowonline.setChecked(false);
                    } else {
                        switchshowonline.setChecked(true);
                    }


                    if (ShowLocationStatus.equalsIgnoreCase("") || ShowLocationStatus.equalsIgnoreCase("0")) {

                        switchshowlocation.setChecked(false);
                    } else {
                        switchshowlocation.setChecked(true);
                    }


                    if (ChatSoundStatus.equalsIgnoreCase("") || ChatSoundStatus.equalsIgnoreCase("0")) {

                        switchsoundchats.setChecked(false);
                    } else {
                        switchsoundchats.setChecked(true);
                    }


                    if (AppSound.equalsIgnoreCase("") || AppSound.equalsIgnoreCase("0")) {

                        switchsappsounds.setChecked(false);
                    } else {
                        switchsappsounds.setChecked(true);
                    }


                    if (ChatMessageNotification.equalsIgnoreCase("") || ChatMessageNotification.equalsIgnoreCase("0")) {

                        switchshowmessagenotification.setChecked(false);
                    } else {
                        switchshowmessagenotification.setChecked(true);
                    }


                    if (NotificationSound.equalsIgnoreCase("") || NotificationSound.equalsIgnoreCase("0")) {

                        switchnotificationsounds.setChecked(false);
                    } else {
                        switchnotificationsounds.setChecked(true);
                    }


                    if (Vibrations.equalsIgnoreCase("") || Vibrations.equalsIgnoreCase("0")) {

                        switchnotificationvibration.setChecked(false);
                    } else {
                        switchnotificationvibration.setChecked(true);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });


    }

    public void findViewsById() {
        switchshowonline = findViewById(R.id.switchshowonline);
        switchshowonline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setShowOnlineStatus("1");
                                startService(new Intent(SettingsActivity.this, SaveUserPrivacyService.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setShowOnlineStatus("0");
                                startService(new Intent(SettingsActivity.this, SaveUserPrivacyService.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });

        switchshowlocation = findViewById(R.id.switchshowlocation);
        switchshowlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setShowLocationStatus("1");
                                startService(new Intent(SettingsActivity.this, SaveUserPrivacyService.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setShowLocationStatus("0");
                                startService(new Intent(SettingsActivity.this, SaveUserPrivacyService.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });

        switchsoundchats = findViewById(R.id.switchsoundchats);
        switchsoundchats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setChatSoundStatus("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setChatSoundStatus("0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        switchsappsounds = findViewById(R.id.switchsappsounds);
        switchsappsounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setAppSound("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setAppSound("0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        switchshowmessagenotification = findViewById(R.id.switchshowmessagenotification);
        switchshowmessagenotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setChatMessageNotification("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setChatMessageNotification("0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
        switchnotificationsounds = findViewById(R.id.switchnotificationsounds);
        switchnotificationsounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setNotificationSound("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setNotificationSound("0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });

        switchnotificationvibration = findViewById(R.id.switchnotificationvibration);
        switchnotificationvibration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((SwitchCompat) view).isChecked()) {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setVibrations("1");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    ThreadManager.getInstance().doWork(new ThreadManager.CustomRunnable() {
                        @Override
                        public void onBackground() {
                            try {
                                DBUtil.setVibrations("0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
